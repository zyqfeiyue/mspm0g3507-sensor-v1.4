#ifndef SAE_J1939_H_
#define SAE_J1939_H_
#include "main.h"
#include "can_driver.h"
typedef union
{
	__IO uint32_t value;
	struct
	{
		__IO uint32_t data1_cyclic_flag							: 1; /* [0] */
		__IO uint32_t data2_cyclic_flag							: 1; /* [1] */
		__IO uint32_t data2_on_request_flag					: 1; /* [2] */
		__IO uint32_t data3_on_request_flag					: 1; /* [3] */
		__IO uint32_t data4_on_request_flag					: 1; /* [4] */
		__IO uint32_t data5_on_request_flag					: 1; /* [5] */
		__IO uint32_t data5_cts_on_request_1_flag		: 1; /* [6] */
		__IO uint32_t data5_cts_on_request_2_flag		: 1; /* [7] */
		__IO uint32_t data5_cts_on_request_3_flag		: 1; /* [8] */
		__IO uint32_t data5_cts_on_request_4_flag		: 1; /* [9] */
		__IO uint32_t data5_cts_on_request_5_flag		: 1; /* [10] */
		__IO uint32_t data6_on_request_flag					: 1; /* [11] */
		__IO uint32_t data7_on_request_flag					: 1; /* [12] */
		__IO uint32_t data7_cts_on_request_1_flag		: 1; /* [13] */
		__IO uint32_t data7_cts_on_request_2_flag		: 1; /* [14] */
		__IO uint32_t data7_cts_on_request_3_flag		: 1; /* [15] */
		__IO uint32_t data10_on_request_flag				: 1; /* [16] */
		__IO uint32_t data10_cts_on_request_1_flag	: 1; /* [17] */
		__IO uint32_t data10_cts_on_request_2_flag	: 1; /* [18] */
		__IO uint32_t data10_cts_on_request_3_flag	: 1; /* [19] */
		__IO uint32_t data10_cts_on_request_4_flag	: 1; /* [19] */
		__IO uint32_t data10_cts_on_request_5_flag	: 1; /* [19] */
		__IO uint32_t data_ack											: 1; /* [20] */
		__IO uint32_t data_null											: 1; /* [21] */
		__IO uint32_t data_busy											: 1; /* [22] */
		__IO uint32_t other													: 9; /* [23-31] */
	} bit;
} j1939_data_flag_type;

typedef struct
{
	/**
	* @brief NOx concentration
	*/
  union
  {
    uint16_t value;
    struct
    {
      uint16_t l      : 8; /* [0-7] */ // 低8位
      uint16_t h      : 8; /* [7-15] */ // 高8位
    } bit;
  }nox_concentration_byte_01;
	/**
	* @brief O2 concentration
	*/
  union
  {
    uint16_t value;
    struct
    {
      uint16_t l      : 8; /* [0-7] */ // 低8位
      uint16_t h      : 8; /* [7-15] */ // 高8位
    } bit;
  }o2_concentration_byte_23;
	/**
	* @brief status byte 4
	*/
  union
  {
    uint8_t value;
    struct
    {
      uint8_t sensor_power						: 2; /* [0-1] */ // 传感器供电状态
      uint8_t sensor_at_temperature		: 2; /* [2-3] */ // 传感器温度
			uint8_t nox_reading_stable 			: 2; /* [4-5] */ // NOx浓度状态
			uint8_t o2_reading_stable				: 2; /* [6-7] */ // O2浓度状态
    } bit;
  }status_byte_4;
	/**
	* @brief status byte 5
	*/
  union
  {
    uint8_t value;
    struct
    {
      uint8_t error_heater				: 5; /* [0-4] */ // 加热器错误
      uint8_t status_heater_mode	: 2; /* [5-6] */ // 加热器模式状态
			uint8_t not_used 						: 1; /* [7] */ // 未使用
    } bit;
  }status_byte_5;
	/**
	* @brief status byte 6
	*/
  union
  {
    uint8_t value;
    struct
    {
      uint8_t error_nox									: 5; /* [0-4] */ // NOx错误
      uint8_t status_diagnosis_feedback	: 3; /* [5-6] */ // 诊断模式返回状态
    } bit;
  }status_byte_6;
	/**
	* @brief status byte 7
	*/
  union
  {
    uint8_t value;
    struct
    {
      uint8_t error_o2	: 5; /* [0-4] */ // O2错误
      uint8_t not_used	: 3; /* [5-6] */ // 未使用
    } bit;
  }status_byte_7;
	uint32_t pgn;
	uint8_t scu_DA;
} scu_data1_signal_values_type;

typedef struct
{
	/**
	* @brief heater ratio
	*/
  union
  {
    uint16_t value;
    struct
    {
      uint16_t l      : 8; /* [0-7] */ // 低8位
      uint16_t h      : 8; /* [7-15] */ // 高8位
    } bit;
  } heater_ratio_byte_01; // range:0-8000 data range:0-8 offset:0 lsb:0.001
	/**
	* @brief nox_correction_gain
	*/
  union
  {
    uint16_t value;
    struct
    {
      uint16_t l      : 8; /* [0-7] */ // 低8位
      uint16_t h      : 8; /* [7-15] */ // 高8位
    } bit;
  } nox_correction_gain_byte_23; // range:0-2000 data range:-100-100 offset:-100 lsb:0.1%
	uint8_t nox_correction_offset_byte_4; // range:0-250 data range:-125-125 offset:-125 lsb:1ppm
	/**
	* @brief operation hours counter
	*/
  union
  {
    uint16_t value;
    struct
    {
      uint16_t l      : 8; /* [0-7] */ // 低8位
      uint16_t h      : 8; /* [7-15] */ // 高8位
    } bit;
  } operation_hours_counter_byte_56; // range:0-36000 data range:0-36000 offset:0 lsb:1hour
	uint8_t not_used_byte_7;
	uint32_t pgn;
	uint8_t scu_DA;
} scu_data2_heater_ratio_deviation_type;
typedef struct
{
	uint8_t correction_pressure_lambda_byte_0; // O2压力敏感性 range:0-200 data range:0-1 offset:0 lsb:0.005
	uint8_t correction_pressure_nox_byte_1; // nox压力敏感性 range:0-200 data range:0-1 offset:0 lsb:0.005
	uint8_t correction_no2_byte_2; // no2补偿 range:0-250 data range:0-1.25 offset:0 lsb:0.005
	uint8_t correction_nh3_byte_3; // nh3补偿 range:0-250 data range:0-1.25 offset:0 lsb:0.005
	uint8_t self_diagnosis_result_value_byte_4; // 自诊断偏差 range:0-250 data range:0-250 offset:0 lsb:1%
	uint32_t pgn;
	uint8_t scu_DA;
} scu_data3_correction_factors_type;

#define PGN_DATA2_INTAKE 0x00FD11
#define PGN_DATA2_OUTLET 0x00FD0F
#define PGN_DATA3_INTAKE 0x00FD10
#define PGN_DATA3_OUTLET 0x00FD0E
#define PGN_DATA4_SOFTWARE_IDENTIFICATION 0x00FEDA
#define PGN_DATA5_ECU_IDENTIFICATION 0X00FDC5
#define PGN_DATA6_ADDRESS_CLAIM 0X00EE00
#define PGN_DATA7_DM19 0X00D300
#define CTS_TIME_OUT 1250

/* Send OK enum */
typedef enum {
	STATUS_SEND_OK = 0x00,
	STATUS_SEND_ERROR = 0x01,
	STATUS_SEND_BUSY = 0x02,
	STATUS_SEND_TIMEOUT = 0x03
} ENUM_J1939_STATUS_CODES;
/* Enums for the acknowledgements */
typedef enum {
	GROUP_FUNCTION_VALUE_NORMAL = 0x0,
	GROUP_FUNCTION_VALUE_CANNOT_MAINTAIN_ANOTHER_CONNECTION = 0x1,
	GROUP_FUNCTION_VALUE_LACKING_NECESSARY_RESOURCES = 0x2,
	GROUP_FUNCTION_VALUE_ABORT_TIME_OUT = 0x3,
	GROUP_FUNCTION_VALUE_NO_CAUSE = 0xFF
} ENUM_GROUP_FUNCTION_VALUE_CODES;
/* Control bytes enums */
typedef enum {
	CONTROL_BYTE_TP_CM_ABORT = 0xFF,
	CONTROL_BYTE_TP_CM_BAM = 0x20,
	CONTROL_BYTE_TP_CM_EndOfMsgACK = 0x13,
	CONTROL_BYTE_TP_CM_CTS = 0x11,
	CONTROL_BYTE_TP_CM_RTS = 0x10,
	CONTROL_BYTE_ACKNOWLEDGEMENT_PGN_SUPPORTED = 0x0,
	CONTROL_BYTE_ACKNOWLEDGEMENT_PGN_NOT_SUPPORTED = 0x1,
	CONTROL_BYTE_ACKNOWLEDGEMENT_PGN_ACCESS_DENIED = 0x2,
	CONTROL_BYTE_ACKNOWLEDGEMENT_PGN_BUSY = 0x3
	/* Add more control bytes here */
}ENUM_CONTROL_BYTES_CODES;
/* PGN: 0x00FEDA - Storing the software identification from the reading process */
struct Software_identification {
	uint8_t number_of_fields;						/* How many numbers contains in the identifications array */
	uint8_t identifications[30];					/* This can be for example ASCII */
	uint8_t number_of_bytes;  /*numbers of identifications*/
	uint8_t from_ecu_address;						/* From which ECU came this message */
};
/* PGN: 0x00FDC5 - Storing the ECU identification from the reading process */
struct ECU_identification {
	uint8_t number_of_bytes;					/* The real length of the fields - Not part of J1939 standard, only for the user */
	uint8_t ecu_part_number[30];					/* ASCII field */
	uint8_t ecu_part_number_bytes;
	uint8_t ecu_serial_number[30];					/* ASCII field */
	uint8_t ecu_serial_number_bytes;
	uint8_t ecu_type[30];							/* ASCII field */
	uint8_t ecu_type_bytes;
	uint8_t from_ecu_address;						/* From which ECU came this message */
};
/* PGN: 0x00EE00 - Storing the Address claimed from the reading process */
struct Name {
	uint32_t identity_number;						/* Specify the ECU serial ID - 0 to 2097151 */
	uint16_t manufacturer_code;						/* Specify the ECU manufacturer code - 0 to 2047 */
	uint8_t function_instance;						/* Specify the ECU function number - 0 to 31 */
	uint8_t ECU_instance;							/* Specify the ECU number - 0 to 7 */
	uint8_t function;								/* Specify the ECU function - 0 to 255 */
	uint8_t vehicle_system;							/* Specify the type of vehicle where ECU is located - 0 to 127 */
	uint8_t arbitrary_address_capable;				/* Specify if the ECU have right to change address if addresses conflicts - 0 to 1 */
	uint8_t industry_group;							/* Specify the group where this ECU is located - 0 to 7 */
	uint8_t vehicle_system_instance;				/* Specify the vehicle system number - 0 to 15 */
	uint8_t from_ecu_address;						/* From which ECU came this message */
};
/* Storing the identifications from the reading process */
struct Identifications {
	struct Software_identification software_identification;
	struct ECU_identification ecu_identification;
//	struct Component_identification component_identification;
};

/* This struct is used for save information and load information from hard drive/SD-card/flash etc. due to the large size of J1939 */
typedef struct{
	struct Name this_name;
	uint8_t this_ECU_address;
	struct Identifications this_identifications;
} Information_this_SCU;
/* PGN: 0x00EC00 - Storing the Transport Protocol Connection Management from the reading process */
struct TP_CM {
	uint8_t control_byte;							/* What type of message are we going to send */
	uint16_t index_message_sent;
	uint16_t total_message_size;					/* Total bytes our complete message includes - 9 to 1785 */
	uint8_t index_of_packages;
	uint8_t number_of_packages;						/* How many times we are going to send packages via TP_DT - 2 to 224 because 1785/8 is 224 rounded up */
	uint32_t PGN_of_the_packeted_message;			/* Our message is going to activate a PGN */
	uint8_t from_ecu_address;						/* From which ECU came this message */
};

/* PGN: 0x00EB00 - Storing the Transport Protocol Data Transfer from the reading process */
struct TP_DT {
	uint8_t sequence_number;						/* When this sequence number is the same as number_of_packages from TP_CM, then we have our complete message */
	uint8_t data[1785];								/* This is the collected data we are going to send. Also we are using this as a filler */
	uint8_t from_ecu_address;						/* From which ECU came this message */
};
typedef enum
{
	CTS_UNCONNECT,  //无通讯	，无连接
	CTS_CONNECTING, //有通讯，连接建立，等后续CTS指令,如等不到2.5s后取消连接，用于data5和data7
	CTS_TRANSMIT,   //接收到CTS后开始发送后续字节，也可用于指示通讯状态忙，用于data5和data7
	CTS_SHUTOFF,    //用于对取消连接的回应
	TRANSMIT,
}cts_status_type;

typedef enum
{
	LOCATION_INTAKE,
	LOCATION_OUTLET
} j1939_location_address;
typedef struct
{
	uint32_t id;
	uint8_t data[8];
	uint8_t scu_address;
	scu_data1_signal_values_type message_data1;
	scu_data2_heater_ratio_deviation_type message_data2;
	scu_data3_correction_factors_type message_data3;
	Information_this_SCU information_this_ECU;
	j1939_data_flag_type data_flag_struct;
	struct TP_CM from_other_ecu_tp_cm;
		/* Temporary hold this values for this ECU when we are going to send data */
	struct TP_CM this_ecu_tp_cm;
	struct TP_DT this_ecu_tp_dt;
	cts_status_type cts_status;
	uint16_t cts_timeout_count;
	uint16_t request_message_timebase_compare;
} J1939;
extern J1939 J1939_struct;
void j1939_struct_init(J1939 *j1939, j1939_location_address location);
uint8_t j1939_normal_transmit_data(J1939 *j1939);
void sae_j1939_listen_for_message(can_handle_type *can_handle, J1939 *j1939);
#endif
