#include "sae_j1939.h"

#include "soft_timer.h"
void sae_j1939_send_data1(J1939 *j1939, uint8_t hardware_error);
void sae_j1939_send_data2(J1939 *j1939, uint8_t hardware_error);
void sae_j1939_send_data3(J1939 *j1939, uint8_t hardware_error);
ENUM_J1939_STATUS_CODES sae_j1939_send_transport_protocol_data_transfer(J1939 *j1939, uint8_t DA);
ENUM_J1939_STATUS_CODES SAE_J1939_Response_Request_Software_Identification(J1939* j1939, uint8_t DA);
ENUM_J1939_STATUS_CODES SAE_J1939_Response_Request_ECU_Identification(J1939* j1939, uint8_t DA);
void SAE_J1939_Read_Transport_Protocol_Connection_Management(J1939 *j1939, uint8_t SA, uint8_t data[]);
J1939 J1939_struct;
void j1939_struct_init(J1939 *j1939, j1939_location_address location)
{
	if (location == LOCATION_INTAKE)
	{
		j1939->scu_address = 0x51;
		j1939->message_data1.pgn = 0xF00E;
		j1939->message_data1.scu_DA = 0x51;
		j1939->message_data2.pgn = 0xFD11;
		j1939->message_data2.scu_DA = 0x51;
		j1939->message_data3.pgn = 0xFD10;
		j1939->message_data3.scu_DA = 0x51;
		j1939->information_this_ECU.this_ECU_address = 0x51;
	}
	else
	{
		j1939->scu_address = 0x52;
		j1939->message_data1.pgn = 0xF00F;
		j1939->message_data1.scu_DA = 0x52;
		j1939->message_data2.pgn = 0xFD0F;
		j1939->message_data2.scu_DA = 0x52;
		j1939->message_data3.pgn = 0xFD0E;
		j1939->message_data3.scu_DA = 0x52;
		j1939->information_this_ECU.this_ECU_address = 0x52;
	}
	uint8_t ecu_part_number[20] = "000000000000*";
	uint8_t ecu_serial_number[20] = "1912083542**";
	uint8_t ecu_type[20] = "UNI***";
	for (uint8_t i = 0; i < 13; i++)
	{
		j1939->information_this_ECU.this_identifications.ecu_identification.ecu_part_number[i] = ecu_part_number[i];
		j1939->information_this_ECU.this_identifications.ecu_identification.ecu_part_number_bytes++;
		j1939->information_this_ECU.this_identifications.ecu_identification.number_of_bytes++;
	}
	for (uint8_t i = 0; i < 12; i++)
	{
		j1939->information_this_ECU.this_identifications.ecu_identification.ecu_serial_number[i] = ecu_serial_number[i];
		j1939->information_this_ECU.this_identifications.ecu_identification.ecu_serial_number_bytes++;
		j1939->information_this_ECU.this_identifications.ecu_identification.number_of_bytes++;
	}
	for (uint8_t i = 0; i < 6; i++)
	{
		j1939->information_this_ECU.this_identifications.ecu_identification.ecu_type[i] = ecu_type[i];
		j1939->information_this_ECU.this_identifications.ecu_identification.ecu_type_bytes++;
		j1939->information_this_ECU.this_identifications.ecu_identification.number_of_bytes++;
	}
	
}
uint8_t j1939_normal_transmit_data(J1939 *j1939)
{
	static uint32_t now_time = 0;
	static uint32_t on_request_time = 0;
	static uint32_t cyclic_time = 0;
	static uint32_t cyclic_message_count = 0;
	now_time = soft_timer_tick_counter_get();
	if (now_time - cyclic_time >= 1)
	{
		cyclic_time = soft_timer_tick_counter_get();
		cyclic_message_count++;
		cyclic_message_count %= 1000;
		if ((j1939->cts_timeout_count >= CTS_TIME_OUT) && (j1939->cts_status == CTS_TRANSMIT))
		{
			j1939->cts_status = CTS_SHUTOFF;
			j1939->cts_timeout_count = CTS_TIME_OUT;
		}
		else
		{
			j1939->cts_timeout_count++;
		}
	}
	else { return 0; }
	if ((cyclic_message_count % 50) == 0) // Data1
	{
		j1939->data_flag_struct.bit.data1_cyclic_flag = 1;
		if (j1939->data_flag_struct.bit.data1_cyclic_flag == 1)
		{
			j1939->data_flag_struct.bit.data1_cyclic_flag = 0;
			sae_j1939_send_data1(j1939, 0);
		}
	}
	else if (cyclic_message_count == 1) // Data2 before dew point or on request
	{
		j1939->data_flag_struct.bit.data2_cyclic_flag = 1;
		if (j1939->data_flag_struct.bit.data2_cyclic_flag == 1)
		{
			j1939->data_flag_struct.bit.data2_cyclic_flag = 0;
			sae_j1939_send_data2(j1939, 0);
		}
	}
	else if (now_time - on_request_time >= j1939->request_message_timebase_compare)
	{
		on_request_time = soft_timer_tick_counter_get();
		if (j1939->data_flag_struct.bit.data2_on_request_flag == 1)
		{
			j1939->data_flag_struct.bit.data2_on_request_flag = 0;
			sae_j1939_send_data2(j1939, 0);
		}
		else if (j1939->data_flag_struct.bit.data3_on_request_flag == 1)
		{
			j1939->data_flag_struct.bit.data3_on_request_flag = 0;
			sae_j1939_send_data3(j1939, 0);
		}
		else if (j1939->data_flag_struct.bit.data4_on_request_flag == 1)
		{
			j1939->data_flag_struct.bit.data4_on_request_flag = 0;
			SAE_J1939_Response_Request_Software_Identification(j1939, j1939->scu_address);
		}
		else if (j1939->data_flag_struct.bit.data5_on_request_flag == 1)
		{
			j1939->data_flag_struct.bit.data5_on_request_flag = 0;
			uint8_t DA = j1939->information_this_ECU.this_identifications.ecu_identification.from_ecu_address;
			if (SAE_J1939_Response_Request_ECU_Identification(j1939, DA) == STATUS_SEND_BUSY) // 多帧
			{
				/* Check if we are going to send it directly (BAM) - Else, the TP CM will send a RTS control byte to the other ECU and the ECU will answer with control byte CTS */
				if(j1939->this_ecu_tp_cm.control_byte == CONTROL_BYTE_TP_CM_BAM) 
				{
					j1939->cts_status = TRANSMIT;
					j1939->request_message_timebase_compare = TIM_60_MS;
					j1939->data_flag_struct.bit.data5_cts_on_request_1_flag = 1;
				}
				else
				{
					j1939->cts_status = CTS_CONNECTING;
					j1939->request_message_timebase_compare = TIM_10_MS;
				}
			}
		}
		else if (j1939->data_flag_struct.bit.data5_cts_on_request_1_flag == 1)
		{
			uint8_t DA = j1939->information_this_ECU.this_identifications.ecu_identification.from_ecu_address;
			if (sae_j1939_send_transport_protocol_data_transfer(j1939, DA) == STATUS_SEND_BUSY)
			{
				j1939->data_flag_struct.bit.data5_cts_on_request_1_flag = 1;
			}
			else
			{
				j1939->data_flag_struct.bit.data5_cts_on_request_1_flag = 0;
			}
		}
//		else if (can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_2_flag == 1)
//		{
//			can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_2_flag = 0;
//			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA5_2;
//		}
//		else if (can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_3_flag == 1)
//		{
//			can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_3_flag = 0;
//			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA5_3;
//		}
//		else if (can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_4_flag == 1)
//		{
//			can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_4_flag = 0;
//			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA5_4;
//		}
//		else if (can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_5_flag == 1)
//		{
//			can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_5_flag = 0;
//			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA5_5;
//		}
//		else if (can_handle->sensor_transmit_data_flag_struct.bit.data6_on_request_flag == 1)
//		{
//			can_handle->sensor_transmit_data_flag_struct.bit.data6_on_request_flag = 0;
//			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA6;
//		}
//		else if (can_handle->sensor_transmit_data_flag_struct.bit.data7_on_request_flag == 1)
//		{
//			can_handle->sensor_transmit_data_flag_struct.bit.data7_on_request_flag = 0;
//			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA7;
//		}
//		else if (can_handle->sensor_transmit_data_flag_struct.bit.data7_cts_on_request_1_flag == 1)
//		{
//			can_handle->sensor_transmit_data_flag_struct.bit.data7_cts_on_request_1_flag = 0;
//			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA7_1;
//		}
//		else if (can_handle->sensor_transmit_data_flag_struct.bit.data7_cts_on_request_2_flag == 1)
//		{
//			can_handle->sensor_transmit_data_flag_struct.bit.data7_cts_on_request_2_flag = 0;
//			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA7_2;
//		}
//		else if (can_handle->sensor_transmit_data_flag_struct.bit.data7_cts_on_request_3_flag == 1)
//		{
//			can_handle->sensor_transmit_data_flag_struct.bit.data7_cts_on_request_3_flag = 0;
//			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA7_3;
//		}
//		else if (can_handle->sensor_transmit_data_flag_struct.bit.data10_on_request_flag == 1)
//		{
//			can_handle->sensor_transmit_data_flag_struct.bit.data10_on_request_flag = 0;
//			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA10;
//		}
//		else if (can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_1_flag == 1)
//		{
//			can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_1_flag = 0;
//			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA10_1;
//		}
//		else if (can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_2_flag == 1)
//		{
//			can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_2_flag = 0;
//			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA10_2;
//		}
//		else if (can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_3_flag == 1)
//		{
//			can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_3_flag = 0;
//			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA10_3;
//		}
//		else if (can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_4_flag == 1)
//		{
//			can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_4_flag = 0;
//			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA10_4;
//		}
//		else if (can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_5_flag == 1)
//		{
//			can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_5_flag = 0;
//			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA10_5;
//		}
//		else if (can_handle->sensor_transmit_data_flag_struct.bit.data_ack == 1)
//		{
//			can_handle->sensor_transmit_data_flag_struct.bit.data_ack = 0;
//			can_handle->message_command_num = CAN_MESSAGE_CMD_ACK;
//		}
//		else if (can_handle->sensor_transmit_data_flag_struct.bit.data_null == 1)
//		{
//			can_handle->sensor_transmit_data_flag_struct.bit.data_null = 0;
//			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
//		}
//		else if (can_handle->sensor_transmit_data_flag_struct.bit.data_busy == 1)
//		{
//			can_handle->sensor_transmit_data_flag_struct.bit.data_busy = 0;
//			can_handle->message_command_num = CAN_MESSAGE_CMD_BUSY;
//		}
	}
	return 1;
}

ENUM_J1939_STATUS_CODES sae_j1939_send_acknowledgement(J1939 *j1939, uint8_t SA, uint8_t control_byte, uint8_t group_function_value, uint32_t PGN_of_requested_info);
void sae_j1939_read_request(J1939 *j1939, uint8_t DA, uint8_t SA, uint8_t data[])
{
	uint32_t PGN = (data[2] << 16) | (data[1] << 8) | data[0];
	if (SA == 0x00 || SA == 0x55 || SA == 0x3D)
	{
		if (PGN == PGN_DATA2_INTAKE && (DA == j1939->scu_address || DA == 0xFF))
		{
			j1939->data_flag_struct.bit.data2_on_request_flag = 1;
		}
		else if (PGN == PGN_DATA2_OUTLET && (DA == j1939->scu_address || DA == 0xFF))
		{
			j1939->data_flag_struct.bit.data2_on_request_flag = 1;
		}
		else if (PGN == PGN_DATA3_INTAKE && (DA == j1939->scu_address || DA == 0xFF))
		{
			j1939->data_flag_struct.bit.data3_on_request_flag = 1;
		}
		else if (PGN == PGN_DATA3_OUTLET && (DA == j1939->scu_address || DA == 0xFF))
		{
			j1939->data_flag_struct.bit.data3_on_request_flag = 1;
		}
		else if (PGN == PGN_DATA4_SOFTWARE_IDENTIFICATION)
		{
			j1939->data_flag_struct.bit.data4_on_request_flag = 1;
			
		}
		else if (PGN == PGN_DATA5_ECU_IDENTIFICATION)
		{
			j1939->data_flag_struct.bit.data5_on_request_flag = 1;
			j1939->information_this_ECU.this_identifications.ecu_identification.from_ecu_address = DA;
		}
		else if (PGN == PGN_DATA6_ADDRESS_CLAIM)
		{
			j1939->data_flag_struct.bit.data6_on_request_flag = 1;
		}
		else if (PGN == PGN_DATA7_DM19)
		{
			j1939->data_flag_struct.bit.data7_on_request_flag = 1;
		}
		else
		{
			if (DA != 0xFF)
			{
				sae_j1939_send_acknowledgement(j1939, SA, CONTROL_BYTE_ACKNOWLEDGEMENT_PGN_NOT_SUPPORTED, GROUP_FUNCTION_VALUE_NO_CAUSE, PGN);
			}
		}
	}
	else
	{
		if (DA != 0xFF)
		{
			sae_j1939_send_acknowledgement(j1939, SA, CONTROL_BYTE_ACKNOWLEDGEMENT_PGN_NOT_SUPPORTED, GROUP_FUNCTION_VALUE_NO_CAUSE, PGN);
		}
	}
}

void sae_j1939_listen_for_message(can_handle_type *can_handle, J1939 *j1939)
{
	static can_receive_message_type rx_message;
	if(can_handle->receive_message_counter == can_handle->receive_message_index )//超过一圈重新造成相等，说明开辟存储深度不足
	{
		return;
	}
//--获取接收数据组--进来之后 RxMsgCounter 总是滞后 RxMsgIndex 1~RX_RECEIVE_NUM值
	rx_message = can_handle->receive_message_link_table[can_handle->receive_message_counter];    //接收数据 RxMsgIndex

	if(++can_handle->receive_message_counter >= RX_RECEIVE_NUM)       //clr
	{
		can_handle->receive_message_counter = 0;
	}
	uint32_t ID = rx_message.id.U32;
	uint8_t id0 = ID >> 24;
	uint8_t id1 = ID >> 16;
	uint8_t DA = ID >> 8; 	/* Destination address which is this ECU. if DA = 0xFF = broadcast to all ECU. Sometimes DA can be an ID number too */
	uint8_t SA = ID; 		/* Source address of the ECU that we got the message from */
	
	if (id0 == 0x18 && id1 == 0xEA && (DA == j1939->scu_address || DA == 0xFF))
	{
		sae_j1939_read_request(j1939, DA, SA, rx_message.data);
	}
	else if(id0 == 0x1C && id1 == 0xEC && DA == j1939->information_this_ECU.this_ECU_address)
	{
		SAE_J1939_Read_Transport_Protocol_Connection_Management(j1939, SA, rx_message.data);
		//			SAE_J1939_Read_Transport_Protocol_Connection_Management(j1939, SA, data);	
	}
	else if (id0 == 0x18 && id1 == 0xEE && DA == 0xFF && SA != 0xFE) // 地址冲突帧
	{
		
	}
	else if (id0 == 0x18 && id1 == 0xEE && DA == 0xFF && SA == 0xFE)
	{
		
	}
}
ENUM_J1939_STATUS_CODES CAN_Send_Message(uint32_t ID, uint8_t data[]) {
	ENUM_J1939_STATUS_CODES status;
	can_transmit_message_type transmit_message;
	transmit_message.id = ID;
	transmit_message.ide = CAN_ID_EXTEND;
	transmit_message.rtr = CAN_RTR_DATA_MODE;
	transmit_message.dlc = 8;
	for (uint8_t i = 0; i < 8; i++)
		transmit_message.data[i] = data[i];
	can_transmit_message(&transmit_message);
	status = STATUS_SEND_OK;
	return status;
}
// 传感器obd can_handle->transmit_message.data2 
// 参数：加热ratio值 运行时间
void sae_j1939_send_data1(J1939 *j1939, uint8_t hardware_error)
{
	uint32_t ID = (0x18 << 24) | (j1939->message_data1.pgn << 8) | j1939->scu_address;
	uint8_t data[8];
	data[0] = j1939->message_data1.nox_concentration_byte_01.bit.l;
	data[1] = j1939->message_data1.nox_concentration_byte_01.bit.h;
	data[2] = j1939->message_data1.o2_concentration_byte_23.bit.l;
	data[3] = j1939->message_data1.o2_concentration_byte_23.bit.h;
	data[4] = j1939->message_data1.status_byte_4.value;
	data[5] = j1939->message_data1.status_byte_5.value;
	data[6] = j1939->message_data1.status_byte_6.value;
	data[7] = j1939->message_data1.status_byte_7.value;
	if((hardware_error & 0x48) == 0x00)
	{
		CAN_Send_Message(ID, data);
	}
}
// 传感器obd can_handle->transmit_message.data2 
// 参数：加热ratio值 运行时间
void sae_j1939_send_data2(J1939 *j1939, uint8_t hardware_error)
{
	uint32_t ID = (0x0C << 24) | (j1939->message_data2.pgn << 8) | j1939->scu_address;
	uint8_t data[8];
	data[0] = j1939->message_data2.heater_ratio_byte_01.bit.l;
	data[1] = j1939->message_data2.heater_ratio_byte_01.bit.h;
	data[2] = j1939->message_data2.nox_correction_gain_byte_23.bit.l;
	data[3] = j1939->message_data2.nox_correction_gain_byte_23.bit.h;
	data[4] = j1939->message_data2.nox_correction_offset_byte_4;
	data[5] = j1939->message_data2.operation_hours_counter_byte_56.bit.l;
	data[6] = j1939->message_data2.operation_hours_counter_byte_56.bit.h;
	data[7] = j1939->message_data2.not_used_byte_7;
	if((hardware_error & 0x48) == 0x00)
	{
		CAN_Send_Message(ID, data);
	}
}
// 传感器obd can_handle->transmit_message.data3
// 参数：加热ratio值 运行时间
void sae_j1939_send_data3(J1939 *j1939, uint8_t hardware_error)
{
	uint32_t ID = (0x0C << 24) | (j1939->message_data3.pgn << 8) | j1939->scu_address;
	uint8_t data[8];
	data[0] = j1939->message_data3.correction_pressure_lambda_byte_0;
	data[1] = j1939->message_data3.correction_pressure_nox_byte_1;
	data[2] = j1939->message_data3.correction_no2_byte_2;
	data[3] = j1939->message_data3.correction_nh3_byte_3;
	data[4] = j1939->message_data3.self_diagnosis_result_value_byte_4;
	data[5] = 0xFF;
	data[6] = 0xFF;
	data[7] = 0xFF;
	if((hardware_error & 0x48) == 0x00)
	{
		CAN_Send_Message(ID, data);
	}
}
/*
 * Send back a acknowledgement to other ECU about the their PGN request
 * PGN: 0x00E800 (59392)
 */
ENUM_J1939_STATUS_CODES sae_j1939_send_acknowledgement(J1939 *j1939, uint8_t SA, uint8_t control_byte, uint8_t group_function_value, uint32_t PGN_of_requested_info)
{
	uint32_t ID = (0x18E8 << 16) | (0xFF << 8) | j1939->scu_address;
	uint8_t data[8];
	data[0] = control_byte;
	data[1] = group_function_value;														/* The cause of the control byte */
	data[2] = 0xFF;																		/* Reserved */
	data[3] = 0xFF;																		/* Reserved */
	data[4] = SA;													/* This source address */
	data[5] = PGN_of_requested_info;
	data[6] = PGN_of_requested_info >> 8;
	data[7] = PGN_of_requested_info >> 16;
	return CAN_Send_Message(ID, data);
}
/*
 * Send information to other ECU about how much sequence data packages this ECU is going to send to other ECU
 * PGN: 0x00EC00 (60416)
 */
ENUM_J1939_STATUS_CODES SAE_J1939_Send_Transport_Protocol_Connection_Management(J1939 *j1939, uint8_t DA) {
	uint32_t ID = (0x1CEC << 16) | (DA << 8) | j1939->information_this_ECU.this_ECU_address;
	uint8_t data[8];
	data[0] = j1939->this_ecu_tp_cm.control_byte;
	data[1] = j1939->this_ecu_tp_cm.total_message_size;
	data[2] = j1939->this_ecu_tp_cm.total_message_size >> 8;
	data[3] = j1939->this_ecu_tp_cm.number_of_packages;
	data[4] = 0xFF; 															/* Reserved */
	data[5] = j1939->this_ecu_tp_cm.PGN_of_the_packeted_message;
	data[6] = j1939->this_ecu_tp_cm.PGN_of_the_packeted_message >> 8;
	data[7] = j1939->this_ecu_tp_cm.PGN_of_the_packeted_message >> 16;
	return CAN_Send_Message(ID, data);
}
/*
 * Send sequence data packages to other ECU that we have loaded
 * PGN: 0x00EB00 (60160)
 */
ENUM_J1939_STATUS_CODES sae_j1939_send_transport_protocol_data_transfer(J1939 *j1939, uint8_t DA)
{
	uint32_t ID = (0x1CEB << 16) | (DA << 8) | j1939->information_this_ECU.this_ECU_address;
	uint8_t package[8];
	ENUM_J1939_STATUS_CODES status = STATUS_SEND_OK;
	if (j1939->this_ecu_tp_cm.index_of_packages <= j1939->this_ecu_tp_cm.number_of_packages)
	{
		package[0] = j1939->this_ecu_tp_cm.index_of_packages; /* Number of package */
		for(uint8_t j = 0; j < 7; j++)
		{
			if(j1939->this_ecu_tp_cm.index_message_sent < j1939->this_ecu_tp_cm.total_message_size)
				package[j+1] = j1939->this_ecu_tp_dt.data[j1939->this_ecu_tp_cm.index_message_sent++]; /* Data that we have collected */
			 else
				package[j+1] = 0xFF; /* Reserved */
		}
		CAN_Send_Message(ID, package);
		j1939->this_ecu_tp_cm.index_of_packages++;
		status = STATUS_SEND_BUSY;
		return status;
	}
	else
		return status;
}
/*
 * Send sequence data packages to other ECU that we have loaded
 * PGN: 0x00EB00 (60160)
 */
ENUM_J1939_STATUS_CODES SAE_J1939_Send_Transport_Protocol_Data_Transfer(J1939 *j1939, uint8_t DA){
	uint32_t ID = (0x1CEB << 16) | (DA << 8) | j1939->information_this_ECU.this_ECU_address;
	uint8_t package[8];
	uint16_t bytes_sent = 0;
	ENUM_J1939_STATUS_CODES status = STATUS_SEND_OK;
	for(uint8_t i = 1; i <= j1939->this_ecu_tp_cm.number_of_packages; i++) {
		package[0] = i; 																	/* Number of package */
		for(uint8_t j = 0; j < 7; j++)
			if(bytes_sent < j1939->this_ecu_tp_cm.total_message_size)
				package[j+1] = j1939->this_ecu_tp_dt.data[bytes_sent++];					/* Data that we have collected */
			 else
				package[j+1] = 0xFF; 														/* Reserved */

		status = CAN_Send_Message(ID, package);
		if(status != STATUS_SEND_OK)
			return status;
	}
	return status;
}
/*
 * Store information about sequence data packages from other ECU who are going to send to this ECU
 * PGN: 0x00EC00 (60416)
 */
void SAE_J1939_Read_Transport_Protocol_Connection_Management(J1939 *j1939, uint8_t SA, uint8_t data[]) {
	j1939->from_other_ecu_tp_cm.control_byte = data[0];
	j1939->from_other_ecu_tp_cm.total_message_size = (data[2] << 8) | data[1];
	j1939->from_other_ecu_tp_cm.number_of_packages = data[3];
	j1939->from_other_ecu_tp_cm.PGN_of_the_packeted_message = (data[7] << 16) | (data[6] << 8) | data[5];
	j1939->from_other_ecu_tp_cm.from_ecu_address = SA;
	/* When we answer with CTS, it means we are going to send the Transport Protocol Data Transfer package */
	if(j1939->from_other_ecu_tp_cm.control_byte == CONTROL_BYTE_TP_CM_CTS)
		j1939->data_flag_struct.bit.data5_cts_on_request_1_flag = 1;
}

/*
 * Response the request of the software identification about this ECU
 * PGN: 0x00FEDA (65242)
 */
ENUM_J1939_STATUS_CODES SAE_J1939_Response_Request_Software_Identification(J1939* j1939, uint8_t DA)
{
	uint8_t number_of_fields = j1939->information_this_ECU.this_identifications.software_identification.number_of_fields;
	uint8_t number_of_bytes = j1939->information_this_ECU.this_identifications.software_identification.number_of_bytes;
	if (number_of_bytes < 8) {
		uint32_t ID = (0x18FEDA << 8) | j1939->information_this_ECU.this_ECU_address;
		uint8_t data[8];
		data[0] = number_of_fields;
		for(uint8_t i = 0; i < 7; i++)
			data[i+1] = j1939->information_this_ECU.this_identifications.software_identification.identifications[i];
		CAN_Send_Message(ID, data);
		return STATUS_SEND_OK;
	} else {
		/* Multiple messages - Load data */
		j1939->this_ecu_tp_cm.total_message_size = 0;
		j1939->this_ecu_tp_dt.data[j1939->this_ecu_tp_cm.total_message_size++] = number_of_fields;
		for(uint8_t i = 0; i < number_of_bytes; i++)
			j1939->this_ecu_tp_dt.data[j1939->this_ecu_tp_cm.total_message_size++] = j1939->information_this_ECU.this_identifications.software_identification.identifications[i];

		/* Send TP CM */
		j1939->this_ecu_tp_cm.number_of_packages = j1939->this_ecu_tp_cm.total_message_size % 8 > 0 ? j1939->this_ecu_tp_cm.total_message_size/8 + 1 : j1939->this_ecu_tp_cm.total_message_size/8; /* Rounding up */
		j1939->this_ecu_tp_cm.PGN_of_the_packeted_message = PGN_DATA4_SOFTWARE_IDENTIFICATION;
		j1939->this_ecu_tp_cm.index_of_packages = 1;
		j1939->this_ecu_tp_cm.index_message_sent = 0;
		j1939->this_ecu_tp_cm.control_byte = DA == 0xFF ? CONTROL_BYTE_TP_CM_BAM : CONTROL_BYTE_TP_CM_RTS; /* If broadcast, then use BAM control byte */
		SAE_J1939_Send_Transport_Protocol_Connection_Management(j1939, DA);

		/* Check if we are going to send it directly (BAM) - Else, the TP CM will send a RTS control byte to the other ECU and the ECU will answer with control byte CTS */
		if(j1939->this_ecu_tp_cm.control_byte == CONTROL_BYTE_TP_CM_BAM)
		{
			SAE_J1939_Send_Transport_Protocol_Data_Transfer(j1939, DA);
			return STATUS_SEND_OK;
		}
		return STATUS_SEND_BUSY;
	}
}

/*
 * Response the request of the ECU identification about this ECU
 * PGN: 0x00FDC5 (64965)
 */
ENUM_J1939_STATUS_CODES SAE_J1939_Response_Request_ECU_Identification(J1939* j1939, uint8_t DA)
{
	/* Find the length of the array fields */
	uint8_t number_of_bytes = j1939->information_this_ECU.this_identifications.ecu_identification.number_of_bytes;
	uint8_t ecu_part_number_bytes = j1939->information_this_ECU.this_identifications.ecu_identification.ecu_part_number_bytes;
	uint8_t ecu_serial_number_bytes = j1939->information_this_ECU.this_identifications.ecu_identification.ecu_serial_number_bytes;
	uint8_t ecu_type_bytes = j1939->information_this_ECU.this_identifications.ecu_identification.ecu_type_bytes;
	if (number_of_bytes < 9) {
		/* If each field have the length 1, then we can send ECU identification as it was a normal message */
		uint32_t ID = (0x18FDC5 << 8) | j1939->information_this_ECU.this_ECU_address;
		uint8_t data[8];
		for (uint8_t i = 0; i < ecu_part_number_bytes; i++)
		{
			data[i] = j1939->information_this_ECU.this_identifications.ecu_identification.ecu_part_number[i];
		}
		for (uint8_t i = 0; i < ecu_serial_number_bytes; i++)
		{
			data[i + ecu_part_number_bytes] = j1939->information_this_ECU.this_identifications.ecu_identification.ecu_serial_number[i];
		}
		for (uint8_t i = 0; i < ecu_type_bytes; i++)
		{
			data[i + ecu_serial_number_bytes + ecu_part_number_bytes] = j1939->information_this_ECU.this_identifications.ecu_identification.ecu_type[i];
		}
		CAN_Send_Message(ID, data);
		return STATUS_SEND_OK;
	} else {
		/* Multiple messages - Load data */
		j1939->this_ecu_tp_cm.total_message_size = 0;
		for (uint8_t i = 0; i < ecu_part_number_bytes; i++)
		{
			j1939->this_ecu_tp_dt.data[j1939->this_ecu_tp_cm.total_message_size++] = j1939->information_this_ECU.this_identifications.ecu_identification.ecu_part_number[i];
		}
		for (uint8_t i = 0; i < ecu_serial_number_bytes; i++)
		{
			j1939->this_ecu_tp_dt.data[j1939->this_ecu_tp_cm.total_message_size++] = j1939->information_this_ECU.this_identifications.ecu_identification.ecu_serial_number[i];
		}
		for (uint8_t i = 0; i < ecu_type_bytes; i++)
		{
			j1939->this_ecu_tp_dt.data[j1939->this_ecu_tp_cm.total_message_size++] = j1939->information_this_ECU.this_identifications.ecu_identification.ecu_type[i];
		}

		/* Send TP CM */
		j1939->this_ecu_tp_cm.number_of_packages = j1939->this_ecu_tp_cm.total_message_size % 7 > 0 ? j1939->this_ecu_tp_cm.total_message_size/7 + 1 : j1939->this_ecu_tp_cm.total_message_size/7; /* Rounding up */
		j1939->this_ecu_tp_cm.PGN_of_the_packeted_message = PGN_DATA5_ECU_IDENTIFICATION;
		j1939->this_ecu_tp_cm.index_of_packages = 1;
		j1939->this_ecu_tp_cm.index_message_sent = 0;
		j1939->this_ecu_tp_cm.control_byte = DA == 0xFF ? CONTROL_BYTE_TP_CM_BAM : CONTROL_BYTE_TP_CM_RTS; /* If broadcast, then use BAM control byte */
		ENUM_J1939_STATUS_CODES status = SAE_J1939_Send_Transport_Protocol_Connection_Management(j1939, DA);

		/* Check if we are going to send it directly (BAM) - Else, the TP CM will send a RTS control byte to the other ECU and the ECU will answer with control byte CTS */
//		if(j1939->this_ecu_tp_cm.control_byte == CONTROL_BYTE_TP_CM_BAM)
//		{
//			SAE_J1939_Send_Transport_Protocol_Data_Transfer(j1939, DA);
//			return STATUS_SEND_OK;
//		}
		return STATUS_SEND_BUSY;
	}
}

/*
 * Response the request address claimed about this ECU to all ECU - Broadcast. This function must be called at the ECU start up according to J1939 standard
 * PGN: 0x00EE00 (60928)
 */
ENUM_J1939_STATUS_CODES SAE_J1939_Response_Request_Address_Claimed(J1939 *j1939) {
	uint32_t ID = (0x18EEFF << 8) | j1939->information_this_ECU.this_ECU_address;
	uint8_t data[8];
	data[0] = j1939->information_this_ECU.this_name.identity_number;
	data[1] = j1939->information_this_ECU.this_name.identity_number >> 8;
	data[2] = (j1939->information_this_ECU.this_name.identity_number >> 16) |  (j1939->information_this_ECU.this_name.manufacturer_code << 5);
	data[3] = j1939->information_this_ECU.this_name.manufacturer_code >> 3;
	data[4] = (j1939->information_this_ECU.this_name.function_instance << 3) | j1939->information_this_ECU.this_name.ECU_instance;
	data[5] = j1939->information_this_ECU.this_name.function;
	data[6] = j1939->information_this_ECU.this_name.vehicle_system << 1;
	data[7] = (j1939->information_this_ECU.this_name.arbitrary_address_capable << 7) | (j1939->information_this_ECU.this_name.industry_group << 4) | j1939->information_this_ECU.this_name.vehicle_system_instance;
	return CAN_Send_Message(ID, data);
}
/*
 * Send Address Not Claimed if the address conflicts with each other
 * PGN: 0x00EE00 (60928)
 */
ENUM_J1939_STATUS_CODES SAE_J1939_Send_Address_Not_Claimed(J1939 *j1939) {
	uint32_t ID = 0x18EEFFFE;
	uint8_t data[8];
	data[0] = j1939->information_this_ECU.this_name.identity_number;
	data[1] = j1939->information_this_ECU.this_name.identity_number >> 8;
	data[2] = (j1939->information_this_ECU.this_name.identity_number >> 16) |  (j1939->information_this_ECU.this_name.manufacturer_code << 5);
	data[3] = j1939->information_this_ECU.this_name.manufacturer_code >> 3;
	data[4] = (j1939->information_this_ECU.this_name.function_instance << 3) | j1939->information_this_ECU.this_name.ECU_instance;
	data[5] = j1939->information_this_ECU.this_name.function;
	data[6] = j1939->information_this_ECU.this_name.vehicle_system << 1;
	data[7] = (j1939->information_this_ECU.this_name.arbitrary_address_capable << 7) | (j1939->information_this_ECU.this_name.industry_group << 4) | j1939->information_this_ECU.this_name.vehicle_system_instance;
	return CAN_Send_Message(ID, data);
}
