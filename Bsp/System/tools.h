/* USER CODE END Header */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TOOLS_H__
#define __TOOLS_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"
// 指数加权平均
typedef struct {
	float shadow;
	float decay;
}exponential_average_type;
float exponential_weighted_float(exponential_average_type *exponent, float getData);

#endif
