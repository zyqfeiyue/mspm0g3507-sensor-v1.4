#include "sensor_obd.h"
#include "heater_control.h"
#define DATA2_IP1_MA_MAX (6500)
#define DATA2_IP1_MA_MIN (-3000)
#define DATA2_IP2_NA_MAX (11637)
#define DATA2_IP2_NA_MIN (-1000)
#define DATA2_VS_MV_DELTA (50)
#define DATA2_JUDGE_TIME (20)
#define DIAGNOSIS_JUDGE_TIMES (20)
//static uint8_t o2_status_count = 0, nox_status_count = 0, ip1_in_range_flag = 0;

sensor_obd_type sensor_obd_struct;
void sensor_obd_task(sensor_handle_type *sensor_handle, sensor_obd_type *sensor_obd, system_type * system, uint8_t mode)
{
	sensor_data_type *sensor_data = &sensor_handle->sensor_data_struct;
	sensor_hardware_type *sensor_hardware = &sensor_handle->sensor_hardware_struct;
	if (sensor_obd->status_diagnosis_flag == SYSTEM_STATE_ENABLE)
	{
		if (sensor_data->ip1_data >= DATA2_IP1_MA_MAX || sensor_data->ip1_data <= DATA2_IP1_MA_MIN)
		{
			sensor_obd->o2_status_count++;
		}
		else
		{
			sensor_obd->o2_status_count = 0;
		}
		if (sensor_data->ip2_data >= DATA2_IP2_NA_MAX || sensor_data->ip2_data <= DATA2_IP2_NA_MIN)
		{
			sensor_obd->nox_status_count++;
		}
		else
		{
			sensor_obd->nox_status_count = 0;
		}
		if (sensor_data->vs_data >= (425 + DATA2_VS_MV_DELTA) || sensor_data->vs_data <= (425 - DATA2_VS_MV_DELTA))
		{
			sensor_obd->vs_in_range_flag = 0;
		}
		else
		{
			sensor_obd->vs_in_range_flag = 1;
		}
		if (sensor_obd->o2_status_count >= DATA2_JUDGE_TIME || sensor_obd->vs_in_range_flag == 0)
		{
			sensor_obd->o2_status_count = DATA2_JUDGE_TIME;
			sensor_obd->data1_signal_struct.status_byte_4.bit.o2_reading_stable = NOT_IN_RANGE_DATA1;
		}
		else
		{
			sensor_obd->data1_signal_struct.status_byte_4.bit.o2_reading_stable = IN_RANGE_DATA1;
		}
		// nox_reading_stable
		if (sensor_obd->nox_status_count >= DATA2_JUDGE_TIME || sensor_obd->vs_in_range_flag == 0)
		{
			sensor_obd->nox_status_count = DATA2_JUDGE_TIME;
			sensor_obd->data1_signal_struct.status_byte_4.bit.nox_reading_stable = NOT_IN_RANGE_DATA1;
		}
		else
		{
			sensor_obd->data1_signal_struct.status_byte_4.bit.nox_reading_stable = IN_RANGE_DATA1;
		}
	}
	if ((sensor_hardware->hardware_error & 0xF0) == 0x00) // 电压在正常范围内
		sensor_obd->data1_signal_struct.status_byte_4.bit.sensor_power = IN_RANGE_DATA1;
	else if ((sensor_hardware->hardware_error & 0xB0) != 0x00) // 小于10v不能正常工作
		sensor_obd->data1_signal_struct.status_byte_4.bit.sensor_power = NOT_IN_RANGE_DATA1;
	if (mode >= HEATER_MODE_3)
		sensor_obd->data1_signal_struct.status_byte_4.bit.sensor_at_temperature = IN_RANGE_DATA1;
	else
		sensor_obd->data1_signal_struct.status_byte_4.bit.sensor_at_temperature = NOT_IN_RANGE_DATA1;
	if(mode == HEATER_MODE_IDLE )
	{
		sensor_obd->data1_signal_struct.status_byte_5.bit.status_heater_mode = HEATER_OFF_DATA1;//11//Heater off
	}
	else if (mode == HEATER_MODE_2)
	{
		sensor_obd->data1_signal_struct.status_byte_5.bit.status_heater_mode = HEAT_UP_SLOPE_3_4_DATA1;//11//Heater 3
	}
	else if (mode == HEATER_MODE_1)
	{
		sensor_obd->data1_signal_struct.status_byte_5.bit.status_heater_mode = HEAT_UP_SLOPE_1_2_DATA1;//11//Heater 1 2
	}
	else
	{
		sensor_obd->data1_signal_struct.status_byte_5.bit.status_heater_mode = AUTOMATIC_MODE_DATA1;
	}
	sensor_obd->data1_signal_struct.status_byte_5.bit.not_used = 1;
	
	if(sensor_obd->sensor_diagnosis.bit.heater_short == 1)
		sensor_obd->data1_signal_struct.status_byte_5.bit.error_heater = SHORT_CIRCUIT_DATA1;
	else if(sensor_obd->sensor_diagnosis.bit.heater_openload == 1)
		sensor_obd->data1_signal_struct.status_byte_5.bit.error_heater = OPEN_WIRE_DATA1;
	else
		sensor_obd->data1_signal_struct.status_byte_5.bit.error_heater = NO_ERROR_DATA1;
	if (sensor_obd->sensor_diagnosis.bit.nox_openload == 1)
		sensor_obd->data1_signal_struct.status_byte_6.bit.error_nox = OPEN_WIRE_DATA1;
	else if (sensor_obd->sensor_diagnosis.bit.nox_short == 1)
		sensor_obd->data1_signal_struct.status_byte_6.bit.error_nox = SHORT_CIRCUIT_DATA1;
	else
		sensor_obd->data1_signal_struct.status_byte_6.bit.error_nox = NO_ERROR_DATA1;
	sensor_obd->data1_signal_struct.status_byte_6.bit.status_diagnosis_feedback = DIAGNOSIS_NOT_SUPPORTED_DATA1;
	if (sensor_obd->sensor_diagnosis.bit.o2_openload == 1)
		sensor_obd->data1_signal_struct.status_byte_7.bit.error_o2 = OPEN_WIRE_DATA1;
	else if (sensor_obd->sensor_diagnosis.bit.o2_short == 1)
		sensor_obd->data1_signal_struct.status_byte_7.bit.error_o2 = SHORT_CIRCUIT_DATA1;
	else
		sensor_obd->data1_signal_struct.status_byte_7.bit.error_o2 = NO_ERROR_DATA1;
	sensor_obd->data1_signal_struct.status_byte_7.bit.not_used = 7;
	// 短路检测
	if (sensor_obd->vs_diagnosis_flag == SYSTEM_STATE_ENABLE)
	{
		if (sensor_data->vs_s_f_data >= 2500) // ip1断路 大于4V
		{
			sensor_obd->vs_status_count++;
		}
		else if (sensor_data->vs_s_f_data < -2200) // ip2短路
		{
			sensor_obd->vs_status_count++;
		}
		else
		{
			sensor_obd->vs_status_count = 0;
		}
		if (sensor_obd->vs_status_count >= DIAGNOSIS_JUDGE_TIMES)
		{
			sensor_obd->vs_status_count = DIAGNOSIS_JUDGE_TIMES;
			if (sensor_data->vs_s_f_data >= 2500)
			{
				sensor_obd->sensor_diagnosis.bit.o2_openload = 1;
			}
			else if (sensor_data->vs_s_f_data < -2200)
			{
				sensor_obd->sensor_diagnosis.bit.o2_short = 1;
			}
			system->dewpoint_command = false;
		}
		else
		{
			sensor_obd->sensor_diagnosis.bit.o2_openload = 0;
			sensor_obd->sensor_diagnosis.bit.o2_short = 0;
		}
	}
	if (sensor_obd->ip1_diagnosis_flag == SYSTEM_STATE_ENABLE)
	{
		if (sensor_data->ip1_s_data >= 4900) // ip1断路 大于4V
		{
			sensor_obd->ip1_status_count++;
		}
		else if (sensor_data->ip1_s_data < 50) // ip2短路
		{
			sensor_obd->ip1_status_count++;
		}
		else
		{
			sensor_obd->ip1_status_count = 0;
		}
		if (sensor_obd->ip1_status_count >= DIAGNOSIS_JUDGE_TIMES)
		{
			sensor_obd->ip1_status_count = DIAGNOSIS_JUDGE_TIMES;
			if (sensor_data->ip1_s_data >= 4900)
				sensor_obd->sensor_diagnosis.bit.o2_openload = 1;
			else if (sensor_data->ip1_s_data < 50)
				sensor_obd->sensor_diagnosis.bit.o2_short = 1;
		}
		else
		{
			sensor_obd->sensor_diagnosis.bit.o2_openload = 0;
			sensor_obd->sensor_diagnosis.bit.o2_short = 0;
		}
	}
	if (sensor_obd->ip2_diagnosis_flag == SYSTEM_STATE_ENABLE)
	{
		if (sensor_data->ip2n_data >= 3200) // ip2断路 大于3.2V
		{
			sensor_obd->ip2_status_count++;
		}
		else if (sensor_data->ip2n_data < 50) // ip2短路
		{
			sensor_obd->ip2_status_count++;
		}
		else
		{
			sensor_obd->ip2_status_count = 0;
		}
		if (sensor_obd->ip2_status_count >= DIAGNOSIS_JUDGE_TIMES)
		{
			sensor_obd->ip2_status_count = DIAGNOSIS_JUDGE_TIMES;
			if (sensor_data->ip2n_data >= 3200)
				sensor_obd->sensor_diagnosis.bit.nox_openload = 1;
			else if (sensor_data->ip2n_data < 50)
				sensor_obd->sensor_diagnosis.bit.nox_short = 1;
		}
		else
		{
			sensor_obd->sensor_diagnosis.bit.nox_openload = 0;
			sensor_obd->sensor_diagnosis.bit.nox_short = 0;
		}
	}
}

void sensor_obd_status_diagnosis(sensor_obd_type *sensor_obd, system_state_enum enable)
{
	sensor_obd->status_diagnosis_flag = enable;
}

void sensor_obd_vs_diagnosis(sensor_obd_type *sensor_obd, system_state_enum enable)
{
	sensor_obd->vs_diagnosis_flag = enable;
}

void sensor_obd_ip2_diagnosis(sensor_obd_type *sensor_obd, system_state_enum enable)
{
	sensor_obd->ip2_diagnosis_flag = enable;
}

void sensor_obd_ip1_diagnosis(sensor_obd_type *sensor_obd, system_state_enum enable)
{
	sensor_obd->ip1_diagnosis_flag = enable;
}
