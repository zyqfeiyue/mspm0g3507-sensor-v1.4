/* USER CODE END Header */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PUMP_CONTROL_H__
#define __PUMP_CONTROL_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc_app.h"
#include "tools.h"
void pump_pid_init(void);


void pump_control(sensor_handle_type *sensor_handle);
#endif
