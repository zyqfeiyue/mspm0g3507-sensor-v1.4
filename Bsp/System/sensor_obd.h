/*
 * can_control.h
 *
 *  Created on: 2021-11-16
 *      Author: zyq
 */

#ifndef SENSOR_OBD_H_
#define SENSOR_OBD_H_
#include "main.h"
#include "adc_app.h"
#include "system_state.h"
#define NOT_IN_RANGE_DATA1	(uint8_t)0
#define IN_RANGE_DATA1 			(uint8_t)1
#define ERROR_DATA1 				(uint8_t)2
#define NOT_AVAILABLE_DATA1	(uint8_t)3

#define AUTOMATIC_MODE_DATA1		(uint8_t)0
#define HEAT_UP_SLOPE_3_4_DATA1	(uint8_t)1
#define HEAT_UP_SLOPE_1_2_DATA1	(uint8_t)2
#define HEATER_OFF_DATA1				(uint8_t)3

#define SHORT_CIRCUIT_DATA1	(uint8_t)3
#define OPEN_WIRE_DATA1			(uint8_t)5
#define NO_ERROR_DATA1			(uint8_t)31

#define DIAGNOSIS_NOT_ACTIVE_DATA1		(uint8_t)0
#define DIAGNOSIS_ACTIVE_DATA1				(uint8_t)1
#define DIAGNOSIS_COMPLETED_DATA1			(uint8_t)2
#define DIAGNOSIS_ABORTED_DATA1				(uint8_t)3
#define DIAGNOSIS_NOT_POSSIBLE_DATA1	(uint8_t)4
#define DIAGNOSIS_NOT_SUPPORTED_DATA1	(uint8_t)7
typedef union
{
	__IO uint8_t value;
	struct
	{
		__IO uint8_t o2_openload     			: 1;
		__IO uint8_t o2_short        			: 1;
		__IO uint8_t nox_openload    			: 1;
		__IO uint8_t nox_short       			: 1;
		__IO uint8_t heater_openload 			: 1;
		__IO uint8_t heater_short    			: 1;
		__IO uint8_t enter_diagnosis_flag	: 1;
		__IO uint8_t other								: 1;
	}bit;
} sensor_diagnosis_status_type;
typedef struct
{
	/**
	* @brief NOx concentration
	*/
  union
  {
    uint16_t value;
    struct
    {
      uint16_t l      : 8; /* [0-7] */ // 低8位
      uint16_t h      : 8; /* [7-15] */ // 高8位
    } bit;
  }nox_concentration_byte_01;
	/**
	* @brief O2 concentration
	*/
  union
  {
    uint16_t value;
    struct
    {
      uint16_t l      : 8; /* [0-7] */ // 低8位
      uint16_t h      : 8; /* [7-15] */ // 高8位
    } bit;
  }o2_concentration_byte_23;
	/**
	* @brief status byte 4
	*/
  union
  {
    uint8_t value;
    struct
    {
      uint8_t sensor_power						: 2; /* [0-1] */ // 传感器供电状态
      uint8_t sensor_at_temperature		: 2; /* [2-3] */ // 传感器温度
			uint8_t nox_reading_stable 			: 2; /* [4-5] */ // NOx浓度状态
			uint8_t o2_reading_stable				: 2; /* [6-7] */ // O2浓度状态
    } bit;
  }status_byte_4;
	/**
	* @brief status byte 5
	*/
  union
  {
    uint8_t value;
    struct
    {
      uint8_t error_heater				: 5; /* [0-4] */ // 加热器错误
      uint8_t status_heater_mode	: 2; /* [5-6] */ // 加热器模式状态
			uint8_t not_used 						: 1; /* [7] */ // 未使用
    } bit;
  }status_byte_5;
	/**
	* @brief status byte 6
	*/
  union
  {
    uint8_t value;
    struct
    {
      uint8_t error_nox									: 5; /* [0-4] */ // NOx错误
      uint8_t status_diagnosis_feedback	: 3; /* [5-6] */ // 诊断模式返回状态
    } bit;
  }status_byte_6;
	/**
	* @brief status byte 7
	*/
  union
  {
    uint8_t value;
    struct
    {
      uint8_t error_o2	: 5; /* [0-4] */ // O2错误
      uint8_t not_used	: 3; /* [5-6] */ // 未使用
    } bit;
  }status_byte_7;
} data1_signal_values_type;

typedef struct
{
	/**
	* @brief heater ratio
	*/
  union
  {
    uint16_t value;
    struct
    {
      uint16_t l      : 8; /* [0-7] */ // 低8位
      uint16_t h      : 8; /* [7-15] */ // 高8位
    } bit;
  } heater_ratio_byte_01; // range:0-8000 data range:0-8 offset:0 lsb:0.001
	/**
	* @brief nox_correction_gain
	*/
  union
  {
    uint16_t value;
    struct
    {
      uint16_t l      : 8; /* [0-7] */ // 低8位
      uint16_t h      : 8; /* [7-15] */ // 高8位
    } bit;
  } nox_correction_gain_byte_23; // range:0-2000 data range:-100-100 offset:-100 lsb:0.1%
	uint8_t nox_correction_offset_byte_4; // range:0-250 data range:-125-125 offset:-125 lsb:1ppm
	/**
	* @brief operation hours counter
	*/
  union
  {
    uint16_t value;
    struct
    {
      uint16_t l      : 8; /* [0-7] */ // 低8位
      uint16_t h      : 8; /* [7-15] */ // 高8位
    } bit;
  } operation_hours_counter_byte_56; // range:0-36000 data range:0-36000 offset:0 lsb:1hour
	uint8_t not_used_byte_7;
} data2_heater_ratio_deviation_type;

typedef struct
{
	uint8_t correction_pressure_lambda_byte_0; // O2压力敏感性 range:0-200 data range:0-1 offset:0 lsb:0.005
	uint8_t correction_pressure_nox_byte_1; // nox压力敏感性 range:0-200 data range:0-1 offset:0 lsb:0.005
	uint8_t correction_no2_byte_2; // no2补偿 range:0-250 data range:0-1.25 offset:0 lsb:0.005
	uint8_t correction_nh3_byte_3; // nh3补偿 range:0-250 data range:0-1.25 offset:0 lsb:0.005
	uint8_t self_diagnosis_result_value_byte_4; // 自诊断偏差 range:0-250 data range:0-250 offset:0 lsb:1%
} data3_correction_factors_type;
typedef struct
{
	uint8_t o2_status_count;
	uint8_t nox_status_count;
	uint8_t vs_in_range_flag;
	data1_signal_values_type data1_signal_struct;
	
	uint8_t ip1_status_count;
	uint8_t ip2_status_count;
	uint8_t vs_status_count;
	uint8_t heater_status_count;
	system_state_enum ip2_diagnosis_flag;
	system_state_enum ip1_diagnosis_flag;
	system_state_enum vs_diagnosis_flag;
	system_state_enum status_diagnosis_flag;
	sensor_diagnosis_status_type sensor_diagnosis;
} sensor_obd_type;

void sensor_obd_task(sensor_handle_type *sensor_handle, sensor_obd_type *sensor_obd, system_type * system, uint8_t mode);
void sensor_obd_status_diagnosis(sensor_obd_type *sensor_obd, system_state_enum enable);
void sensor_obd_vs_diagnosis(sensor_obd_type *sensor_obd, system_state_enum enable);
void sensor_obd_ip1_diagnosis(sensor_obd_type *sensor_obd, system_state_enum enable);
void sensor_obd_ip2_diagnosis(sensor_obd_type *sensor_obd, system_state_enum enable);
extern sensor_obd_type sensor_obd_struct;
#endif
