#include "run_control.h"
#include "adc_app.h"
#include "dac_driver.h"
#include "state_machine_timer.h"
#include "pump_control.h"
#include "heater_control.h"
#include "soft_timer.h"
#include "sensor_obd.h"

rpvs_function_type rpvs_function_struct = {0};
void rpvs_function_measure(rpvs_function_type *rpvs_function, rpvs_enum state, vs_irpvs_enum vs_irpvs)
{
	rpvs_function->measure_flag = state;
	rpvs_function->wait_hpwm_flag = RPVS_DISABLE;
	rpvs_function->vs_irpvs_flag = vs_irpvs;
}
void rpvs_function_set_hpwm_flag(rpvs_function_type *rpvs_function, rpvs_enum state)
{
	rpvs_function->wait_hpwm_flag = state;
}
void rpvs_function_set_delay_time(rpvs_function_type *rpvs_function, rpvs_delay_time_enum delay_time)
{
	rpvs_function->delay_time = delay_time;
}
void rpvs_handle_function(adc_handle_type *adc_handle, rpvs_function_type *rpvs_function)
{
	rpvs_function->time = state_machine_tick_counter_get();
	if (rpvs_function->measure_flag == RPVS_ENABLE && rpvs_function->wait_hpwm_flag == RPVS_ENABLE)
	{
		if (rpvs_function->delay_time_flag == RPVS_DISABLE)
		{
			if ((rpvs_function->time - rpvs_function->wait_delay_time) >= rpvs_function->delay_time)
			{
				
				if (rpvs_function->vs_irpvs_flag == VS_TEST)
				{
					system_state_set_vs_i(SW_VS_I_NEG);
					system_state_set_vs_i_output(SW_VS_F_I_RPVS);
					system_state_set_vs_f_irpvs(SW_VS_F);
				}
				else
				{
					system_state_set_vs_i(SW_VS_I_POS);
					system_state_set_vs_i_output(SW_VS_F_I_RPVS);
					system_state_set_vs_f_irpvs(SW_IRPVS);
				}
				rpvs_function->delay_time_flag = RPVS_ENABLE;
				rpvs_function->vs_circle_time = state_machine_tick_counter_get();
			}
		}
		else
		{
			if (rpvs_function->time - rpvs_function->vs_circle_time >= VS_CIRCLE_TIME_10US)
			{
				rpvs_function->vs_circle_time = state_machine_tick_counter_get();
				rpvs_function->count++;
				if (rpvs_function->count == RPVS_SAMPING_TIME_50US)
				{
					if (rpvs_function->vs_irpvs_flag == VS_TEST)
					{
						adc_handle->vs_s_f_inject_flag = 1;
						adc_handle->vs_inject_flag = 1;
					}
					else 
					{
						adc_handle->i_rpvs_inject_flag = 1;
						adc_handle->i_rpvs_neg_pos_flag = 1;
					}
				}
				else if (rpvs_function->count == RPVS_SAMPING_TIME_100US)
				{
					if (rpvs_function->vs_irpvs_flag == VS_TEST)
					{
						system_state_set_vs_i(SW_VS_I_POS);
					}
					else
					{
						system_state_set_vs_i(SW_VS_I_NEG);
					}
				}
				else if (rpvs_function->count == RPVS_SAMPING_TIME_150US)
				{
//					if (rpvs_function->vs_irpvs_flag == IRPVS_TEST)
//					{
//						adc_handle->i_rpvs_inject_flag = 1;
//						adc_handle->i_rpvs_neg_pos_flag = 0;
//					}
				}
				else if (rpvs_function->count >= RPVS_SAMPING_TIME_200US)
				{
					system_state_set_vs_i_output(SW_NC);
					system_state_set_vs_f_irpvs(SW_IRPVS);
//					system_state_set_vs_i(SW_VS_I_POS);
					rpvs_function->delay_time_flag = RPVS_DISABLE;
					rpvs_function->measure_flag = RPVS_DISABLE;
					rpvs_function->count = 0;
				}
			}
		}
	}
	else
	{
		rpvs_function->wait_delay_time = state_machine_tick_counter_get();
	}
}

void run_control1_handle(sensor_handle_type *sensor_handle, adc_handle_type *adc_handle, rpvs_function_type *rpvs, 
												 heater_control_type *heater_control)
{
	static uint8_t count = 0;
	static uint32_t heater_count = 0;
	static float pwm = 0;
	static uint8_t heater_flag = 0;
	static uint8_t calibration_current_flag = 0;
	static rpvs_enum rpvs_state;
	static uint8_t com_temp = 0;
	sensor_data_type *sensor_data = &sensor_handle->sensor_data_struct;
	if (heater_control->to_test_rpvs_flag == 1)
	{
		rpvs_state = RPVS_ENABLE;
		sensor_obd_vs_diagnosis(&sensor_obd_struct, SYSTEM_STATE_ENABLE);
	}
	else
	{
		rpvs_state = RPVS_DISABLE;
		sensor_obd_vs_diagnosis(&sensor_obd_struct, SYSTEM_STATE_DISABLE);
		sensor_obd_status_diagnosis(&sensor_obd_struct, SYSTEM_STATE_DISABLE);
	}
	if (heater_control->com_voltage_set == 1)
	{
		if (com_temp == 0)
		{
			com_temp = 1;
			system_state_set_com_power(SW_COM_2_25V);
			system_state_set_ip2p_boost_normal(SW_IP2P_NORMAL);
			system_state_set_ip2p_boost_output(SYSTEM_STATE_DISABLE);
			sensor_obd_ip2_diagnosis(&sensor_obd_struct, SYSTEM_STATE_DISABLE);
			sensor_obd_status_diagnosis(&sensor_obd_struct, SYSTEM_STATE_ENABLE);
		}
	}
	else
	{
		if (com_temp == 1)
		{
			com_temp = 0;
			system_state_set_com_power(SW_COM_1V);
			system_state_set_ip2p_boost_normal(SW_IP2P_BOOST);
			system_state_set_ip2p_boost_output(SYSTEM_STATE_ENABLE);
			sensor_obd_ip2_diagnosis(&sensor_obd_struct, SYSTEM_STATE_ENABLE);
		}
	}
	if ((heater_count % 500) == 0) // i_rpvs
	{
		heater_count = 0;
		rpvs_function_measure(rpvs, rpvs_state, IRPVS_TEST);
	}
	else if ((heater_count % 50) == 10) // 50ms
	{
		heater_flag = 1;
		rpvs_function_measure(rpvs, rpvs_state, VS_TEST);
	}
	rpvs_function_set_delay_time(rpvs, RPVS_DELAY_TIME_250US);
	switch ((heater_count % 10))
	{
		case 0:
			adc_handle->adc0_dma_flag = 1;
			adc_handle->adc1_dma_flag = 1;
			break;
		case 1:
			get_sensor_data(sensor_handle, adc_handle);
			if (heater_control->begin_pump_flag == 1)
			{
				pump_control(sensor_handle);
				sensor_obd_ip1_diagnosis(&sensor_obd_struct, SYSTEM_STATE_ENABLE);
			}
			else
			{
				sensor_obd_ip1_diagnosis(&sensor_obd_struct, SYSTEM_STATE_DISABLE);
			}
			break;
		case 2:
//				
			break;
		case 3:
			bosch_sensor_heater_set_duty(pwm);
			break;
		case 4:
			if (heater_flag)
			{
				heater_control_task(heater_control, sensor_data);
				pwm = heater_control->heater_duty;
				heater_flag = 0;
			}
			break;
		case 9:
			break;
		default:
			break;
	}
	heater_count++;
}

void sensor_run_control_task(void)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
	static uint32_t time = 0;
	static uint32_t activetime = 0;
	time = soft_timer_tick_counter_get();
	if (time - activetime >= 1)
	{
//		run_control_handle(&sensor_handle_struct, &adc_handle_struct, &rpvs_function_struct);
		sensor_obd_task(&sensor_handle_struct, &sensor_obd_struct, &system_struct, heater_control_struct.mode);
		run_control1_handle(&sensor_handle_struct, &adc_handle_struct, &rpvs_function_struct, &heater_control_struct);
		activetime = soft_timer_tick_counter_get();
	}
  /* USER CODE END StartDefaultTask */
}
void sensor_data_collect_task(void)
{
  /* USER CODE BEGIN StartDefaultTask */
	adc_task_handle(&adc_handle_struct);
	rpvs_handle_function(&adc_handle_struct, &rpvs_function_struct);
  /* USER CODE END StartDefaultTask */
}
