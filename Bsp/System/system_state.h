/* USER CODE END Header */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SYSTEM_STATE_H__
#define __SYSTEM_STATE_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#define VS_CIRCLE_TIME_10US (1)
#define RPVS_SAMPING_TIME_50US (5)
#define RPVS_SAMPING_TIME_100US (10)
#define RPVS_SAMPING_TIME_150US (15)
#define RPVS_SAMPING_TIME_200US (20)

typedef enum
{
	SW_VS_F = 0,
	SW_IRPVS
} switch_vs_f_irpvs_enum;

typedef enum
{
	SW_COM_2_25V = 0,
	SW_COM_1V
} switch_com_voltage_enum;

typedef enum
{
	SW_VS_F_I_RPVS = 0,
	SW_NC
} switch_vs_i_output_enum;

typedef enum
{
	SW_VS_I_NEG = 0,
	SW_VS_I_POS
} switch_vs_i_enum;

typedef enum
{
	SW_IP2P_VS = 0,
	SW_IP2P_COM
} switch_ip2p_power_enum;

typedef enum
{
	SW_IP2P_NORMAL = 0,
	SW_IP2P_BOOST
} switch_ip2p_boost_normal_enum;

typedef enum
{
	SYSTEM_STATE_DISABLE = 0,
	SYSTEM_STATE_ENABLE
} system_state_enum;

typedef enum
{
	NOHEATER,
	PREHEATER,
	HEATER
} bosch_sensor_heater_mode_type;

typedef enum
{
	RPVS_DISABLE,
	RPVS_ENABLE
} rpvs_enum;
typedef enum
{
	RPVS_DELAY_TIME_250US = 25,
	RPVS_DELAY_TIME_400US = 40
} rpvs_delay_time_enum;

typedef enum
{
	VS_TEST,
	IRPVS_TEST
} vs_irpvs_enum;

// 系统滴答定时器为1ms
#define TIM_5_MS	5
#define TIM_10_MS	10
#define TIM_20_MS	20
#define TIM_60_MS	60
#define TIM_100_MS	100
#define TIM_1000_MS	1000
#define TIM_60_S_BASE_1MS	60000

//--10ms时基
#define POWER_ON_PREHEATER_DELAY_TIME_BASE_10MS 1000 // 10s
#define DL35TIMEOUT_BASE_1MS    1250 // 1.25s
#define CALIBRATION_TIME_BASE_10MS	200
#define TIM_300_S_BASE_1MS	300000
#define TIM_1_6_S_BASE_10MS	160
#define TIM_60_S_BASE_1MS	60000

enum SYSTEM_MODE
{
	SYSTEM_MODE_NORMAL,		// 默认工作模式 --原始名SYSTEM_MODE_DEFAULT
	SYSTEM_MODE_DEBUG,		// 调试模式
	SYSTEM_MODE_CALIBRATION,// 标定模式
	SYSTEM_MODE_UNIQUE_ID, // ID模式
};
enum SYSTEM_RUN_STATE
{
	SYSTEM_RUN_STATE_IDLE,
	SYSTEM_RUN_STATE_LOAD_PAREMETER,  //提前到初始化里
	SYSTEM_RUN_STATE_FILL_DATA, //提前到初始化里
	SYSTEM_RUN_STATE_DELAY,       //提前到初始化里
	SYSTEM_RUN_STATE_WAIT_JUDGESENSOR, //提前到初始化里

	SYSTEM_RUN_STATE_WAIT_DEW_POINT,
	//SYSTEM_RUN_STATE_HEATER,//在中断里同步进行，所以无状态
	SYSTEM_RUN_STATE_ADJUST_IP_REFENCE,
	SYSTEM_RUN_STATE_NOX_MEASURE,//大概持续300S左右
	//SYSTEM_RUN_STATE_NOX_MEASURE_TEST,//大概持续300S左右
};
typedef enum
{
	POSITION_UNKNOW,
	POSITION_INTAKE,//01 =0x51 进气传感器
	POSITION_2,
	POSITION_3,
	POSITION_EXHAUST,//04 =0x52 出气传感器
}SENSOR_POSITION;

typedef union
{
	uint32_t data;
	struct
	{
		uint32_t original : 16;
		uint32_t invert : 16;
	} bit;
} eeprom_data_union;
typedef struct
{
	enum SYSTEM_MODE mode;           //运行模式
	enum SYSTEM_RUN_STATE run_state;  //运行状态
	SENSOR_POSITION position;   //安装位置，决定是进气传感器还是出气传感器

	bool dewpoint_command;                  //传感器芯片工作控制状态，TRUE:工作，false:关闭
	bool preheater_command;                   //传感器芯片工作控制状态，TRUE:工作，false:关闭
	uint16_t  eeprom_buffer[2];              //8Byte，保存EEPROM校验
	eeprom_data_union eeprom_union;
} system_type;
typedef struct
{
	float p;
	float i;
	float d;
} pid_type;

typedef  struct
{
	int16_t calibration_no100_o2low_ip1;
	int16_t calibration_no100_o2low_ip2;
	int16_t calibration_no100_o2low_vp1;
	int16_t calibration_no100_o2low_vbin;
	
	int16_t calibration_no100_o2high_ip1;
	int16_t calibration_no100_o2high_ip2;
	int16_t calibration_no100_o2high_vp1;
	int16_t calibration_no100_o2high_vbin;
	
	int16_t calibration_no500_o2low_ip1;
	int16_t calibration_no500_o2low_ip2;
	int16_t calibration_no500_o2low_vp1;
	int16_t calibration_no500_o2low_vbin;

	int16_t calibration_no500_o2high_ip1;
	int16_t calibration_no500_o2high_ip2;
	int16_t calibration_no500_o2high_vp1;
	int16_t calibration_no500_o2high_vbin;
	
	int16_t calibration_no1500_o2low_ip1;
	int16_t calibration_no1500_o2low_ip2;
	int16_t calibration_no1500_o2low_vp1;
	int16_t calibration_no1500_o2low_vbin;

	int16_t calibration_no1500_o2high_ip1;
	int16_t calibration_no1500_o2high_ip2;
	int16_t calibration_no1500_o2high_vp1;
	int16_t calibration_no1500_o2high_vbin;
	
	int16_t calibration_no0_o2high_ip1;
	int16_t calibration_no0_o2high_ip2;
	int16_t calibration_no0_o2high_vp1;
	int16_t calibration_no0_o2high_vbin;
	
	uint16_t calibration_vs;
	uint16_t calibration_rpvs;
	
	//--补偿系数 （针对不同点位）
	int8_t no100_add;               //
	int8_t no500_add;               //
	uint8_t no500_factor;            //
	int8_t no1500_add;              //
	uint8_t no1500_factor;           //
	int8_t no3000_add;              //
	uint8_t no3000_factor;           //
	
	pid_type vs_pid;
	pid_type rpvs_pid;
	
//--标定参数
	uint8_t calibration_number;     //标定传感器编号    ————原变量名 CalibrationNo
	uint16_t calibration_count;        // 标定定时计数器
} sensor_calibration_type;

extern system_type system_struct;
void system_state_set_vs_f_irpvs(switch_vs_f_irpvs_enum enum_state);
void system_state_set_com_power(switch_com_voltage_enum enum_state);
void system_state_set_vs_i_output(switch_vs_i_output_enum enum_state);
void system_state_set_vs_i(switch_vs_i_enum enum_state);
void system_state_set_ip2p_power(switch_ip2p_power_enum enum_state);
void system_state_set_ip2p_boost_normal(switch_ip2p_boost_normal_enum enum_state);
void system_state_set_ip2p_boost_output(system_state_enum enum_state);
system_state_enum system_state_get_sensor_position(void);
void pump_com_pin_connect(system_state_enum enum_state);
void pump_ip1_pin_connect(system_state_enum enum_state);
void pump_ip2_pin_connect(system_state_enum enum_state);
void pump_vs_pin_connect(system_state_enum enum_state);
void system_state_init(void);
#endif
