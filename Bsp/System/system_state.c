#include "system_state.h"
system_type system_struct;
void system_state_init(void)
{
	system_state_set_vs_f_irpvs(SW_VS_F);
	system_state_set_com_power(SW_COM_2_25V);
	system_state_set_vs_i_output(SW_NC);
	system_state_set_vs_i(SW_VS_I_NEG);
	system_state_set_ip2p_power(SW_IP2P_COM);
	system_state_set_ip2p_boost_normal(SW_IP2P_NORMAL);
	system_state_set_ip2p_boost_output(SYSTEM_STATE_DISABLE);
}

void system_state_set_vs_f_irpvs(switch_vs_f_irpvs_enum enum_state)
{
	if (enum_state == SW_VS_F)
	{
		DL_GPIO_setPins(GPIO_GRP_0_PORT, GPIO_GRP_0_RPVS_SELECT_PIN);
	}
	else
	{
		DL_GPIO_clearPins(GPIO_GRP_0_PORT, GPIO_GRP_0_RPVS_SELECT_PIN);
	}
}

void system_state_set_com_power(switch_com_voltage_enum enum_state)
{
	if (enum_state == SW_COM_2_25V)
	{
		DL_GPIO_setPins(GPIO_GRP_0_PORT, GPIO_GRP_0_COM_SELECT_PIN);
	}
	else
	{
		DL_GPIO_clearPins(GPIO_GRP_0_PORT, GPIO_GRP_0_COM_SELECT_PIN);
	}
}

void system_state_set_vs_i_output(switch_vs_i_output_enum enum_state)
{
	if (enum_state == SW_VS_F_I_RPVS)
	{
		DL_GPIO_setPins(GPIO_GRP_0_PORT, GPIO_GRP_0_VS_I_OUT_SELECT_PIN);
	}
	else
	{
		DL_GPIO_clearPins(GPIO_GRP_0_PORT, GPIO_GRP_0_VS_I_OUT_SELECT_PIN);
		DL_GPIO_setPins(GPIO_GRP_0_PORT, GPIO_GRP_0_VS_I_SELECT_PIN);
	}
}

void system_state_set_vs_i(switch_vs_i_enum enum_state)
{
	if (enum_state == SW_VS_I_NEG)
	{
		DL_GPIO_setPins(GPIO_GRP_0_PORT, GPIO_GRP_0_VS_I_SELECT_PIN);
	}
	else
	{
		DL_GPIO_clearPins(GPIO_GRP_0_PORT, GPIO_GRP_0_VS_I_SELECT_PIN);
	}
}

void system_state_set_ip2p_power(switch_ip2p_power_enum enum_state)
{
	if (enum_state == SW_IP2P_VS)
	{
	}
	else
	{
	}
}

void system_state_set_ip2p_boost_output(system_state_enum enum_state)
{
	if (enum_state == SYSTEM_STATE_ENABLE)
	{
		DL_GPIO_initDigitalOutput(GPIO_GRP_0_IP2_BOOST_SELECT_IOMUX);
		DL_GPIO_clearPins(GPIO_GRP_0_PORT, GPIO_GRP_0_IP2_BOOST_SELECT_PIN);
		DL_GPIO_enableOutput(GPIO_GRP_0_PORT, GPIO_GRP_0_IP2_BOOST_SELECT_PIN);
	}
	else
	{
		DL_GPIO_disableOutput(GPIO_GRP_0_PORT, GPIO_GRP_0_IP2_BOOST_SELECT_PIN);
		DL_GPIO_initDigitalInput(GPIO_GRP_0_IP2_BOOST_SELECT_IOMUX);
	}
}

void system_state_set_ip2p_boost_normal(switch_ip2p_boost_normal_enum enum_state)
{
	if (enum_state == SW_IP2P_NORMAL)
	{
		DL_GPIO_setPins(GPIO_GRP_0_PORT, GPIO_GRP_0_IP2P_SELECT_PIN);
	}
	else
	{
		DL_GPIO_clearPins(GPIO_GRP_0_PORT, GPIO_GRP_0_IP2P_SELECT_PIN);
	}
}

system_state_enum system_state_get_sensor_position(void)
{
	if (DL_GPIO_readPins(GPIO_GRP_0_PORT, GPIO_GRP_0_CAN_SET_PIN))
	{
		return SYSTEM_STATE_ENABLE;
	}
	else
	{
		return SYSTEM_STATE_DISABLE;
	}
}
