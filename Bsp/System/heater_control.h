/* USER CODE END Header */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HEATER_CONTROL_H__
#define __HEATER_CONTROL_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc_app.h"
#include "system_state.h"
#include "position_pid.h"
#include "can_driver.h"
typedef enum
{
	HEATER_MODE_IDLE = 0,
	HEATER_MODE_1,
	HEATER_MODE_2,
	HEATER_MODE_3,
	HEATER_MODE_4,
	HEATER_MODE_5,
	HEATER_MODE_6,
	HEATER_MODE_7,
} heater_control_mode_type;

typedef struct 
{
	uint32_t max_duty;
	uint32_t min_duty;
	
	float heater_duty; // 加热pwm
	float init_duty;
	float rise_duty;
	float init_duty_constant; // 幂函数的常数
	uint8_t rise_duty_coefficient; // 升温系数
	float rise_step_duty; // 升温过程中步进值
	uint16_t rise_step_amount; // 升温过程数量
	uint16_t to_test_rpvs_amount; // 测内阻时刻
	uint8_t to_test_rpvs_flag; // 测试内阻标志位
	uint16_t rise_step_count;
	uint8_t begin_pump_flag; // 开始联泵标志位
	uint16_t rpvs_first_aim; // rpvs第一个目标值
	uint16_t rpvs_second_aim; // rpvs第二个目标值
	uint16_t wait_for_pump_time; // 等待联泵的最大时间
	uint16_t wait_for_ip2_boost_time; // 等待P2泵升压时间
	uint8_t com_voltage_set;
	heater_control_mode_type mode;
} heater_control_type;
extern heater_control_type heater_control_struct;
void heater_control_init(void);
void bosch_sensor_heater_set_duty(float duty);
float bosch_sensor_heater_step_handle(sensor_data_type *sensor_data);
void bosch_sensor_heater_mode(bosch_sensor_heater_mode_type mode);
void heater_control_task(heater_control_type *heater_control, sensor_data_type *sensor_data);
void heater_command_control(system_type *system, sensor_hardware_type *sensor_hardware, can_handle_type *can_handle);
#endif
