#ifndef __POSITION_H__
#define __POSITION_H__
typedef struct
{
	float kp;
	float ki;
	float kd;
	
	float error;
	float last_error;
	float last_last_error;
	float dcalc;
	float result;
	float set_point; // 目标值
} position_pid_type;
float position_pid(position_pid_type *pid, float error);
typedef struct 
{
	float Kp;
	float Kts;
	float Kti;
	float error;
	float proportionnal;
	float integral;
	float computation;
	float result;
	float set_point; // 目标值
	float result_max;
	float result_min;
} position_pid_extend_type;
float position_pid_extend(position_pid_extend_type *pid, float error);
#endif
