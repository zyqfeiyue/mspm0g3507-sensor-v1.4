#include "heater_control.h"

position_pid_extend_type heater_pid_extend_struct = {0};
heater_control_type heater_control_struct = { 0 };
void heater_control_init(void)
{
	heater_pid_extend_struct.Kp = 0.001f; // 0.009
	heater_pid_extend_struct.Kti = 3.f; // 9
	heater_pid_extend_struct.Kts = 0.05f;
	heater_pid_extend_struct.set_point = 250.f;
	heater_pid_extend_struct.result_min = 0;
	heater_pid_extend_struct.result_max = 1;
	bosch_sensor_heater_mode(HEATER);
	
	heater_control_struct.init_duty_constant = 182000;
	heater_control_struct.rise_duty_coefficient = 11;
	heater_control_struct.rise_step_amount = 28;
	heater_control_struct.to_test_rpvs_amount = 44;
	heater_control_struct.rpvs_first_aim = 250;
	heater_control_struct.rpvs_second_aim = 150;
	heater_control_struct.wait_for_pump_time = 300;
	heater_control_struct.wait_for_ip2_boost_time = 200;
	heater_control_struct.min_duty = 1;
	heater_control_struct.max_duty = 80;
	heater_control_struct.mode = HEATER_MODE_IDLE;
}
void heater_pwm_pulse_set(uint16_t pwm)
{
	DL_TimerA_setTimerCount(PWM_0_INST, 0);
	DL_TimerA_setCaptureCompareValue(PWM_0_INST, pwm, DL_TIMER_CC_0_INDEX);
}
#define PWM_MAX 10000.f
float pwm_value;
void bosch_sensor_heater_set_duty(float duty)
{
	pwm_value = (duty / 100.f) * PWM_MAX;
	if (pwm_value >= PWM_MAX)
	{
		pwm_value = 5000;
	}
	else if (pwm_value <= 0)
	{
		pwm_value = 319;
	}
	heater_pwm_pulse_set((uint16_t)pwm_value);
}
float heater_duty_pid;

void calculate_heater_first_step_pwm(heater_control_type *heater_control, sensor_data_type *sensor_data)
{
	uint8_t input_power_v = (uint8_t)((float)sensor_data->power_input_data / 1000.f + 0.5f);
	if (input_power_v >= 14 && input_power_v <= 36) // 电压在合适范围内
	{
		heater_control->init_duty = heater_control->init_duty_constant / (input_power_v * input_power_v) * 0.01f; // duty %
		heater_control->rise_duty = heater_control->init_duty * heater_control->rise_duty_coefficient;
		if (heater_control->rise_duty > heater_control->max_duty)
		{
			heater_control->rise_duty = heater_control->max_duty;
		}
		if (heater_control->init_duty < heater_control->min_duty)
		{
			heater_control->init_duty = heater_control->min_duty;
		}
		heater_control->rise_step_duty = (heater_control->rise_duty - heater_control->init_duty) / heater_control->rise_step_amount;
	}
	else
	{
		
	}
}

void heater_control_task(heater_control_type *heater_control, sensor_data_type *sensor_data)
{
	static float res_error;
	switch (heater_control->mode)
	{
		case HEATER_MODE_IDLE:
			calculate_heater_first_step_pwm(heater_control, sensor_data);
			heater_control->heater_duty = heater_control->init_duty;
			heater_control->com_voltage_set = 1;
			heater_control->rise_step_count = 0;
			heater_control->to_test_rpvs_flag = 0;
			heater_control->begin_pump_flag = 0;
			break;
		case HEATER_MODE_1: // 升温阶段
			heater_control->heater_duty += heater_control->rise_step_duty;
			heater_control->rise_step_count++;
			if (heater_control->rise_step_count > heater_control->rise_step_amount)
				heater_control->mode = HEATER_MODE_2;
			break;
		case HEATER_MODE_2:
			heater_control->rise_step_count++;
			if (heater_control->to_test_rpvs_flag == 0)
			{
				if (heater_control->rise_step_count > heater_control->to_test_rpvs_amount)
				{
					heater_control->rise_step_count = 0;
					heater_control->to_test_rpvs_flag = 1; 
				}
			}
			else
			{
				if (heater_control->rise_step_count >= heater_control->wait_for_pump_time) // 超时
				{
					heater_control->mode = HEATER_MODE_IDLE;
				}
				if (sensor_data->rpvs_origin_data >= 100 && sensor_data->rpvs_origin_data <= heater_control->rpvs_second_aim)
				{
//					heater_control->rise_step_count = 0;
					heater_control->mode = HEATER_MODE_3;
				}
				else if (sensor_data->rpvs_origin_data >= 100 && sensor_data->rpvs_origin_data <= heater_control->rpvs_first_aim)
				{
					if (heater_control->begin_pump_flag == 0)
					{
						heater_control->rise_step_count = 0;
						heater_control->begin_pump_flag = 1;
						heater_control->com_voltage_set = 0;
					}
				}
			}
			break;
		case HEATER_MODE_3:
			heater_control->rise_step_count++;
			if (heater_control->rise_step_count > heater_control->wait_for_ip2_boost_time)
			{
				heater_control->rise_step_count = heater_control->wait_for_ip2_boost_time;
				heater_pid_extend_struct.set_point = 250;
				heater_control->com_voltage_set = 1;
			}
			else
			{
				heater_pid_extend_struct.set_point = 150;
			}
			if (sensor_data->vs_s_f_data >= 120 && sensor_data->vs_s_f_data <= 550)
			{
				res_error = (sensor_data->rpvs_amp_data - heater_pid_extend_struct.set_point);
			}
			else
			{
				res_error = (sensor_data->rpvs_origin_data - heater_pid_extend_struct.set_point);
			}
			heater_duty_pid = position_pid_extend(&heater_pid_extend_struct, res_error);
			heater_control->heater_duty = heater_duty_pid * 100.f;
			break;
		default:
			break;
	}
}

void heater_command_control(system_type *system, sensor_hardware_type *sensor_hardware, can_handle_type *can_handle)
{
	if (((sensor_hardware->hardware_error) & 0xF0) == 0x00)
	{
		if (system_struct.dewpoint_command == true && heater_control_struct.mode == HEATER_MODE_IDLE)
		{
			heater_control_struct.mode = HEATER_MODE_1;
		}
		else if (system_struct.dewpoint_command == false && heater_control_struct.mode != HEATER_MODE_IDLE)
		{
			heater_control_struct.mode = HEATER_MODE_IDLE;
			system->run_state = SYSTEM_RUN_STATE_WAIT_DEW_POINT;          // 跳到等待露点处
		}
	}
	else // 停止加热
	{
		system->preheater_command = false;
		system->dewpoint_command = false;
		system->run_state = SYSTEM_RUN_STATE_WAIT_DEW_POINT;          // 跳到等待露点处
	}
}
void bosch_sensor_heater_mode(bosch_sensor_heater_mode_type mode)
{
	
}


