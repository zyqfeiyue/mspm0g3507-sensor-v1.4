/* USER CODE END Header */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __RUN_CONTROL_H__
#define __RUN_CONTROL_H__
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "system_state.h"
#include "adc_driver.h"
typedef struct
{
	volatile uint32_t time; // 计数时间
	volatile uint32_t wait_delay_time; // 等待时间
	volatile uint32_t vs_circle_time; // vs计数时间
	rpvs_enum delay_time_flag;
	rpvs_enum measure_flag;
	vs_irpvs_enum vs_irpvs_flag;
	rpvs_enum wait_hpwm_flag;
	uint16_t delay_time;
	uint8_t count;
} rpvs_function_type;
extern rpvs_function_type rpvs_function_struct;
void rpvs_function_set_hpwm_flag(rpvs_function_type *rpvs_function, rpvs_enum state);
void sensor_run_control_task(void);
void sensor_data_collect_task(void);
#endif
