#include "position_pid.h"
#include "math.h"

float position_pid(position_pid_type *pid, float error)
{
	pid->error = error;
	pid->last_error += pid->error;
	pid->result = pid->error * pid->kp + pid->last_error * pid->ki + pid->kd * (pid->error - pid->last_last_error);
	if (pid->result >= 100)
	{
		pid->result = 100;
	}
	else if (pid->result <= 0)
	{
		pid->result = 0;
	}
	return pid->result;
}

float position_pid_extend(position_pid_extend_type *pid, float error)
{
	pid->error = error;
	pid->proportionnal = pid->Kp * pid->error;
	pid->computation = pid->proportionnal + pid->integral;
	if ( pid->computation < pid->result_min || pid->computation > pid->result_max)
	{
		if (pid->computation < pid->result_min)
		{
			pid->result = pid->result_min;
		}
		else if (pid->computation > pid->result_max)
		{
			pid->result = pid->result_max;
		}
	}
	else
	{
		pid->result = pid->computation;
		pid->integral = pid->integral + pid->Kp * (pid->Kts / pid->Kti) * pid->error;
	}
	return pid->result;
}
