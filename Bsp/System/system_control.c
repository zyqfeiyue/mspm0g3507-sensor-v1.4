#include "system_control.h"
#include "sae_j1939.h"

/*-----------------------------------------------------------------------
函数: void PumpControl_Timer_Task(time_type *time,CanBusType *can_handle,system_type * system,sensor_type *sensor,HeaterType *pHEA)
描述: T4的625us中断 ISR_TimerProcess 函数产生标志
参数:
返回: none.
备注:          （主程序循环的定时任务  ）主程序内执行625us
//-------------------------------------------------------------------------*/
void general_control_process(can_handle_type *can_handle, system_type *system, sensor_handle_type *sensor_handle, sensor_obd_type *sensor_obd,
	 sensor_calibration_type *sensor)
{
	static uint32_t nowtime = 0;
	static uint32_t activetime = 0;
	static uint32_t active_time_10ms = 0;
	nowtime = soft_timer_tick_counter_get();
//CAN信息输出：>>>>>>
	if(system->mode == SYSTEM_MODE_NORMAL)//正常模式，输出调试信息
	{
		j1939_normal_transmit_data(&J1939_struct);
//		normal_can_transmit_message(sensor_handle, can_handle, system, sensor_obd);
	}
	else if (system->mode == SYSTEM_MODE_UNIQUE_ID)
	{
//		uniqueid_can_transmit_message(can_handle, sensor);
	}
	else  //debug模式，标定模式，输出调试信息
	{
		debug_can_transmit_message(can_handle, system, sensor_handle, sensor);
		if( system->mode == SYSTEM_MODE_CALIBRATION )
		{
			write_calibration_parameter(sensor, sensor_handle, system, can_handle);
		}
	}
	sae_j1939_listen_for_message(can_handle, &J1939_struct);
//	can_receive_process(can_handle, system);
//	can_receive_process1(can_handle, system, &sensor_handle->sensor_hardware_struct, sensor);
	//--10ms 周期事件处理--在中断里产生10ms标志位
	if (nowtime - active_time_10ms >= 10 )
	{
		active_time_10ms = soft_timer_tick_counter_get();  //clr

		

	}
}
/*-----------------------------------------------------------------------
 函数: void  NOx_Calc_FSM(time_type *time,CanBusType *can_handle,system_type * system,sensor_type *sensor,HeaterType *pHEA)
 描述:
 参数:
 返回: none.
 备注:
------------------------------------------------------------------------*/
void system_run_state_process(can_handle_type *can_handle, system_type * system, sensor_handle_type *sensor_handle, heater_control_type *heater_control,
	sensor_calibration_type *sensor)
{
	static uint32_t now_time = 0;
	static uint32_t active_time = 0;
	sensor_data_type *sensor_data = &sensor_handle->sensor_data_struct;
	sensor_hardware_type *sensor_hardware = &sensor_handle->sensor_hardware_struct;
	now_time = soft_timer_tick_counter_get();
	if (now_time - active_time >= 1)
	{
		active_time = soft_timer_tick_counter_get();
		if(can_handle->recovery_time > 1) can_handle->recovery_time--;
		//--300秒超时计数
		if( sensor_hardware->heater_timeout_count > 0) 
			sensor_hardware->heater_timeout_count--;  //减计数，直到0即超时（针对正常加热有效）
	}
	else
		return;
	//--停止指令  直接由指令
	switch( system->run_state )
	{
    case SYSTEM_RUN_STATE_IDLE:break;

    case SYSTEM_RUN_STATE_LOAD_PAREMETER:
		if (SYSTEM_STATE_ENABLE == system_state_get_sensor_position())// --检测安装位置
		{
			system_struct.position = POSITION_EXHAUST;   // 高电平为进气  52 悬空
			j1939_struct_init(&J1939_struct, LOCATION_OUTLET);
		}
		else
		{
			system_struct.position = POSITION_INTAKE;   // 低电平为进气  51 接地
			j1939_struct_init(&J1939_struct, LOCATION_INTAKE);
		}
		read_calibration_parameter(sensor, system); // 读取标定信息
		system->run_state = SYSTEM_RUN_STATE_FILL_DATA;
		break;

    case SYSTEM_RUN_STATE_FILL_DATA:
		sensor_hardware->heater_timeout_count = TIM_300_S_BASE_1MS;
		sensor->calibration_count = 0;
		sensor_hardware->write_eeprom_flag = 0x23;
		system->run_state = SYSTEM_RUN_STATE_WAIT_JUDGESENSOR;
		break;

    case SYSTEM_RUN_STATE_WAIT_JUDGESENSOR:
  //--判断是否有两重复地址（判断干涉）
		sensor_obd_command_data6(can_handle, system->position, sensor_hardware->hardware_error);
		system->run_state = SYSTEM_RUN_STATE_WAIT_DEW_POINT;
		break;

		case SYSTEM_RUN_STATE_WAIT_DEW_POINT:  //预热前发送数据
			if( system->dewpoint_command == true )  // && ((sensor->hardware_error&0x0F) == 0x00)
			{
				system->run_state = SYSTEM_RUN_STATE_ADJUST_IP_REFENCE;  //切换成下一状态
			}
			else
			{
				if((sensor_hardware->hardware_error & 0x40) == 0x00) // 超过7v开始通讯
				{
					if (system->mode == SYSTEM_MODE_NORMAL)
					{
						can_handle->sensor_transmit_data_flag_struct.bit.data1_cyclic_flag = 1;
						can_handle->sensor_transmit_data_flag_struct.bit.data2_cyclic_flag = 1;
					}
				}
			}
			break;

		case SYSTEM_RUN_STATE_ADJUST_IP_REFENCE: // 初始化加热头变量
			sensor_hardware->heater_timeout_count = TIM_300_S_BASE_1MS;   // clr
			sensor_hardware->heater_enable_flag = 1; // 使能PID加热
			system->run_state = SYSTEM_RUN_STATE_NOX_MEASURE;
			break;

		case SYSTEM_RUN_STATE_NOX_MEASURE: // 大概持续300S左右
			// 整个加热过程持续300S>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			if(( 0 < sensor_hardware->heater_timeout_count) &&  ((sensor_hardware->hardware_error&0x0F) == 0x00))
			{
				if(system->mode == SYSTEM_MODE_NORMAL)
				{
					sensor_nox_o2_calculate_task(sensor, sensor_data, can_handle, system, heater_control); // 计算nox can发送
				}
			}
			else
			{
				system->dewpoint_command = false;
				system->run_state = SYSTEM_RUN_STATE_WAIT_DEW_POINT;
			}
			break;
			default:break;
		}
	if(system->dewpoint_command == false)
	{
		sensor_hardware->heater_enable_flag = 0;                     //使能PID加热
	}
}


