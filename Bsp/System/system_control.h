/* USER CODE END Header */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SYSTEM_CONTROL_H__
#define __SYSTEM_CONTROL_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "can_control.h"
#include "system_state.h"
#include "soft_timer.h"
#include "adc_app.h"
#include "heater_control.h"
#include "calibration.h"
#include "can_command.h"
#include "nox_calculate.h"
void general_control_process(can_handle_type *can_handle, system_type *system, sensor_handle_type *sensor_handle, sensor_obd_type *sensor_obd,
	 sensor_calibration_type *sensor);
void system_run_state_process(can_handle_type *can_handle, system_type * system, sensor_handle_type *sensor_handle, heater_control_type *heater_control,
	sensor_calibration_type *sensor);
#endif
