#include "pump_control.h"
#include "position_pid.h"
#include "dac_driver.h"
position_pid_extend_type position_ip1_pid_extend_struct = {0};
position_pid_extend_type position_ip1_vp1_pid_extend_struct = {0};

float out_pid_ip1_vs = 0;
//#define ip1_b (float)((5.9f + 0.1f) / 32.f / 100.f * 1000000.f)
#define ip1_b (float)((5.9f + 0.1f) / 32.4f / 100.f * 1000000.f)
	float final;
void set_ip1_ua(sensor_handle_type *sensor_handle, float ip1_ua)
{
	sensor_character_para_type *sensor_character_para = &sensor_handle->sensor_character_para_struct;
	if (ip1_ua >= 6000)
	{
		ip1_ua = 6000;
	}
	else if (ip1_ua <= -3000)
	{
		ip1_ua = -3000;
	}
	float a = (float)ip1_ua / ip1_b;
	float c = (3.3f - a) * 1000.f;
	if (c >= 5000)
	{
		c = 5000;
	}
	else if (c <= 0)
	{
		c = 0;
	}
	final = c * (sensor_character_para->ip1p_dac_div_res / sensor_character_para->ip1p_dac_smp_res);
	ip1p_dac_set_value(final);
}
// PID��ʼ��
void pump_pid_init(void)
{
	position_ip1_pid_extend_struct.Kp = 0.15f; // 0.02
	position_ip1_pid_extend_struct.Kti = 0.5f; // 0.9f;
	position_ip1_pid_extend_struct.Kts = 0.01f; // 0.01
	position_ip1_pid_extend_struct.set_point = 425.f;
	position_ip1_pid_extend_struct.result_min = -3;
	position_ip1_pid_extend_struct.result_max = 6;	
	position_ip1_pid_extend_struct.result = 3;

	
	// p1 vp1
	position_ip1_vp1_pid_extend_struct.Kp = 0.002f; // 0.25
	position_ip1_vp1_pid_extend_struct.Kti = 0.1f; // 0.1f;
	position_ip1_vp1_pid_extend_struct.Kts = 0.01f; // 0.01
	position_ip1_vp1_pid_extend_struct.set_point = 100.f;
	position_ip1_vp1_pid_extend_struct.result_min = -3;
	position_ip1_vp1_pid_extend_struct.result_max = 6;	
	position_ip1_vp1_pid_extend_struct.result = 3;
}

float ip1_ua;
float vp2 = 450;
uint8_t flag = 0;
void pump_control(sensor_handle_type *sensor_handle)
{
	sensor_data_type *sensor_data = &sensor_handle->sensor_data_struct;
	
	float error = (position_ip1_pid_extend_struct.set_point - sensor_data->vs_data);
	out_pid_ip1_vs = position_pid_extend(&position_ip1_pid_extend_struct, error);
	set_ip1_ua(sensor_handle, (out_pid_ip1_vs * 1000.f));
//	position_ip1_vp1_pid_extend_struct.set_point = sensor_debug_struct.pump_voltage;
//	float error = (position_ip1_vp1_pid_extend_struct.set_point - sensor_data->vp1_data);
//	out_pid_ip1_vs = position_pid_extend(&position_ip1_vp1_pid_extend_struct, error);
//	set_ip1_ua(sensor_handle, (out_pid_ip1_vs * 1000.f));
//	set_ip1_ua(sensor_handle, ip1_ua);
//	
//	sensor->ip0 = sensor_data->ip1_filt;
//	sensor->ip2 = sensor_data->ip2_filt;
}
