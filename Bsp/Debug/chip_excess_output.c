#include "chip_excess_output.h"

ExcessOutputStruct excessOutStruct = {0};

void ReceiveExcessOutputData(uint8_t *data)
{
	uint8_t number = data[0];
	if (data[7] != 0xFF)
		return;
	switch (number)
	{
		case 0x00:
			excessOutStruct.V0_Receive_PID.ReceiveFlag = 1;
			excessOutStruct.V0_Receive_PID.P = data[1] << 8 | data[2];
			excessOutStruct.V0_Receive_PID.I = data[3] << 8 | data[4];
			excessOutStruct.V0_Receive_PID.D = data[5] << 8 | data[6];
			break;
		case 0x01:
			excessOutStruct.P1_Receive_PID.ReceiveFlag = 1;
			excessOutStruct.P1_Receive_PID.P = data[1] << 8 | data[2];
			excessOutStruct.P1_Receive_PID.I = data[3] << 8 | data[4];
			excessOutStruct.P1_Receive_PID.D = data[5] << 8 | data[6];
			break;
		case 0x02:
			excessOutStruct.V1_Receive_PID.ReceiveFlag = 1;
			excessOutStruct.V1_Receive_PID.P = data[1] << 8 | data[2];
			excessOutStruct.V1_Receive_PID.I = data[3] << 8 | data[4];
			excessOutStruct.V1_Receive_PID.D = data[5] << 8 | data[6];
			break;
		case 0x03:
			excessOutStruct.V2_Receive_PID.ReceiveFlag = 1;
			excessOutStruct.V2_Receive_PID.P = data[1] << 8 | data[2];
			excessOutStruct.V2_Receive_PID.I = data[3] << 8 | data[4];
			excessOutStruct.V2_Receive_PID.D = data[5] << 8 | data[6];
			break;
		case 0x04:
			excessOutStruct.Sliding_ReceiveFlag = 1;
			excessOutStruct.P0_Sliding_Time = data[1] << 8 | data[2];
			excessOutStruct.P2_Sliding_Time = data[3] << 8 | data[4];
			break;
		default:
			break;
	}
}
