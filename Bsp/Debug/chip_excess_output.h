#ifndef CHIP_EXCESS_OUTPUT_H_
#define CHIP_EXCESS_OUTPUT_H_
#include "main.h"
typedef struct
{
	int16_t P;
	int16_t I;
	int16_t D;
	uint8_t ReceiveFlag;
} PumpPIDStruct;

typedef struct
{
	PumpPIDStruct V0_Receive_PID;
	PumpPIDStruct P1_Receive_PID;
	PumpPIDStruct V1_Receive_PID;
	PumpPIDStruct V2_Receive_PID;
	uint8_t Sliding_ReceiveFlag;
	uint8_t P0_Sliding_Time;
	uint8_t P2_Sliding_Time;
}ExcessOutputStruct;
void ReceiveExcessOutputData(uint8_t *data);
extern ExcessOutputStruct excessOutStruct;
#endif
