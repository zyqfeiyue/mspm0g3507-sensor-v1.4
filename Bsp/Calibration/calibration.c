#include "calibration.h"
#include "soft_timer.h"
#include "eeprom_emulation_type_b.h"
#include "can_unique_id.h"

sensor_calibration_type sensor_calibration_struct;
#define get_compensate_lookup_table sensor_operatime_compensated
uint32_t read_backup_operation_hours_seconds(void);

void calibration_init(void)
{
	uint32_t EEPROMEmulationState;
	EEPROMEmulationState = EEPROM_TypeB_init();
	if (EEPROMEmulationState == EEPROM_EMULATION_INIT_ERROR ||
			EEPROMEmulationState == EEPROM_EMULATION_TRANSFER_ERROR) {
			__BKPT(0);
	}
//	EEPROMEmulationState = EEPROM_TypeB_write(0x00, 155);
//	if (EEPROMEmulationState != EEPROM_EMULATION_WRITE_OK) {
//			__BKPT(0);
//	}
}
/*-----------------------------------------------------------------------
 函数: static  uint8_t NOxOperaTime_Compensated(uint32_t LifeSeconds)
 描述:
 参数:
 返回: none.
 备注:
------------------------------------------------------------------------*/
static uint8_t sensor_operatime_compensated(uint32_t life_seconds)
{
	uint8_t run_phase;

	run_phase = (uint8_t)(life_seconds / 1080000);//720000对应于200小时//1080000对应于300小时
	if(run_phase >= 14) run_phase = 14;

	return run_phase;
}

/*-----------------------------------------------------------------------
 函数: uint8_t  eeprom_write_verify( uint16_t addr, uint8_t *pDat, uint8_t length,ObdStructType* OST)
 描述:   带校验的参数写
 参数:   地址，数据，长度，OBD结构体
 返回:   校验失败数量
 备注:
------------------------------------------------------------------------*/
uint8_t eeprom_write_data( uint16_t addr, eeprom_data_union eeprom_data, sensor_hardware_type *sensor_hardware)
{
	uint8_t ret_err_counter = 0;
	if((sensor_hardware->write_eeprom_flag != 0x45) && (sensor_hardware->write_eeprom_flag != 0x85))return  0xFF;  //说明在忙线
	//--清忙线标志
	uint32_t EEPROMEmulationState;
	EEPROMEmulationState = EEPROM_TypeB_write(addr, eeprom_data.data);
	if (EEPROMEmulationState != EEPROM_EMULATION_WRITE_OK) {
			__BKPT(0);
	}
	sensor_hardware->write_eeprom_flag = 0x56;
	return ret_err_counter;
}
/*-----------------------------------------------------------------------
 函数: void eeprom_read( uint16_t addr, uint8_t *pDat, uint8_t length )
 描述:  addr EEPROM中的地址，从0开始，在函数写时，加入了初始地址
 参数:  pDat
 返回: none.
 备注:
------------------------------------------------------------------------*/
eeprom_data_union eeprom_read_data(uint16_t addr)
{
	eeprom_data_union eeprom_data;
	eeprom_data.data = EEPROM_TypeB_readDataItem(addr);
	return eeprom_data;
}
/*-----------------------------------------------------------------------
 函数: void WriteHotTimeSecondsToEEP(uint32_t XCounter,uint16_t EEPAdr,sensor_type *sensor)
 描述:  写入寿命秒值到指定地址
 参数:
 返回: none.
 备注:
------------------------------------------------------------------------*/
void write_operation_hours_seconds_to_eeprom(sensor_hardware_type *sensor_hardware, uint32_t operation_hours_seconds)
{
	static uint16_t read_data_high;
	static uint16_t write_data_high;
	static uint16_t read_data_low;
	static uint16_t write_data_low;

	if(operation_hours_seconds >= 129600000) // 36000h
		operation_hours_seconds = 129600000;
	write_data_low = operation_hours_seconds & 0xFFFF;
	write_data_high = (operation_hours_seconds >> 16) & 0xFFFF;
	eeprom_data_union read_data = eeprom_read_data(EEPROM_ADDRESS_OPERATION_HOURS_LOW);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF)
	{
		read_data_low = read_data.bit.original;
	}
	if (read_data_low != write_data_low)
	{
		eeprom_data_union write_data;
		write_data.bit.original = write_data_low;
		write_data.bit.invert = ~write_data_low;
		sensor_hardware->write_eeprom_flag = 0x45;
		eeprom_write_data(EEPROM_ADDRESS_OPERATION_HOURS_LOW, write_data, sensor_hardware);
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_OPERATION_HOURS_HIGH);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF)
	{
		read_data_high = read_data.bit.original;
	}
	if (read_data_high != write_data_high)
	{
		eeprom_data_union write_data;
		write_data.bit.original = write_data_high;
		write_data.bit.invert = ~write_data_high;
		sensor_hardware->write_eeprom_flag = 0x45;
		eeprom_write_data(EEPROM_ADDRESS_OPERATION_HOURS_HIGH, write_data, sensor_hardware);
	}
}
/*---------------------------------------------------------------------------------
 函数: void  TimeAging_Compensation(uint16_t addr,time_type *time)
 描述: 老化补偿模块
 参数:
 返回:
 备注:
----------------------------------------------------------------------------------*/
void timeaging_compensation_process(sensor_hardware_type *sensor_hardware, system_type * system)
{
	static uint32_t now_time = 0;
	static uint32_t active_time = 0;
	if (system->dewpoint_command == true)
	{
		now_time = soft_timer_tick_counter_get();
		if (now_time - active_time >= TIM_60_S_BASE_1MS)
		{
			active_time = soft_timer_tick_counter_get();
			sensor_hardware->life_seconds += 60;
			sensor_hardware->opera_compensated_factor = get_compensate_lookup_table(sensor_hardware->life_seconds);
			if (read_backup_operation_hours_seconds() < sensor_hardware->life_seconds)
			{
				write_operation_hours_seconds_to_eeprom(sensor_hardware, sensor_hardware->life_seconds);
			}
		}
	}
	else
	{
		active_time = soft_timer_tick_counter_get();
	}
}



/*-----------------------------------------------------------------------
 函数: uint32_t ReadHotTimeSeconds(uint16_t EEPAdr)
 描述:
 参数:
 返回: none.
 备注:
------------------------------------------------------------------------*/
uint32_t read_operation_hours_seconds(void)
{
	uint32_t operation_hours_seconds = 0;
	eeprom_data_union read_data = eeprom_read_data(EEPROM_ADDRESS_OPERATION_HOURS_HIGH_BACKUP);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF)
	{
		operation_hours_seconds = (uint32_t)read_data.bit.original << 16;
	}
	else
	{}
	read_data = eeprom_read_data(EEPROM_ADDRESS_OPERATION_HOURS_LOW_BACKUP);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF)
	{
		operation_hours_seconds += read_data.bit.original;
	}
	else
	{}
	return operation_hours_seconds;
}

/*-----------------------------------------------------------------------
 函数: void BackupHotTime(uint16_t EEPAdr,system_type* ST,ObdStructType* OST)
 描述:  备份加热时间
 参数:
 返回: none.
 备注:
------------------------------------------------------------------------*/
void write_backup_operation_hours_seconds(sensor_hardware_type *sensor_hardware)
{
	eeprom_data_union read_data = eeprom_read_data(EEPROM_ADDRESS_OPERATION_HOURS_HIGH);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF)
	{
		sensor_hardware->write_eeprom_flag = 0x45;
		eeprom_write_data(EEPROM_ADDRESS_OPERATION_HOURS_HIGH_BACKUP, read_data, sensor_hardware);
	}
	else
	{}
	read_data = eeprom_read_data(EEPROM_ADDRESS_OPERATION_HOURS_LOW);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF)
	{
		sensor_hardware->write_eeprom_flag = 0x45;
		eeprom_write_data(EEPROM_ADDRESS_OPERATION_HOURS_LOW_BACKUP, read_data, sensor_hardware);
	}
	else
	{}
}

/*-----------------------------------------------------------------------
 函数: uint32_t read_backup_operation_hours_seconds(void)
 描述:  读备份加热时间
 参数:
 返回: none.
 备注:
------------------------------------------------------------------------*/
uint32_t read_backup_operation_hours_seconds(void)
{
	uint32_t backup_operation_hours_seconds = 0;
	eeprom_data_union read_data = eeprom_read_data(EEPROM_ADDRESS_OPERATION_HOURS_HIGH_BACKUP);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF)
	{
		backup_operation_hours_seconds = (uint32_t)read_data.bit.original << 16;
	}
	else
	{}
	read_data = eeprom_read_data(EEPROM_ADDRESS_OPERATION_HOURS_LOW_BACKUP);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF)
	{
		backup_operation_hours_seconds += read_data.bit.original;
	}
	else
	{}
	return backup_operation_hours_seconds;
}

/*-----------------------------------------------------------------------
 函数: void Read_CalibrationParameter(sensor_type *sensor,system_type * system  ,time_type *time)
 描述:
 参数:
 返回: none.
 备注:
------------------------------------------------------------------------*/
void read_calibration_parameter(sensor_calibration_type *sensor, system_type * system)
{
	eeprom_data_union read_data;
	//================================100ppmNOX low%O2===========================
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO100O2LOW_IP1);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no100_o2low_ip1 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO100O2LOW_IP2);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no100_o2low_ip2 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO100O2LOW_VP1);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no100_o2low_vp1 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO100O2LOW_VBIN);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no100_o2low_vbin = read_data.bit.original;
	}
	//==============================100ppmNOX high%O2============================
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO100O2HIGH_IP1);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no100_o2high_ip1 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO100O2HIGH_IP2);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no100_o2high_ip2 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO100O2HIGH_VP1);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no100_o2high_vp1 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO100O2HIGH_VBIN);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no100_o2high_vbin = read_data.bit.original;
	}
	//==============================500ppmNOX low%O2==============================
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO500O2LOW_IP1);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no500_o2low_ip1 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO500O2LOW_IP2);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no500_o2low_ip2 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO500O2LOW_VP1);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no500_o2low_vp1 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO500O2LOW_VBIN);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no500_o2low_vbin = read_data.bit.original;
	}
	//==============================500ppmNOX high%O2============================
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO500O2HIGH_IP1);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no500_o2high_ip1 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO500O2HIGH_IP2);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no500_o2high_ip2 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO500O2HIGH_VP1);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no500_o2high_vp1 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO500O2HIGH_VBIN);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no500_o2high_vbin = read_data.bit.original;
	}
	//==============================1500ppmNOX low%O2==============================
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO1500O2LOW_IP1);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no1500_o2low_ip1 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO1500O2LOW_IP2);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no1500_o2low_ip2 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO500O2LOW_VP1);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no1500_o2low_vp1 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO1500O2LOW_VBIN);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no1500_o2low_vbin = read_data.bit.original;
	}
	//==============================1500ppmNOX high%O2============================
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO1500O2HIGH_IP1);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no1500_o2high_ip1 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO1500O2HIGH_IP2);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no1500_o2high_ip2 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO1500O2HIGH_VP1);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no1500_o2high_vp1 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO1500O2HIGH_VBIN);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no1500_o2high_vbin = read_data.bit.original;
	}
	//==============================0ppmNOX high%O2============================
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO0O2HIGH_IP1);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no0_o2high_ip1 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO0O2HIGH_IP2);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no0_o2high_ip2 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO0O2HIGH_VP1);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no0_o2high_vp1 = read_data.bit.original;
	}
	read_data = eeprom_read_data(EEPROM_ADDRESS_NO0O2HIGH_VBIN);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_no0_o2high_vbin = read_data.bit.original;
	}
	//==============================ratio=============================================
	read_data = eeprom_read_data(EEPROM_ADDRESS_VS_BASE);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_vs = read_data.bit.original;
		if(sensor->calibration_vs > 550) sensor->calibration_vs = 425;
	}
	else sensor->calibration_vs = 425;
	read_data = eeprom_read_data(EEPROM_ADDRESS_RPVS_BASE);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->calibration_rpvs = read_data.bit.original;
	}
	else sensor->calibration_rpvs = 250;
	///============================operation time==============================================
//	sensor->life_seconds = read_operation_hours_seconds();

	//==============================修正系数======================================
	read_data = eeprom_read_data(EEPROM_ADDRESS_NOx100_ADD);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->no100_add = read_data.bit.original;
	}
	else sensor->no100_add = 0;
	read_data = eeprom_read_data(EEPROM_ADDRESS_NOx500_ADD);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->no500_add = read_data.bit.original;
	}
	else sensor->no500_add = 0;
	read_data = eeprom_read_data(EEPROM_ADDRESS_NOx500_FACTOR);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->no500_factor = read_data.bit.original;
	}
	else sensor->no500_factor = 32;
	read_data = eeprom_read_data(EEPROM_ADDRESS_NOx1500_ADD);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->no1500_add = read_data.bit.original;
	}
	else sensor->no1500_add = 0;
	read_data = eeprom_read_data(EEPROM_ADDRESS_NOx1500_FACTOR);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->no1500_factor = read_data.bit.original;
	}
	else sensor->no1500_factor = 32;
	read_data = eeprom_read_data(EEPROM_ADDRESS_NOx3000_ADD);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->no3000_add = read_data.bit.original;
	}
	else sensor->no3000_add = 0;
	read_data = eeprom_read_data(EEPROM_ADDRESS_NOx3000_FACTOR);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->no3000_factor = read_data.bit.original;
	}
	else sensor->no3000_factor = 32;
	
	//==============================PID参数======================================
	read_data = eeprom_read_data(EEPROM_ADDRESS_VS_P);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->vs_pid.p = (float)read_data.bit.original / 100;
	}
	else sensor->vs_pid.p = 15;
	read_data = eeprom_read_data(EEPROM_ADDRESS_VS_I);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->vs_pid.i = (float)read_data.bit.original / 100;
	}
	else sensor->vs_pid.i = 2;
	read_data = eeprom_read_data(EEPROM_ADDRESS_VS_D);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->vs_pid.d = (float)read_data.bit.original / 100;
	}
	else sensor->vs_pid.d = 2;
	read_data = eeprom_read_data(EEPROM_ADDRESS_RPVS_P);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->rpvs_pid.p = (float)read_data.bit.original / 100;
	}
	else sensor->rpvs_pid.p = 15;
	read_data = eeprom_read_data(EEPROM_ADDRESS_RPVS_I);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->rpvs_pid.i = (float)read_data.bit.original / 100;
	}
	else sensor->rpvs_pid.i = 2;
	read_data = eeprom_read_data(EEPROM_ADDRESS_RPVS_D);
	if((read_data.bit.original + read_data.bit.invert) == 0xFFFF) {
		sensor->rpvs_pid.d = (float)read_data.bit.original / 100;
	}
	else sensor->rpvs_pid.d = 2;

//	UniqueID_ReadCalibration_ReadSN();
//	write_backup_operation_hours_seconds(sensor); // 备份加热时间
}


/*-----------------------------------------------------------------------
 函数: void Cali_AppParameter( sensor_type *sensor,system_type * system,time_type *time,HeaterType *pHEA)
 描述:  标定应用参数
 参数:
 返回: none.
 备注:		   气氛标定从未发生过上位机所有标定点完成而EEPROM里丢失数据问题2021-6-9
------------------------------------------------------------------------*/
void write_calibration_parameter( sensor_calibration_type *sensor, sensor_handle_type *sensor_handle, system_type * system, can_handle_type *can_handle)
{
	static uint32_t nowtime = 0;
	static uint32_t active_time_10ms = 0;
	static eeprom_data_union write_data;
	sensor_data_type *sensor_data = &sensor_handle->sensor_data_struct;
	sensor_hardware_type *sensor_hardware = &sensor_handle->sensor_hardware_struct;
	nowtime = soft_timer_tick_counter_get();
	//--10ms 周期事件处理--在中断里产生10ms标志位
	if (nowtime - active_time_10ms > 10 )
	{
		active_time_10ms = soft_timer_tick_counter_get();
		//--标定超时计数
		if( sensor->calibration_count < 0xFFFF ) 
			sensor->calibration_count++;
	}
	if(sensor->calibration_count >= CALIBRATION_TIME_BASE_10MS) //标定超时否  10ms递加
	{
		system->mode = SYSTEM_MODE_DEBUG;//超时后退回到SYSTEM_MODE_DEBUG模式
		can_handle->can_debug_count = 0;         //清零Debug计数器
		//time->calibration_count = 0;       //在此可以不用清零，CAN接收成功后会清零
	}
	else if(CALIBRATION_NUMBER_NONE < sensor->calibration_number && sensor->calibration_number < CALIBRATION_NUMBER_MAX)//保证标定参数在NONE~MAX之间算合法
	{
		//标定气氛与参数>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		switch( sensor->calibration_number )
		{
			case CALIBRATION_NUMBER_HEATER:
				break;

			case CALIBRATION_NUMBER_NO100O2LOW:
				sensor->calibration_no100_o2low_ip1 = sensor_data->ip1_filt;
				sensor->calibration_no100_o2low_ip2 = sensor_data->ip2_filt;
				sensor->calibration_no100_o2low_vp1 = sensor_data->vp1_data;
				write_data.bit.original = sensor->calibration_no100_o2low_ip1;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO100O2LOW_IP1, write_data, sensor_hardware);
				write_data.bit.original = sensor->calibration_no100_o2low_ip2;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO100O2LOW_IP2, write_data, sensor_hardware);
				write_data.bit.original = sensor->calibration_no100_o2low_vp1;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO100O2LOW_VP1, write_data, sensor_hardware);
				sensor->calibration_number = CALIBRATION_NUMBER_MAX;
				can_handle->can_debug_count = 100;//输出标定后的消息后退出到SYSTEM_MODE_DEBUG模式
				break;

			case CALIBRATION_NUMBER_NO100O2HIGH:
				sensor->calibration_no100_o2high_ip1 = sensor_data->ip1_filt;
				sensor->calibration_no100_o2high_ip2 = sensor_data->ip2_filt;
				sensor->calibration_no100_o2high_vp1 = sensor_data->vp1_data;
				write_data.bit.original = sensor->calibration_no100_o2high_ip1;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO100O2HIGH_IP1, write_data, sensor_hardware);
				write_data.bit.original = sensor->calibration_no100_o2high_ip2;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO100O2HIGH_IP2, write_data, sensor_hardware);
				write_data.bit.original = sensor->calibration_no100_o2high_vp1;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO100O2HIGH_VP1, write_data, sensor_hardware);
				sensor->calibration_number = CALIBRATION_NUMBER_MAX;
				can_handle->can_debug_count = 100;//输出标定后的消息后退出到SYSTEM_MODE_DEBUG模式
				break;

			case CALIBRATION_NUMBER_NO500O2LOW:
				sensor->calibration_no500_o2low_ip1 = sensor_data->ip1_filt;
				sensor->calibration_no500_o2low_ip2 = sensor_data->ip2_filt;
				sensor->calibration_no500_o2low_vp1 = sensor_data->vp1_data;
//				sensor->calibration_no500_o2low_vbin = sensor->vbin;
				write_data.bit.original = sensor->calibration_no500_o2low_ip1;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO500O2LOW_IP1, write_data, sensor_hardware);
				write_data.bit.original = sensor->calibration_no500_o2low_ip2;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO500O2LOW_IP2, write_data, sensor_hardware);
				write_data.bit.original = sensor->calibration_no500_o2low_vp1;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO500O2LOW_VP1, write_data, sensor_hardware);
//				sensor_hardware->write_eeprom_flag = 0x85;
//				eeprom_write_verify( EEPROM_ADDRESS_NO500O2LOW_VBIN, system->eeprom_buffer, 2 ,sensor_hardware);
				sensor->calibration_number = CALIBRATION_NUMBER_MAX;
				can_handle->can_debug_count = 100;//输出标定后的消息后退出到SYSTEM_MODE_DEBUG模式
				break;

			case CALIBRATION_NUMBER_NO500O2HIGH:
				sensor->calibration_no500_o2high_ip1 = sensor_data->ip1_filt;
				sensor->calibration_no500_o2high_ip2 = sensor_data->ip2_filt;
				sensor->calibration_no500_o2high_vp1 = sensor_data->vp1_data;
//				sensor->calibration_no500_o2high_vbin = sensor->vbin;
				write_data.bit.original = sensor->calibration_no500_o2high_ip1;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO500O2HIGH_IP1, write_data, sensor_hardware);
				write_data.bit.original = sensor->calibration_no500_o2high_ip2;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO500O2HIGH_IP2, write_data, sensor_hardware);
				write_data.bit.original = sensor->calibration_no500_o2high_vp1;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO500O2HIGH_VP1, write_data, sensor_hardware);
//				sensor_hardware->write_eeprom_flag = 0x85;
//				eeprom_write_verify( EEPROM_ADDRESS_NO500O2HIGH_VBIN, system->eeprom_buffer, 2 ,sensor_hardware);
				sensor->calibration_number = CALIBRATION_NUMBER_MAX;
				can_handle->can_debug_count = 100;//输出标定后的消息后退出到SYSTEM_MODE_DEBUG模式
				break;

			case CALIBRATION_NUMBER_NO1500O2LOW:
				sensor->calibration_no1500_o2low_ip1 = sensor_data->ip1_filt;
				sensor->calibration_no1500_o2low_ip2 = sensor_data->ip2_filt;
				sensor->calibration_no1500_o2low_vp1 = sensor_data->vp1_data;
//				sensor->calibration_no1500_o2low_vbin = sensor->vbin;
				write_data.bit.original = sensor->calibration_no1500_o2low_ip1;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO1500O2LOW_IP1, write_data, sensor_hardware);
				write_data.bit.original = sensor->calibration_no1500_o2low_ip2;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO1500O2LOW_IP2, write_data, sensor_hardware);
				write_data.bit.original = sensor->calibration_no1500_o2low_vp1;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO1500O2LOW_VP1, write_data, sensor_hardware);
//				sensor_hardware->write_eeprom_flag = 0x85;
//				eeprom_write_verify( EEPROM_ADDRESS_NO1500O2LOW_VBIN, system->eeprom_buffer, 2 ,sensor_hardware);
				sensor->calibration_number = CALIBRATION_NUMBER_MAX;
				can_handle->can_debug_count = 100;//输出标定后的消息后退出到SYSTEM_MODE_DEBUG模式
				break;
			case CALIBRATION_NUMBER_NO1500O2HIGH:
				sensor->calibration_no1500_o2high_ip1 = sensor_data->ip1_filt;
				sensor->calibration_no1500_o2high_ip2 = sensor_data->ip2_filt;
				sensor->calibration_no1500_o2high_vp1 = sensor_data->vp1_data;
//				sensor->calibration_no1500_o2high_vbin = sensor->vbin;
				write_data.bit.original = sensor->calibration_no1500_o2high_ip1;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO1500O2HIGH_IP1, write_data, sensor_hardware);
				write_data.bit.original = sensor->calibration_no1500_o2high_ip2;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO1500O2HIGH_IP2, write_data, sensor_hardware);
				write_data.bit.original = sensor->calibration_no1500_o2high_vp1;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO1500O2HIGH_VP1, write_data, sensor_hardware);
//				system->eeprom_buffer[ 0 ] = sensor->calibration_no1500_o2high_vbin;
//				system->eeprom_buffer[ 1 ] = ~system->eeprom_buffer[ 0 ];
//				sensor_hardware->write_eeprom_flag = 0x85;
//				eeprom_write_verify( EEPROM_ADDRESS_NO1500O2HIGH_VBIN, system->eeprom_buffer, 2 ,sensor_hardware);
				sensor->calibration_number = CALIBRATION_NUMBER_MAX;
				can_handle->can_debug_count = 100;//输出标定后的消息后退出到SYSTEM_MODE_DEBUG模式
				break;
			case CALIBRATION_NUMBER_NO0O2HIGH:
				sensor->calibration_no0_o2high_ip1 = sensor_data->ip1_filt;
				sensor->calibration_no0_o2high_ip2 = sensor_data->ip2_filt;
				sensor->calibration_no0_o2high_vp1 = sensor_data->vp1_data;
//				sensor->calibration_no0_o2high_vbin = sensor->vbin;
				write_data.bit.original = sensor->calibration_no0_o2high_ip1;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO0O2HIGH_IP1, write_data, sensor_hardware);
				write_data.bit.original = sensor->calibration_no0_o2high_ip2;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO0O2HIGH_IP2, write_data, sensor_hardware);
				write_data.bit.original = sensor->calibration_no0_o2high_vp1;
				write_data.bit.invert = ~write_data.bit.original;
				sensor_hardware->write_eeprom_flag = 0x85;
				eeprom_write_data(EEPROM_ADDRESS_NO0O2HIGH_VP1, write_data, sensor_hardware);
//				sensor_hardware->write_eeprom_flag = 0x85;
//				eeprom_write_verify( EEPROM_ADDRESS_NO0O2HIGH_VBIN, system->eeprom_buffer, 2 ,sensor_hardware);
				sensor->calibration_number = CALIBRATION_NUMBER_MAX;
				can_handle->can_debug_count = 100;//输出标定后的消息后退出到SYSTEM_MODE_DEBUG模式
				break;
			default:break;
		}
		//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	}
}

void modify_calibration_parameter(uint8_t* receive_data, sensor_handle_type *sensor_handle, system_type * system, sensor_calibration_type *sensor)
{
	#ifdef CAN_W_R_CALIBRATION  //支持写参数命令
	sensor_data_type *sensor_data = &sensor_handle->sensor_data_struct;
	sensor_hardware_type *sensor_hardware = &sensor_handle->sensor_hardware_struct;
	uint8_t temp;
	int16_t sdata1, sdata2, sdata3;
	uint8_t u8data1, u8data2, u8data3;
	int8_t s8data1, s8data2, s8data3, s8data4;
	static eeprom_data_union write_data;
	switch (receive_data[0])
	{
		case(0xA0): // CaliNO100O2LOW
			sdata1 = receive_data[ 2 ];
			sdata1 <<= 8;
			sdata1 += receive_data[ 1 ]; //sensor->CaliNO100O1Ip0
			sdata2 = receive_data[ 6 ];
			sdata2 <<= 8;
			sdata2 += receive_data[ 5 ];//sensor->CaliNO100O1Ip2
			sdata3 = receive_data[ 4 ];
			sdata3 <<= 8;
			sdata3 += receive_data[ 3 ];//sensor->CaliNO100O1Pwm
			write_data.bit.original = sdata1;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO100O2LOW_IP1, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no100_o2low_ip1 = sdata1;
			}
			write_data.bit.original = sdata2;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO100O2LOW_IP2, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no100_o2low_ip2 = sdata2;
			}
			write_data.bit.original = sdata3;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO100O2LOW_VP1, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no100_o2low_vp1 = sdata3;
			}
			break;
		case(0xA1): // CaliNO100O2HIGH
			sdata1 = receive_data[ 2 ];
			sdata1 <<= 8;
			sdata1 += receive_data[ 1 ];
			sdata2 = receive_data[ 4 ];
			sdata2 <<= 8;
			sdata2 += receive_data[ 3 ];
			sdata3 = receive_data[ 7 ];
			sdata3 <<= 8;
			sdata3 += receive_data[ 6 ];
			write_data.bit.original = sdata1;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO100O2HIGH_IP1, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no100_o2high_ip1 = sdata1;
			}
			write_data.bit.original = sdata2;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO100O2HIGH_IP2, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no100_o2high_ip2 = sdata2;
			}
			write_data.bit.original = sdata3;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO100O2HIGH_VP1, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no100_o2high_vp1 = sdata3;
			}
			break;
		case(0xA2): // CaliNO500O2LOWIp0
			sdata1 = receive_data[ 2 ];
			sdata1 <<= 8;
			sdata1 += receive_data[ 1 ]; //sensor->CaliNO500O1Ip0
			sdata2 = receive_data[ 4 ];
			sdata2 <<= 8;
			sdata2 += receive_data[ 3 ];//sensor->CaliNO500O1Ip2
			sdata3 = receive_data[ 7 ];
			sdata3 <<= 8;
			sdata3 += receive_data[ 6 ];//sensor->CaliNO500O1Pwm
			write_data.bit.original = sdata1;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO500O2LOW_IP1, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no500_o2low_ip1 = sdata1;
			}
			write_data.bit.original = sdata2;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO500O2LOW_IP2, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no500_o2low_ip2 = sdata2;
			}
			write_data.bit.original = sdata3;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO500O2LOW_VP1, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no500_o2low_vp1 = sdata3;
			}
			break;
		case(0xA3): // CaliNO500OHIGHIp0
			sdata1 = receive_data[ 2 ];
			sdata1 <<= 8;
			sdata1 += receive_data[ 1 ]; // sensor->CaliNO500O16Ip0
			sdata2 = receive_data[ 4 ];
			sdata2 <<= 8;
			sdata2 += receive_data[ 3 ]; // sensor->CaliNO500O16Ip2
			sdata3 = receive_data[ 7 ];
			sdata3 <<= 8;
			sdata3 += receive_data[ 6 ]; //sensor->CaliNO500O16Pwm
			write_data.bit.original = sdata1;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO500O2HIGH_IP1, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no500_o2high_ip1 = sdata1;
			}
			write_data.bit.original = sdata2;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO500O2HIGH_IP2, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no500_o2high_ip2 = sdata2;
			}
			write_data.bit.original = sdata3;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO500O2HIGH_VP1, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no500_o2high_vp1 = sdata3;
			}
			break;
		case(0xA4): // CaliNO1500O2LOWIp0
			sdata1 = receive_data[ 2 ];
			sdata1 <<= 8;
			sdata1 += receive_data[ 1 ]; // sensor->CaliNO500O16Ip0
			sdata2 = receive_data[ 4 ];
			sdata2 <<= 8;
			sdata2 += receive_data[ 3 ]; // sensor->CaliNO500O16Ip2
			sdata3 = receive_data[ 7 ];
			sdata3 <<= 8;
			sdata3 += receive_data[ 6 ]; //sensor->CaliNO500O16Pwm
			write_data.bit.original = sdata1;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO1500O2LOW_IP1, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no1500_o2low_ip1 = sdata1;
			}
			write_data.bit.original = sdata2;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO1500O2LOW_IP2, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no1500_o2low_ip2 = sdata2;
			}
			write_data.bit.original = sdata3;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO1500O2LOW_VP1, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no1500_o2low_vp1 = sdata3;
			}
			break;
		case(0xA5): // CaliNO1500O2HIGHIp0
			sdata1 = receive_data[ 2 ];
			sdata1 <<= 8;
			sdata1 += receive_data[ 1 ]; // sensor->CaliNO500O16Ip0
			sdata2 = receive_data[ 4 ];
			sdata2 <<= 8;
			sdata2 += receive_data[ 3 ]; // sensor->CaliNO500O16Ip2
			sdata3 = receive_data[ 7 ];
			sdata3 <<= 8;
			sdata3 += receive_data[ 6 ]; //sensor->CaliNO500O16Pwm
			write_data.bit.original = sdata1;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO1500O2HIGH_IP1, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no1500_o2high_ip1 = sdata1;
			}
			write_data.bit.original = sdata2;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO1500O2HIGH_IP2, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no1500_o2high_ip2 = sdata2;
			}
			write_data.bit.original = sdata3;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO1500O2HIGH_VP1, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no1500_o2high_vp1 = sdata3;
			}
			break;
		case(0xA6): // CaliNO0O2HIGHIp0
			sdata1 = receive_data[ 2 ];
			sdata1 <<= 8;
			sdata1 += receive_data[ 1 ]; // sensor->CaliNO0O16Ip0
			sdata2 = receive_data[ 4 ];
			sdata2 <<= 8;
			sdata2 += receive_data[ 3 ]; // sensor->CaliNO0O16Ip2
			sdata3 = receive_data[ 7 ];
			sdata3 <<= 8;
			sdata3 += receive_data[ 6 ]; //sensor->CaliNO0O16Pwm
			write_data.bit.original = sdata1;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO0O2HIGH_IP1, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no0_o2high_ip1 = sdata1;
			}
			write_data.bit.original = sdata2;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO0O2HIGH_IP2, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no0_o2high_ip2 = sdata2;
			}
			write_data.bit.original = sdata3;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_NO0O2HIGH_VP1, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_no0_o2high_vp1 = sdata3;
			}
			break;
		case(0xA7): // vs rpvs
			sdata1 = receive_data[ 2 ]; // vs
			sdata1 <<= 8;
			sdata1 += receive_data[ 1 ]; // rpvs
			sdata2 = receive_data[ 4 ];
			write_data.bit.original = sdata1;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_VS_BASE, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_vs = sdata1;
			}
			write_data.bit.original = sdata2;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_RPVS_BASE, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->calibration_rpvs = sdata2;
			}
			break;
//		case(0xA8)://sensor->no100_add no500_add no500_factor no1500_add no1500_factor no3000_add no3000_factor
//			s8data1 = (int8_t)receive_data[ 1 ]; // sensor->no100_add-s8
//			s8data2 = (int8_t)receive_data[ 2 ]; // sensor->no500_add-s8
//			u8data1 = (uint8_t)receive_data[ 3 ]; // sensor->no500_factor--  ----u8
//			s8data3 = (int8_t)receive_data[ 4 ]; //sensor->no1500_add-s8
//			u8data2 = (uint8_t)receive_data[ 5 ]; //sensor->no1500_factor-----  u8
//			s8data4 = (int8_t)receive_data[ 6 ]; //sensor->no3000_add-s8
//			u8data3 = (uint8_t)receive_data[ 7 ]; //sensor->no3000_factor-----  u8
//			if((-30<= s8data1) && (s8data1 <= 30))
//			{
//				system->eeprom_buffer[ 0 ] = s8data1;
//				system->eeprom_buffer[ 1 ] = ~system->eeprom_buffer[ 0 ];
//				sensor->write_eeprom_flag = 0x45;
//				if(eeprom_write_verify( EEPROM_ADDRESS_NOx100_ADD, system->eeprom_buffer, 2,sensor)==0)//写入成功则直接赋值
//				{
//					sensor->no100_add = s8data1;
//				}
//			}
//			if((-125<= s8data2) && (s8data2 <= 125))
//			{
//				system->eeprom_buffer[ 0 ] = s8data2;
//				system->eeprom_buffer[ 1 ] = ~system->eeprom_buffer[ 0 ];
//				sensor->write_eeprom_flag = 0x45;
//				if(eeprom_write_verify( EEPROM_ADDRESS_NOx500_ADD, system->eeprom_buffer, 2,sensor)==0)//写入成功则直接赋值
//				{
//					sensor->no500_add = s8data2;
//				}
//			}
//			if((16<= u8data1) && (u8data1 <= 48))
//			{
//				system->eeprom_buffer[ 0 ] = u8data1;
//				system->eeprom_buffer[ 1 ] = ~system->eeprom_buffer[ 0 ];
//				sensor->write_eeprom_flag = 0x45;
//				if(eeprom_write_verify( EEPROM_ADDRESS_NOx500_FACTOR, system->eeprom_buffer, 2,sensor)==0)//写入成功则直接赋值
//				{
//					sensor->no500_factor = u8data1;
//				}
//			}
//			if((-125<= s8data3) && (s8data3 <= 125))
//			{
//				system->eeprom_buffer[ 0 ] = s8data3;
//				system->eeprom_buffer[ 1 ] = ~system->eeprom_buffer[ 0 ];
//				sensor->write_eeprom_flag = 0x45;
//				if(eeprom_write_verify( EEPROM_ADDRESS_NOx1500_ADD, system->eeprom_buffer, 2,sensor)==0)//写入成功则直接赋值
//				{
//					sensor->no1500_add = s8data3;
//				}
//			}
//			if((16<= u8data2) && (u8data2 <= 48))
//			{
//				system->eeprom_buffer[ 0 ] = u8data2;
//				system->eeprom_buffer[ 1 ] = ~system->eeprom_buffer[ 0 ];
//				sensor->write_eeprom_flag = 0x45;
//				if(eeprom_write_verify( EEPROM_ADDRESS_NOx1500_FACTOR, system->eeprom_buffer, 2,sensor)==0)//写入成功则直接赋值
//				{
//					sensor->no1500_factor = u8data2;
//				}
//			}
//			if((-125<= s8data4) && (s8data4 <= 125))
//			{
//				system->eeprom_buffer[ 0 ] = s8data4;
//				system->eeprom_buffer[ 1 ] = ~system->eeprom_buffer[ 0 ];
//				sensor->write_eeprom_flag = 0x45;
//				if(eeprom_write_verify( EEPROM_ADDRESS_NOx3000_ADD, system->eeprom_buffer, 2,sensor)==0)//写入成功则直接赋值
//				{
//					sensor->no3000_add = s8data4;
//				}
//			}
//			if((16<= u8data3) && (u8data3 <= 48))
//			{
//				system->eeprom_buffer[ 0 ] = u8data3;
//				system->eeprom_buffer[ 1 ] = ~system->eeprom_buffer[ 0 ];
//				sensor->write_eeprom_flag = 0x45;
//				if(eeprom_write_verify( EEPROM_ADDRESS_NOx3000_FACTOR, system->eeprom_buffer, 2,sensor)==0)//写入成功则直接赋值
//				{
//					sensor->no3000_factor = u8data3;
//				}
//			}
//			break;
		case(0xB0): // VS_PID
			sdata1 = receive_data[ 2 ];
			sdata1 <<= 8;
			sdata1 += receive_data[ 1 ]; // P
			sdata2 = receive_data[ 4 ];
			sdata2 <<= 8;
			sdata2 += receive_data[ 3 ]; // I
			sdata3 = receive_data[ 6 ];
			sdata3 <<= 8;
			sdata3 += receive_data[ 5 ]; // D
			write_data.bit.original = sdata1;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_VS_P, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->vs_pid.p=(float)sdata1 / 100;
			}
			write_data.bit.original = sdata2;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_VS_I, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->vs_pid.i=(float)sdata2 / 100;
			}
			write_data.bit.original = sdata3;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_VS_D, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->vs_pid.d=(float)sdata3 / 100;
			}
			break;
		case(0xB1): // V0_PID
			sdata1 = receive_data[ 2 ];
			sdata1 <<= 8;
			sdata1 += receive_data[ 1 ]; // P
			sdata2 = receive_data[ 4 ];
			sdata2 <<= 8;
			sdata2 += receive_data[ 3 ]; // I
			sdata3 = receive_data[ 6 ];
			sdata3 <<= 8;
			sdata3 += receive_data[ 5 ]; // D
			write_data.bit.original = sdata1;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_RPVS_P, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->rpvs_pid.p = (float)sdata1 / 100;
			}
			write_data.bit.original = sdata2;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_RPVS_I, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->rpvs_pid.i=(float)sdata2 / 100;
			}
			write_data.bit.original = sdata3;
			write_data.bit.invert = ~write_data.bit.original;
			sensor_hardware->write_eeprom_flag = 0x45;
			if(eeprom_write_data(EEPROM_ADDRESS_RPVS_D, write_data, sensor_hardware) == 0)//写入成功则直接赋值
			{
				sensor->rpvs_pid.d=(float)sdata3 / 100;
			}
			break;
		default:
			break;
	}
	#endif
}

/******************* (C) COPYRIGHT 2021 Shenzhen SenSorTech co.,LTD  www.sensor-auto.com*/
/*****END OF FILE****/

