/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CAN_UNIQUE_ID_H__
#define __CAN_UNIQUE_ID_H__
#include "main.h"
//#include "calibration.h"
#include "can_control.h"
#define ENABLE_UNIQUE_ID_MODE
/*传感器序列号定义*/
//新增传感器ID类型定义
/***********************************************************************************************************************************
上位机下发数据
          |---------Byte3---------|---------Byte4---------|---------Byte5---------|---------Byte6---------|---------Byte7----------|
          |b7 b6 b5 b4 b3 b2 b1 b0|b7 b6 b5 b4 b3 b2 b1 b0|b7 b6 b5 b4 b3 b2 b1 b0|b7 b6 b5 b4 b3 b2 b1 b0|b7 b6 b5 b4 b3 b2 b1 b0 |
          |01 02 03 04 05 06 07 08|09 10 11 12 13 14 15 16|17 18 19 20 21 22 23 24|25 26 27 28 29 30 31 32|33 34 35 36 37 38 39 40 |
          |<------------------------------------------共 计 40 个 bit 来 表 示 传 感 器 编 号------------------------------------->|
下位机数据处理
          |<-------------前29个bit作为传感器在UniqueID模式下的CAN ID--------------------------- >|<---后11个作为该模式下的数据              ---->|
实际CAN ID = 4Byte  CAN Data=1Byte+3bit
|---------ID0xxx--------|---------IDx0xx--------|---------IDxx0x--------|---------IDxxx0---------|---------data[0]-------| data[1] |
|31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00 |07 06 05 04 03 02 01 00|07 06 05 |
|XX XX XX|b7 b6 b5 b4 b3|b2 b1 b0|b7 b6 b5 b4 b3|b2 b1 b0|b7 b6 b5 b4 b3|b2 b1 b0|b7 b6 b5 b4 b3 |b2 b1 b0|b7 b6 b5 b4 b3|b2 b1 b0 |
         |  Byte3高5     |Byte3低3|   Byte4高5     |Byte4低3|   Byte5高5     |Byte5低3|   Byte6高5      |Byte6低3|   Byte7高5     |Byte7低3 |
************************************************************************************************************************************/
typedef struct
{
  uint8_t ID_b23_b21:    3;//byte3_b2:b0
  uint8_t ID_b28_b24:    5;//byte3_b7:b3

  uint8_t ID_b15_b13:    3;//byte4_b2:b0
  uint8_t ID_b20_b16:    5;//byte4_b7:b3

  uint8_t ID_b07_b05:    3;//byte5_b2:b0
  uint8_t ID_b12_b08:    5;//byte5_b7:b3

  uint8_t Data0_b07_b05: 3;//byte6_b2:b0
  uint8_t ID_b04_b00:    5;//byte6_b7:b3

  uint8_t Data1_b07_b05: 3;//byte7_b2:b0
  uint8_t Data0_b04_b00: 5;//byte7_b7:b3
} UNIQUE_ID_CANID_DATA0_DATA1;

typedef union
{
  uint8_t                          ReceiveData[5];//上位机写入传感器ID命令的byte3:byte7
  UNIQUE_ID_CANID_DATA0_DATA1 CanIdData01;   //按位域定义的传感器信息结构体
}UNIQUE_ID_SNINFO_UNION;

/*唯一ID模式结构体*/
typedef enum{Invalid,Global,Special}Uniqueid_RequestType;//0:无请求 1 全局请求 2特定请求
typedef struct//SN:Serial Number
{
  uint16_t                      SNTemp[10];
  UNIQUE_ID_SNINFO_UNION  SNInfo;
  uint32_t                     CanId;
  uint8_t                      CanData0;
  uint8_t                      CanData1H3;
  uint8_t                      CanOutIndex;
  Uniqueid_RequestType    CanOutRequestType;
}UNIQUE_ID;

extern UNIQUE_ID Uniqueid;

void UniqueID_ReadCalibration_ReadSN(void);
//void UniqueID_CAN_Comm_DP(can_handle_type *can_handle, sensor_type *sensor);//DP=During Product
//void UniqueID_CAN_HardFilterCfg(void);
//void UniqueID_CANRecInterrupt_WriteSN(can_receive_message_type *receive_data, sensor_type *sensor);
//void uniqueid_can_transmit_message(can_handle_type *can_handle, sensor_type *sensor);
#endif
