#include "can_command.h"

// 传感器硬件错误
void sensor_hardware_error(can_handle_type *can_handle, SENSOR_POSITION position)
{
	if (position == POSITION_EXHAUST)
		can_handle->transmit_message.id = 0x18EEFF52;
	else
		can_handle->transmit_message.id = 0x18EEFF51;
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	can_handle->transmit_message.data[0] = 0xA2;
	can_handle->transmit_message.data[1] = 0x30;
	can_handle->transmit_message.data[2] = 0x44;
	can_handle->transmit_message.data[3] = 0x16;
	can_handle->transmit_message.data[4] = 0x00;
	can_handle->transmit_message.data[5] = 0x44;
	can_handle->transmit_message.data[6] = 0x00;
	can_handle->transmit_message.data[7] = 0x00;
	if (can_handle->busoff_flag == 0)
	{
		can_transmit_message(&can_handle->transmit_message);
	}
}
// 传感器obd回传
void sensor_obd_command_ack(can_handle_type *can_handle, SENSOR_POSITION position, uint8_t ack)
{
	if (position == POSITION_EXHAUST)
		can_handle->transmit_message.id = 0x18E8FF52;
	else
		can_handle->transmit_message.id = 0x18E8FF51;
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	can_handle->transmit_message.data[0] = ack;
	can_handle->transmit_message.data[1] = 0xFF;
	can_handle->transmit_message.data[2] = 0xFF;
	can_handle->transmit_message.data[3] = 0xFF;
	can_handle->transmit_message.data[4] = can_handle->yy;
	can_handle->transmit_message.data[5] = can_handle->pgnlsb;
	can_handle->transmit_message.data[6] = can_handle->pgn;
	can_handle->transmit_message.data[7] = can_handle->pgnmsb;
	if (can_handle->busoff_flag == 0)
	{
		can_transmit_message(&can_handle->transmit_message);
	}
}
// 传感器obd繁忙
void sensor_obd_command_busy(can_handle_type *can_handle, SENSOR_POSITION position)
{
	sensor_obd_command_ack(can_handle, position, 0x03);
}
// 传感器obd can_handle->transmit_message.data2 
// 参数：加热ratio值 运行时间
void sensor_obd_command_data1(can_handle_type *can_handle, SENSOR_POSITION position, uint8_t hardware_error)
{
	if( position == POSITION_EXHAUST )
		can_handle->transmit_message.id = 0x18F00F52;
	else
		can_handle->transmit_message.id = 0x18F00E51;
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	can_handle->transmit_message.data[0] = can_handle->data1_signal_values_struct.nox_concentration_byte_01.bit.l;
	can_handle->transmit_message.data[1] = can_handle->data1_signal_values_struct.nox_concentration_byte_01.bit.h;
	can_handle->transmit_message.data[2] = can_handle->data1_signal_values_struct.o2_concentration_byte_23.bit.l;
	can_handle->transmit_message.data[3] = can_handle->data1_signal_values_struct.o2_concentration_byte_23.bit.h;
	can_handle->transmit_message.data[4] = can_handle->data1_signal_values_struct.status_byte_4.value;
	can_handle->transmit_message.data[5] = can_handle->data1_signal_values_struct.status_byte_5.value;
	can_handle->transmit_message.data[6] = can_handle->data1_signal_values_struct.status_byte_6.value;
	can_handle->transmit_message.data[7] = can_handle->data1_signal_values_struct.status_byte_7.value;
	if((hardware_error & 0x48) == 0x00)
	{
		can_transmit_message(&can_handle->transmit_message);
	}
}
// 传感器obd can_handle->transmit_message.data2 
// 参数：加热ratio值 运行时间
void sensor_obd_command_data2(can_handle_type *can_handle, SENSOR_POSITION position, uint8_t hardware_error)
{
	if( position == POSITION_EXHAUST )
		can_handle->transmit_message.id = 0x0CFD0F52;
	else
		can_handle->transmit_message.id = 0x0CFD1151;
	can_handle->transmit_message.data[0] = can_handle->data2_heater_ratio_deviation_struct.heater_ratio_byte_01.bit.l;
	can_handle->transmit_message.data[1] = can_handle->data2_heater_ratio_deviation_struct.heater_ratio_byte_01.bit.h;
	can_handle->transmit_message.data[2] = can_handle->data2_heater_ratio_deviation_struct.nox_correction_gain_byte_23.bit.l;
	can_handle->transmit_message.data[3] = can_handle->data2_heater_ratio_deviation_struct.nox_correction_gain_byte_23.bit.h;
	can_handle->transmit_message.data[4] = can_handle->data2_heater_ratio_deviation_struct.nox_correction_offset_byte_4;
	can_handle->transmit_message.data[5] = can_handle->data2_heater_ratio_deviation_struct.operation_hours_counter_byte_56.bit.l;
	can_handle->transmit_message.data[6] = can_handle->data2_heater_ratio_deviation_struct.operation_hours_counter_byte_56.bit.h;
	can_handle->transmit_message.data[7] = can_handle->data2_heater_ratio_deviation_struct.not_used_byte_7;
	if((hardware_error & 0x48) == 0x00)
	{
		can_transmit_message(&can_handle->transmit_message);
	}
}
// 传感器obd can_handle->transmit_message.data3
// 参数：加热ratio值 运行时间
void sensor_obd_command_data3(can_handle_type *can_handle, SENSOR_POSITION position, uint8_t hardware_error)
{
	if(position == POSITION_EXHAUST)
	{
		can_handle->transmit_message.id = 0x0CFD0E52;
	}
	else
	{
		can_handle->transmit_message.id = 0x0CFD1051;
	}
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	can_handle->transmit_message.data[0] = can_handle->data3_correction_factors_struct.correction_pressure_lambda_byte_0; // lambda校正范围0---200  
	can_handle->transmit_message.data[1] = can_handle->data3_correction_factors_struct.correction_pressure_nox_byte_1; // NOx压力校正
	can_handle->transmit_message.data[2] = can_handle->data3_correction_factors_struct.correction_no2_byte_2; // NO2校正
	can_handle->transmit_message.data[3] = can_handle->data3_correction_factors_struct.correction_nh3_byte_3;
	can_handle->transmit_message.data[4] = can_handle->data3_correction_factors_struct.self_diagnosis_result_value_byte_4;
	can_handle->transmit_message.data[5] = 0xFF;
	can_handle->transmit_message.data[6] = 0xFF;
	can_handle->transmit_message.data[7] = 0xFF;
	if (can_handle->adr == 0xFF || can_handle->adr == 0x51 || can_handle->adr == 0x52)
	{
		if((hardware_error & 0x48) == 0x00)
		{
			can_transmit_message(&can_handle->transmit_message);
		}
	}
	else
	{
	
	}
}
// 传感器obd can_handle->transmit_message.data4
// 参数：版本号
void sensor_obd_command_data4(can_handle_type *can_handle, SENSOR_POSITION position, uint8_t *pVersion)
{
	if(position == POSITION_EXHAUST)
	{
		can_handle->transmit_message.id = 0x18FEDA52;
	}
	else
	{
		can_handle->transmit_message.id = 0x18FEDA51;
	}
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	can_handle->transmit_message.data[0] = 0x02; // lambda校正范围0---200  
	can_handle->transmit_message.data[1] = pVersion[0]; // NOx压力校正
	can_handle->transmit_message.data[2] = pVersion[1]; // NO2校正
	can_handle->transmit_message.data[3] = pVersion[2];
	can_handle->transmit_message.data[4] = pVersion[3];
	can_handle->transmit_message.data[5] = 0xDA;
	can_handle->transmit_message.data[6] = 0xFE;
	can_handle->transmit_message.data[7] = 0x00;
	if (can_handle->adr == 0xFF || can_handle->adr == 0x51 || can_handle->adr == 0x52)
	{
		if (can_handle->busoff_flag == 0)
		{
			can_transmit_message(&can_handle->transmit_message);
		}
	}
	else
	{
	
	}
}
// 传感器obd can_handle->transmit_message.data5
// 
void sensor_obd_command_data5(can_handle_type *can_handle, SENSOR_POSITION position)
{
	if(position == POSITION_EXHAUST)
	{
		can_handle->transmit_message.id = 0x1CEC0052;
	}
	else
	{
		can_handle->transmit_message.id = 0x1CEC0051;
	}
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	if(can_handle->adr == 0xFF)
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->adr << 8;
		can_handle->transmit_message.data[0] = 0x20; // 控制字节，请求为16
	}
	else
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->yy << 8;
		can_handle->transmit_message.data[0] = 0x10;
	}
	can_handle->transmit_message.data[1] = 0x1E; // NOx压力校正
	can_handle->transmit_message.data[2] = 0x00; // NO2校正
	can_handle->transmit_message.data[3] = 0x05;
	can_handle->transmit_message.data[4] = 0xFF;
	can_handle->transmit_message.data[5] = 0xC5;
	can_handle->transmit_message.data[6] = 0xFD;
	can_handle->transmit_message.data[7] = 0x00;
	if (can_handle->adr == 0xFF || can_handle->adr == 0x51 || can_handle->adr == 0x52)
	{
		if (can_handle->busoff_flag == 0)
		{
			can_transmit_message(&can_handle->transmit_message);
		}
	}
	else
	{
	
	}
}
// 传感器obd can_handle->transmit_message.data5_1
// 参数：ECU版本号
void sensor_obd_command_data5_1(can_handle_type *can_handle, SENSOR_POSITION position)
{
	if(position == POSITION_EXHAUST)
	{
		can_handle->transmit_message.id = 0x1CEB0052;
	}
	else
	{
		can_handle->transmit_message.id = 0x1CEB0051;
	}
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	if(can_handle->adr == 0xFF)
		can_handle->transmit_message.id |= (uint32_t)can_handle->adr << 8;
	else
		can_handle->transmit_message.id |= (uint32_t)can_handle->yy << 8;
	can_handle->transmit_message.data[0] = 0x01;
	can_handle->transmit_message.data[1] = '0';
	can_handle->transmit_message.data[2] = '0';
	can_handle->transmit_message.data[3] = '0';
	can_handle->transmit_message.data[4] = '0';
	can_handle->transmit_message.data[5] = '0';
	can_handle->transmit_message.data[6] = '0';
	can_handle->transmit_message.data[7] = '0';
	if (can_handle->busoff_flag == 0)
	{
		can_transmit_message(&can_handle->transmit_message);
	}
}
// 传感器obd can_handle->transmit_message.data5_2
// 参数：ECU版本号
void sensor_obd_command_data5_2(can_handle_type *can_handle, SENSOR_POSITION position)
{
	if(position == POSITION_EXHAUST)
	{
		can_handle->transmit_message.id = 0x1CEB0052;
	}
	else
	{
		can_handle->transmit_message.id = 0x1CEB0051;
	}
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	if(can_handle->adr == 0xFF)
		can_handle->transmit_message.id |= (uint32_t)can_handle->adr << 8;
	else
		can_handle->transmit_message.id |= (uint32_t)can_handle->yy << 8;
	can_handle->transmit_message.data[0] = 0x02;
	can_handle->transmit_message.data[1] = '0';
	can_handle->transmit_message.data[2] = '0';
	can_handle->transmit_message.data[3] = '0';
	can_handle->transmit_message.data[4] = 0x20;
	can_handle->transmit_message.data[5] = 0x20;
	can_handle->transmit_message.data[6] = '*';
	can_handle->transmit_message.data[7] = '1';
	if (can_handle->busoff_flag == 0)
	{
		can_transmit_message(&can_handle->transmit_message);
	}
}
// 传感器obd can_handle->transmit_message.data5_3
// 参数：ECU版本号
void sensor_obd_command_data5_3(can_handle_type *can_handle, SENSOR_POSITION position)
{
	if(position == POSITION_EXHAUST)
	{
		can_handle->transmit_message.id = 0x1CEB0052;
	}
	else
	{
		can_handle->transmit_message.id = 0x1CEB0051;
	}
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	if(can_handle->adr == 0xFF)
		can_handle->transmit_message.id |= (uint32_t)can_handle->adr << 8;
	else
		can_handle->transmit_message.id |= (uint32_t)can_handle->yy << 8;
	can_handle->transmit_message.data[0] = 0x03;
	can_handle->transmit_message.data[1] = '9';
	can_handle->transmit_message.data[2] = '1';
	can_handle->transmit_message.data[3] = '2';
	can_handle->transmit_message.data[4] = '0';
	can_handle->transmit_message.data[5] = '8';
	can_handle->transmit_message.data[6] = '0';
	can_handle->transmit_message.data[7] = '0';
	if (can_handle->busoff_flag == 0)
	{
		can_transmit_message(&can_handle->transmit_message);
	}
}
// 传感器obd can_handle->transmit_message.data5_4
//
void sensor_obd_command_data5_4(can_handle_type *can_handle, SENSOR_POSITION position)
{
	if(position == POSITION_EXHAUST)
	{
		can_handle->transmit_message.id = 0x1CEB0052;
	}
	else
	{
		can_handle->transmit_message.id = 0x1CEB0051;
	}
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	if(can_handle->adr == 0xFF)
		can_handle->transmit_message.id |= (uint32_t)can_handle->adr << 8;
	else
		can_handle->transmit_message.id |= (uint32_t)can_handle->yy << 8;
	can_handle->transmit_message.data[0] = 0x04;
	can_handle->transmit_message.data[1] = '0';
	can_handle->transmit_message.data[2] = '1';
	can_handle->transmit_message.data[3] = '*';
	can_handle->transmit_message.data[4] = '*';
	can_handle->transmit_message.data[5] = 0x55;
	can_handle->transmit_message.data[6] = 0x4E;
	can_handle->transmit_message.data[7] = 0x49;
	if (can_handle->busoff_flag == 0)
	{
		can_transmit_message(&can_handle->transmit_message);
	}
}
// 传感器obd can_handle->transmit_message.data5_5
// 
void sensor_obd_command_data5_5(can_handle_type *can_handle, SENSOR_POSITION position)
{
	if(position == POSITION_EXHAUST)
	{
		can_handle->transmit_message.id = 0x1CEB0052;
	}
	else
	{
		can_handle->transmit_message.id = 0x1CEB0051;
	}
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	if(can_handle->adr == 0xFF)
		can_handle->transmit_message.id |= (uint32_t)can_handle->adr << 8;
	else
		can_handle->transmit_message.id |= (uint32_t)can_handle->yy << 8;
	can_handle->transmit_message.data[0] = 0x05;
	can_handle->transmit_message.data[1] = '*';
	can_handle->transmit_message.data[2] = '*';
	can_handle->transmit_message.data[3] = 0xFF;
	can_handle->transmit_message.data[4] = 0xFF;
	can_handle->transmit_message.data[5] = 0xFF;
	can_handle->transmit_message.data[6] = 0xFF;
	can_handle->transmit_message.data[7] = 0xFF;
	if (can_handle->busoff_flag == 0)
	{
		can_transmit_message(&can_handle->transmit_message);
	}
}
// 传感器obd can_handle->transmit_message.data6
// 
void sensor_obd_command_data6(can_handle_type *can_handle, SENSOR_POSITION position, uint8_t hardware_error)
{
	if(position == POSITION_EXHAUST)
	{
		can_handle->transmit_message.id = 0x18EEFF52;
		can_handle->transmit_message.data[0] = 0xA2;
	}
	else
	{
		can_handle->transmit_message.id = 0x18EEFF51;
		can_handle->transmit_message.data[0] = 0xA1;
	}
	if(hardware_error & 0x08)//地址干涉
	{
		can_handle->transmit_message.id = 0x18EEFFFE;
	}
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	can_handle->transmit_message.data[1] = 0x30;
	can_handle->transmit_message.data[2] = 0x44;
	can_handle->transmit_message.data[3] = 0x16;
	can_handle->transmit_message.data[4] = 0x00;
	can_handle->transmit_message.data[5] = 0x44;
	can_handle->transmit_message.data[6] = 0x00;
	can_handle->transmit_message.data[7] = 0x00;
	if (can_handle->adr == 0xFF || can_handle->adr == 0x51 || can_handle->adr == 0x52)
	{
		if((hardware_error & 0x40) == 0)
		{
			can_transmit_message(&can_handle->transmit_message);
		}
	}
	else
	{
	}
}
// 传感器obd can_handle->transmit_message.data7
// 
void sensor_obd_command_data7(can_handle_type *can_handle, SENSOR_POSITION position)
{
	if(position == POSITION_EXHAUST)
	{
		can_handle->transmit_message.id = 0x1CEC0052;
	}
	else
	{
		can_handle->transmit_message.id = 0x1CEC0051;
	}
	if(can_handle->adr == 0xFF)
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->adr << 8;
		can_handle->transmit_message.data[0] = 0x20; // 控制字节，请求为16
	}
	else
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->yy << 8;
		can_handle->transmit_message.data[0] = 0x10;
	}
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	can_handle->transmit_message.data[1] = 0x14;
	can_handle->transmit_message.data[2] = 0x00;
	can_handle->transmit_message.data[3] = 0x03;
	can_handle->transmit_message.data[4] = 0xFF;
	can_handle->transmit_message.data[5] = 0x00;
	can_handle->transmit_message.data[6] = 0xD3;
	can_handle->transmit_message.data[7] = 0x00;
	if (can_handle->adr == 0xFF || can_handle->adr == 0x51 || can_handle->adr == 0x52)
	{
		if (can_handle->busoff_flag == 0)
		{
			can_transmit_message(&can_handle->transmit_message);
		}
	}
	else
	{
	
	}
}
// 传感器obd can_handle->transmit_message.data7_1
// 
void sensor_obd_command_data7_1(can_handle_type *can_handle, SENSOR_POSITION position)
{
	if(position == POSITION_EXHAUST)
	{
		can_handle->transmit_message.id = 0x1CEB0052;
	}
	else
	{
		can_handle->transmit_message.id = 0x1CEB0051;
	}
	if(can_handle->adr == 0xFF)
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->adr << 8;
	}
	else
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->yy << 8;
	}
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	can_handle->transmit_message.data[0] = 0x01;
	can_handle->transmit_message.data[1] = 0x5A;
	can_handle->transmit_message.data[2] = 0xD5;
	can_handle->transmit_message.data[3] = 0xD9;
	can_handle->transmit_message.data[4] = 0x7F;
	can_handle->transmit_message.data[5] = 'N';
	can_handle->transmit_message.data[6] = 'O';
	can_handle->transmit_message.data[7] = 'X';
	if (can_handle->adr == 0xFF || can_handle->adr == 0x51 || can_handle->adr == 0x52)
	{
		if (can_handle->busoff_flag == 0)
		{
			can_transmit_message(&can_handle->transmit_message);
		}
	}
	else
	{
	
	}
}
// 传感器obd can_handle->transmit_message.data7_2
// 
void sensor_obd_command_data7_2(can_handle_type *can_handle, SENSOR_POSITION position)
{
	if(position == POSITION_EXHAUST)
	{
		can_handle->transmit_message.id = 0x1CEB0052;
	}
	else
	{
		can_handle->transmit_message.id = 0x1CEB0051;
	}
	if(can_handle->adr == 0xFF)
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->adr << 8;
	}
	else
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->yy << 8;
	}
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	can_handle->transmit_message.data[0] = 0x02;
	can_handle->transmit_message.data[1] = '-';
	can_handle->transmit_message.data[2] = 'N';
	can_handle->transmit_message.data[3] = '_';
	can_handle->transmit_message.data[4] = '1';
	can_handle->transmit_message.data[5] = '5';
	can_handle->transmit_message.data[6] = '3';
	can_handle->transmit_message.data[7] = '1';
	if (can_handle->busoff_flag == 0)
	{
		can_transmit_message(&can_handle->transmit_message);
	}
}
// 传感器obd can_handle->transmit_message.data7_3
// 
void sensor_obd_command_data7_3(can_handle_type *can_handle, SENSOR_POSITION position)
{
	if(position == POSITION_EXHAUST)
	{
		can_handle->transmit_message.id = 0x1CEB0052;
		can_handle->transmit_message.data[4] = 'O';
	}
	else
	{
		can_handle->transmit_message.id = 0x1CEB0051;
		can_handle->transmit_message.data[4] = 'I';
	}
	if(can_handle->adr == 0xFF)
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->adr << 8;
	}
	else
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->yy << 8;
	}
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	can_handle->transmit_message.data[0] = 0x03;
	can_handle->transmit_message.data[1] = ' ';
	can_handle->transmit_message.data[2] = 'A';
	can_handle->transmit_message.data[3] = 'T';
	can_handle->transmit_message.data[5] = '1';
	can_handle->transmit_message.data[6] = 0x00;
	can_handle->transmit_message.data[7] = 0xFF;
	if (can_handle->busoff_flag == 0)
	{
		can_transmit_message(&can_handle->transmit_message);
	}
}
// 传感器obd can_handle->transmit_message.data10
// 
void sensor_obd_command_data10(can_handle_type *can_handle, SENSOR_POSITION position)
{
	if(position == POSITION_EXHAUST)
	{
		can_handle->transmit_message.id = 0x1CEC0052;
	}
	else
	{
		can_handle->transmit_message.id = 0x1CEC0051;
	}
	if(can_handle->adr == 0xFF)
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->adr << 8;
		can_handle->transmit_message.data[0] = 0x20; // 控制字节，请求为16
	}
	else
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->yy << 8;
		can_handle->transmit_message.data[0] = 0x10; // 控制字节，请求为16
	}
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	can_handle->transmit_message.data[1] = 0x1C;
	can_handle->transmit_message.data[2] = 0x00;
	can_handle->transmit_message.data[3] = 0x04;
	can_handle->transmit_message.data[4] = 0xFF;
	can_handle->transmit_message.data[5] = 0xFF;
	can_handle->transmit_message.data[6] = 0xEE;
	can_handle->transmit_message.data[7] = 0x59;
	if (can_handle->busoff_flag == 0)
	{
		can_transmit_message(&can_handle->transmit_message);
	}
}
// 传感器obd can_handle->transmit_message.data10_1
// 
void sensor_obd_command_data10_1(can_handle_type *can_handle, SENSOR_POSITION position, uint16_t heaterResValue, uint16_t vp0, uint16_t heaterRatio)
{
	if(position == POSITION_EXHAUST)
	{
		can_handle->transmit_message.id = 0x1CEB0052;
	}
	else
	{
		can_handle->transmit_message.id = 0x1CEB0051;
	}
	if(can_handle->adr == 0xFF)
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->adr << 8;
	}
	else
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->yy << 8;
	}
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	can_handle->transmit_message.data[0] = 0x01;
	can_handle->transmit_message.data[1] = heaterResValue & 0xFF;
	can_handle->transmit_message.data[2] = heaterResValue >> 8;
	can_handle->transmit_message.data[3] = vp0 & 0xFF;
	can_handle->transmit_message.data[4] = vp0 >> 8;
	can_handle->transmit_message.data[5] = heaterRatio & 0xFF;
	can_handle->transmit_message.data[6] = heaterRatio >> 8;
	can_handle->transmit_message.data[7] = '*';
	if (can_handle->busoff_flag == 0)
	{
		can_transmit_message(&can_handle->transmit_message);
	}
}
// 传感器obd can_handle->transmit_message.data10_1
// 
void sensor_obd_command_data10_2(can_handle_type *can_handle, SENSOR_POSITION position, uint16_t ip0, uint16_t ip1, uint16_t ip2)
{
	if(position == POSITION_EXHAUST)
	{
		can_handle->transmit_message.id = 0x1CEB0052;
	}
	else
	{
		can_handle->transmit_message.id = 0x1CEB0051;
	}
	if(can_handle->adr == 0xFF)
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->adr << 8;
	}
	else
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->yy << 8;
	}
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	can_handle->transmit_message.data[0] = 0x02;
	can_handle->transmit_message.data[1] = ip0 & 0xFF;
	can_handle->transmit_message.data[2] = ip0 >> 8;
	can_handle->transmit_message.data[3] = ip1 & 0xFF;
	can_handle->transmit_message.data[4] = ip1 >> 8;
	can_handle->transmit_message.data[5] = ip2 & 0xFF;
	can_handle->transmit_message.data[6] = ip2 >> 8;
	can_handle->transmit_message.data[7] = '*';
	if (can_handle->busoff_flag == 0)
	{
		can_transmit_message(&can_handle->transmit_message);
	}
}
void sensor_obd_command_data10_3(can_handle_type *can_handle, SENSOR_POSITION position, uint16_t Vbin, uint16_t HeaterPwm, uint16_t Vref)
{
	if(position == POSITION_EXHAUST)
	{
		can_handle->transmit_message.id = 0x1CEB0052;
	}
	else
	{
		can_handle->transmit_message.id = 0x1CEB0051;
	}
	if(can_handle->adr == 0xFF)
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->adr << 8;
	}
	else
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->yy << 8;
	}
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	can_handle->transmit_message.data[0] = 0x03;
	can_handle->transmit_message.data[1] = Vbin & 0xFF;
	can_handle->transmit_message.data[2] = Vbin >> 8;
	can_handle->transmit_message.data[3] = HeaterPwm & 0xFF;
	can_handle->transmit_message.data[4] = HeaterPwm >> 8;
	can_handle->transmit_message.data[5] = Vref & 0xFF;
	can_handle->transmit_message.data[6] = Vref >> 8;
	can_handle->transmit_message.data[7] = '*';
	if (can_handle->busoff_flag == 0)
	{
		can_transmit_message(&can_handle->transmit_message);
	}
}
void sensor_obd_command_data10_4(can_handle_type *can_handle, SENSOR_POSITION position, uint8_t heaterPhase, uint16_t voltageValue, uint16_t ip1CtrValue)
{
	if(position == POSITION_EXHAUST)
	{
		can_handle->transmit_message.id = 0x1CEB0052;
	}
	else
	{
		can_handle->transmit_message.id = 0x1CEB0051;
	}
	if(can_handle->adr == 0xFF)
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->adr << 8;
	}
	else
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->yy << 8;
	}
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	can_handle->transmit_message.data[0] = 0x04;
	can_handle->transmit_message.data[1] = heaterPhase;
	can_handle->transmit_message.data[2] = voltageValue & 0xFF;
	can_handle->transmit_message.data[3] = voltageValue >> 8;
	can_handle->transmit_message.data[4] = ip1CtrValue & 0xFF;
	can_handle->transmit_message.data[5] = ip1CtrValue >> 8;
	can_handle->transmit_message.data[6] = '*';
	can_handle->transmit_message.data[7] = '*';
	if (can_handle->busoff_flag == 0)
	{
		can_transmit_message(&can_handle->transmit_message);
	}
}
void sensor_obd_command_data10_5(can_handle_type *can_handle, SENSOR_POSITION position)
{
	if(position == POSITION_EXHAUST)
	{
		can_handle->transmit_message.id = 0x1CEB0052;
	}
	else
	{
		can_handle->transmit_message.id = 0x1CEB0051;
	}
	if(can_handle->adr == 0xFF)
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->adr << 8;
	}
	else
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->yy << 8;
	}
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	can_handle->transmit_message.data[0] = 0x05;
	can_handle->transmit_message.data[1] = 0x00;
	can_handle->transmit_message.data[2] = 0x00;
	can_handle->transmit_message.data[3] = 0x00;
	can_handle->transmit_message.data[4] = 0x00;
	can_handle->transmit_message.data[5] = 0x00;
	can_handle->transmit_message.data[6] = 0x00;
	can_handle->transmit_message.data[7] = 0x00;
	if (can_handle->busoff_flag == 0)
	{
		can_transmit_message(&can_handle->transmit_message);
	}
}
// 传感器obd shutoff
// 
void Sensor_OBD_Command_shutoff(can_handle_type *can_handle, SENSOR_POSITION position)
{
	if(position == POSITION_EXHAUST)
	{
		can_handle->transmit_message.id = 0x1CEC0052;
	}
	else
	{
		can_handle->transmit_message.id = 0x1CEC0051;
	}
	if(can_handle->adr == 0xFF)
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->adr << 8;
	}
	else
	{
		can_handle->transmit_message.id |= (uint32_t)can_handle->yy << 8;
	}
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	can_handle->transmit_message.data[0] = 0xFF;
	can_handle->transmit_message.data[1] = 0x03;
	can_handle->transmit_message.data[2] = 0xFF;
	can_handle->transmit_message.data[3] = 0xFF;
	can_handle->transmit_message.data[4] = 0xFF;
	can_handle->transmit_message.data[5] = can_handle->pgnlsb;
	can_handle->transmit_message.data[6] = can_handle->pgn;
	can_handle->transmit_message.data[7] = can_handle->pgnmsb;
	if (can_handle->busoff_flag == 0)
	{
		can_transmit_message(&can_handle->transmit_message);
	}
}
