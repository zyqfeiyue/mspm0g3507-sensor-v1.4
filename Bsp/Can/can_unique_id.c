#include "can_unique_id.h"
#include "soft_timer.h"
//---------------------------------------------------------------------------
// Global variables
//---------------------------------------------------------------------------
UNIQUE_ID Uniqueid;
uint8_t UniqueID_CANHardFilterEnable = 1;

///*******************************************************************************
//* 函数名称: UniqueID_CAN_HardFilterCfg

//* 函数功能: 配置硬件滤波器

//* 输入参数: 无

//* 返 回 值: 无

//* 备     注: 注意先关闭所有，再重新配置方可生效
//            目前仅配置了一个滤波器(只允许接收2个ID)
//*******************************************************************************/
//void UniqueID_CAN_HardFilterCfg(void)
//{
//  if(UniqueID_CANHardFilterEnable)
//	{
//		//只配置一次
//		UniqueID_CANHardFilterEnable = 0;
//		can_filter_init(1, 0x18FEDF00, 0x18EA5155, 0x18EA5255);// 配置并使能新滤波器
//	}
//  else
//	{
//		//do nothing
//	}
//}

///*******************************************************************************
//* 函数名称: UniqueID_ReadCalibration_ReadSN

//* 函数功能: 在传感器读标定信息时，获取传感器序列号，并存入全局变量以备调用

//* 输入参数: 无

//* 返 回 值: 无

//* 备     注:
//            a.在错误状态时，传感器传出的ID号罗列如下
//              //从未写入 S00101000000           20 03 5E 10 2F
//              //写入失败 S00101000001           20 23 5E 10 2F
//              //读取失败 S00101000002           20 43 5E 10 2F
//            b.ID存放地址已加入在存储序列中 可查找 EEPROM_ADDRESS_SERIAL_NUMBER
//*******************************************************************************/
////#define EEPROM_ADDRESS_SERIAL_NUMBERxx 144
//void UniqueID_ReadCalibration_ReadSN(void)
//{
//    //u16 ENUM_DATA1 = 0;
////获取传感器序列号
//  eeprom_read(EEPROM_ADDRESS_SERIAL_NUMBER,Uniqueid.SNTemp, 10);

// //ENUM_DATA1 = EEPROM_ADDRESS_SERIAL_NUMBER;
//  //if(ENUM_DATA1!=144)
//    //{
//      //while(1);
//    //}
////校验 ID + ~ID
//  static uint16_t *ptr = 0;
//  ptr = &Uniqueid.SNTemp[0];
//  uint8_t CheckErrFlag  = 0;
//  for(uint8_t i = 0; i < 5; i++)
//	{
//		if((*(ptr+2*i) + *(ptr+2*i+1)) != 0xFFFF)//校验失败
//		{
//			CheckErrFlag = 1;
//			break;
//		}
//	}
//  //校验失败
//  if(CheckErrFlag)
//	{
//		for(uint8_t j = 0; j < 10; j++)
//		{
//			//已写入，但读取失败
//			if(Uniqueid.SNTemp[j]!=0)//在该步骤，读出不对就认为是EEPROM读取存在问题，因为在生产工序中已对是否正确写入进行验证
//			{
//				Uniqueid.SNInfo.ReceiveData[0] = 0x20;
//				Uniqueid.SNInfo.ReceiveData[1] = 0x43;
//				Uniqueid.SNInfo.ReceiveData[2] = 0x5E;
//				Uniqueid.SNInfo.ReceiveData[3] = 0x10;
//				Uniqueid.SNInfo.ReceiveData[4] = 0x2F;
//				break;
//			}
//			//未写入
//			else
//			{
//				if(j>=9)//能进入此处说明全是0，此时认为序列号 从未写入
//				{
//					Uniqueid.SNInfo.ReceiveData[0] = 0x20;
//					Uniqueid.SNInfo.ReceiveData[1] = 0x03;
//					Uniqueid.SNInfo.ReceiveData[2] = 0x5E;
//					Uniqueid.SNInfo.ReceiveData[3] = 0x10;
//					Uniqueid.SNInfo.ReceiveData[4] = 0x2F;
//				}
//			}
//		}
//	}
//  //校验成功
//  else
//	{
//		Uniqueid.SNInfo.ReceiveData[0] = Uniqueid.SNTemp[0];
//		Uniqueid.SNInfo.ReceiveData[1] = Uniqueid.SNTemp[2];
//		Uniqueid.SNInfo.ReceiveData[2] = Uniqueid.SNTemp[4];
//		Uniqueid.SNInfo.ReceiveData[3] = Uniqueid.SNTemp[6];
//		Uniqueid.SNInfo.ReceiveData[4] = Uniqueid.SNTemp[8];
//	}

//    Uniqueid.CanId      = (uint32_t)Uniqueid.SNInfo.CanIdData01.ID_b28_b24    << 24 |\
//                          (uint32_t)Uniqueid.SNInfo.CanIdData01.ID_b23_b21    << 21 |\
//                          (uint32_t)Uniqueid.SNInfo.CanIdData01.ID_b20_b16    << 16 |\
//                          (uint32_t)Uniqueid.SNInfo.CanIdData01.ID_b15_b13    << 13 |\
//                          (uint32_t)Uniqueid.SNInfo.CanIdData01.ID_b12_b08    <<  8 |\
//                          (uint32_t)Uniqueid.SNInfo.CanIdData01.ID_b07_b05    <<  5 |\
//                          (uint32_t)Uniqueid.SNInfo.CanIdData01.ID_b04_b00    <<  0 ;
//    Uniqueid.CanData0   = (uint8_t )Uniqueid.SNInfo.CanIdData01.Data0_b07_b05 <<  5  |\
//                          (uint8_t )Uniqueid.SNInfo.CanIdData01.Data0_b04_b00 <<  0 ;
//    Uniqueid.CanData1H3 = (uint8_t )Uniqueid.SNInfo.CanIdData01.Data1_b07_b05 <<  5 ;
//}

///*******************************************************************************
//* 函数名称: UniqueID_CANRecInterrupt_WriteSN

//* 函数功能: 在CAN接收中断中，接收上位机下发的序列号信息并写入EEPROM

//* 输入参数: 无

//* 返 回 值: 无

//* 备     注: 写入失败，需反馈
//*******************************************************************************/
//void UniqueID_CANRecInterrupt_WriteSN(can_receive_message_type *receive_data, sensor_type *sensor)
//{
////写入EEPROM：接收到的byte3:byte7为传感器40bit的序列号
//  Uniqueid.SNTemp[0] = receive_data->data[3];
//  Uniqueid.SNTemp[2] = receive_data->data[4];
//  Uniqueid.SNTemp[4] = receive_data->data[5];
//  Uniqueid.SNTemp[6] = receive_data->data[6];
//  Uniqueid.SNTemp[8] = receive_data->data[7];
//  Uniqueid.SNTemp[1] = ~Uniqueid.SNTemp[0];
//  Uniqueid.SNTemp[3] = ~Uniqueid.SNTemp[2];
//  Uniqueid.SNTemp[5] = ~Uniqueid.SNTemp[4];
//  Uniqueid.SNTemp[7] = ~Uniqueid.SNTemp[6];
//  Uniqueid.SNTemp[9] = ~Uniqueid.SNTemp[8];
//	sensor->write_eeprom_flag = 0x45;
//	eeprom_write_verify(EEPROM_ADDRESS_SERIAL_NUMBER, Uniqueid.SNTemp, 10, sensor);
////static u8 errtest = 0xFA;
////Sensor.WriteEEPFlag = 0x45;
////EepromWrite( EEPROM_ADDRESS_SERIAL_NUMBER,&errtest , 1 );
//  //从EEPROM中读取：数据作为传感器外发ID，以验证是否写入正确
//  for(uint8_t i = 0; i < 10; i++)
//	{
//		Uniqueid.SNTemp[i] = 0;
//	}
//  eeprom_read(EEPROM_ADDRESS_SERIAL_NUMBER,Uniqueid.SNTemp, 10);
//  uint16_t *ptr = &Uniqueid.SNTemp[0];
//  uint8_t CheckErrFlag = 0;
//  for(uint8_t i = 0; i < 5; i++)
//	{
//		if((*(ptr+2*i) + *(ptr+2*i+1)) != 0xFFFF)//校验失败
//		{
//			CheckErrFlag = 1;
//			break;
//		}
//	}
//  if(CheckErrFlag)
//	{//在该工序，只认读出结果，读出不对就认为是写的不对，而不是硬件读的有问题
//		Uniqueid.SNInfo.ReceiveData[0] = 0x20;
//		Uniqueid.SNInfo.ReceiveData[1] = 0x23;
//		Uniqueid.SNInfo.ReceiveData[2] = 0x5E;
//		Uniqueid.SNInfo.ReceiveData[3] = 0x10;
//		Uniqueid.SNInfo.ReceiveData[4] = 0x2F;
//	}
//  else
//	{
//		Uniqueid.SNInfo.ReceiveData[0] = Uniqueid.SNTemp[0];
//		Uniqueid.SNInfo.ReceiveData[1] = Uniqueid.SNTemp[2];
//		Uniqueid.SNInfo.ReceiveData[2] = Uniqueid.SNTemp[4];
//		Uniqueid.SNInfo.ReceiveData[3] = Uniqueid.SNTemp[6];
//		Uniqueid.SNInfo.ReceiveData[4] = Uniqueid.SNTemp[8];
//	}
//	Uniqueid.CanId      = (uint32_t)Uniqueid.SNInfo.CanIdData01.ID_b28_b24    << 24 |\
//												(uint32_t)Uniqueid.SNInfo.CanIdData01.ID_b23_b21    << 21 |\
//												(uint32_t)Uniqueid.SNInfo.CanIdData01.ID_b20_b16    << 16 |\
//												(uint32_t)Uniqueid.SNInfo.CanIdData01.ID_b15_b13    << 13 |\
//												(uint32_t)Uniqueid.SNInfo.CanIdData01.ID_b12_b08    <<  8 |\
//												(uint32_t)Uniqueid.SNInfo.CanIdData01.ID_b07_b05    <<  5 |\
//												(uint32_t)Uniqueid.SNInfo.CanIdData01.ID_b04_b00    <<  0 ;
//	Uniqueid.CanData0   = (uint8_t )Uniqueid.SNInfo.CanIdData01.Data0_b07_b05 <<  5  |\
//												(uint8_t )Uniqueid.SNInfo.CanIdData01.Data0_b04_b00 <<  0 ;
//	Uniqueid.CanData1H3 = (uint8_t )Uniqueid.SNInfo.CanIdData01.Data1_b07_b05 <<  5 ;
//}
///*******************************************************************************
//* 函数名称: UniqueID_CAN_OutPut

//* 函数功能: 以传感器序列号为ID，按照预设帧序号进行输出

//* 输入参数: u8 index		帧序列
//            u16 arg1  可传变量1
//            u16 arg2  可传变量2
//            u16 arg3  可传变量3

//* 返 回 值: 无

//* 备     注:
//*******************************************************************************/
//static void UniqueID_CAN_OutPut(can_handle_type *can_handle, uint8_t index, uint16_t arg1, uint16_t arg2, uint16_t arg3)
//{
//	can_handle->transmit_message.id = Uniqueid.CanId;
//	can_handle->transmit_message.ide = CAN_ID_EXTEND;
//	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
//	can_handle->transmit_message.dlc = 8;
//	can_handle->transmit_message.data[0] = Uniqueid.CanData0;
//	can_handle->transmit_message.data[1] = Uniqueid.CanData1H3|(index&0x1F);
//	can_handle->transmit_message.data[2] = arg1 & 0xFF;
//	can_handle->transmit_message.data[3] = arg1 >> 8;
//	can_handle->transmit_message.data[4] = arg2 & 0xFF;
//	can_handle->transmit_message.data[5] = arg2 >> 8;
//	can_handle->transmit_message.data[6] = arg3 & 0xFF;
//	can_handle->transmit_message.data[7] = arg3 >> 8;
//	can_transmit_message(&can_handle->transmit_message);
//}

///*******************************************************************************
//* 函数名称: UniqueID_CAN_Comm_DP

//* 函数功能: 在生产工序(激活)中，序列传输传感器内部信息

//* 输入参数: 无

//* 返 回 值: 无

//* 备     注:
//                                    传输列表
//                     序号        变量1         变量2          变量3
//                      0        vPower    vTemprature     vp0

//                      1      heaterRatio ip1ctrvalue     Ip0

//                      2         Ip1         Ip2          heaterPhase/10

//                      3        vipPwm     heaterPwm      Vref

//                      4        vipPwm1     0xFFFF        0xFFFF
//*******************************************************************************/
//void UniqueID_CAN_Comm_DP(can_handle_type *can_handle, sensor_type *sensor)//DP=During Product
//{
//  switch(Uniqueid.CanOutIndex)
//    {
//      case 0: 
////				UniqueID_CAN_OutPut(0,(uint16_t)Sensor.vPower,     (uint16_t)Sensor.vTemprature, (uint16_t)Sensor.vp0);
//				Uniqueid.CanOutIndex++;
//				break;
//      case 1: 
//				UniqueID_CAN_OutPut(can_handle, 1,(uint16_t)sensor->calibration_25c_heater_real_res_u16_1000,(uint16_t)sensor->ip1_control_value,(uint16_t)sensor->ip0);
//        Uniqueid.CanOutIndex++;
//        break;
//      case 2: 
//				UniqueID_CAN_OutPut(can_handle, 2,(uint16_t)sensor->ip1,        (uint16_t)sensor->ip2, 0);
//				Uniqueid.CanOutIndex++;
//				break;
//      case 3: 
//				UniqueID_CAN_OutPut(can_handle, 3,(uint16_t)0,     (uint16_t)0,  (uint16_t)0);
//				Uniqueid.CanOutIndex++;
//				break;
//      case 4: 
//				UniqueID_CAN_OutPut(can_handle, 4,(uint16_t)0,    (uint16_t)0,  (uint16_t)0);
//				Uniqueid.CanOutIndex = 0;
//				break;
//      default://UniqueID_CAN_OutPut(31,(u16)Sensor.Ip1,(u16)Sensor.Ip2,(u16)(Sensor.heaterPhase/10));
//				break;
//    }
//}

//void uniqueid_can_transmit_message(can_handle_type *can_handle, sensor_type *sensor)
//{
//	static uint32_t now_time = 0;
//	static uint32_t active_time = 0;
//	now_time = soft_timer_tick_counter_get();
//	if (now_time - active_time > 1500)
//	{
//		active_time = soft_timer_tick_counter_get();
//		UniqueID_CAN_Comm_DP(can_handle, sensor);
//	}
//}
