/*
 * can_command.h
 *
 *  Created on: 2021-11-15
 *      Author: zyq
 */
#ifndef CAN_command_H_
#define CAN_command_H_
#include "main.h"
#include "can_driver.h"
#include "can_control.h"
#include "system_state.h"
/**
  * @brief   CAN identifier type */

void sensor_hardware_error(can_handle_type *can_handle, SENSOR_POSITION position);
void sensor_obd_command_ack(can_handle_type *can_handle, SENSOR_POSITION position, uint8_t ack);
void sensor_obd_command_busy(can_handle_type *can_handle, SENSOR_POSITION position);
void sensor_obd_command_data1(can_handle_type *can_handle, SENSOR_POSITION position, uint8_t hardware_error);
void sensor_obd_command_data2(can_handle_type *can_handle, SENSOR_POSITION position, uint8_t hardware_error);
void sensor_obd_command_data3(can_handle_type *can_handle, SENSOR_POSITION position, uint8_t hardware_error);
void sensor_obd_command_data4(can_handle_type *can_handle, SENSOR_POSITION position, uint8_t *pVersion);
void sensor_obd_command_data5(can_handle_type *can_handle, SENSOR_POSITION position);
void sensor_obd_command_data5_1(can_handle_type *can_handle, SENSOR_POSITION position);
void sensor_obd_command_data5_2(can_handle_type *can_handle, SENSOR_POSITION position);
void sensor_obd_command_data5_3(can_handle_type *can_handle, SENSOR_POSITION position);
void sensor_obd_command_data5_4(can_handle_type *can_handle, SENSOR_POSITION position);
void sensor_obd_command_data5_5(can_handle_type *can_handle, SENSOR_POSITION position);
void sensor_obd_command_data6(can_handle_type *can_handle, SENSOR_POSITION position, uint8_t hardware_error);
void sensor_obd_command_data7(can_handle_type *can_handle, SENSOR_POSITION position);
void sensor_obd_command_data7_1(can_handle_type *can_handle, SENSOR_POSITION position);
void sensor_obd_command_data7_2(can_handle_type *can_handle, SENSOR_POSITION position);
void sensor_obd_command_data7_3(can_handle_type *can_handle, SENSOR_POSITION position);
void sensor_obd_command_data10(can_handle_type *can_handle, SENSOR_POSITION position);
void sensor_obd_command_data10_1(can_handle_type *can_handle, SENSOR_POSITION position, uint16_t heaterResValue, uint16_t vp0, uint16_t heaterRatio);
void sensor_obd_command_data10_2(can_handle_type *can_handle, SENSOR_POSITION position, uint16_t ip0, uint16_t ip1, uint16_t ip2);
void sensor_obd_command_data10_3(can_handle_type *can_handle, SENSOR_POSITION position, uint16_t Vbin, uint16_t HeaterPwm, uint16_t Vref);
void sensor_obd_command_data10_4(can_handle_type *can_handle, SENSOR_POSITION position, uint8_t heaterPhase, uint16_t voltageValue, uint16_t ip1CtrValue);
void sensor_obd_command_data10_5(can_handle_type *can_handle, SENSOR_POSITION position);
void sensor_obd_command_shutoff(can_handle_type *can_handle, SENSOR_POSITION position);
#endif
