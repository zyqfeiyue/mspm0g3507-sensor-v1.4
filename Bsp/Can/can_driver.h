/*
 * can_driver.h
 *
 *  Created on: 2021-11-16
 *      Author: zyq
 */

#ifndef CAN_DRIVER_H_
#define CAN_DRIVER_H_
#include "main.h"
#include "sensor_obd.h"
#define CFG_FAST_RECOVERY_CNT    5   //快恢复5次
#define CFG_FAST_RECOVERY_TIME  100	//快恢复频率100ms
#define CFG_SLOW_RECOVERY           1000 //慢恢复频率1000ms

//enum CAN_MESSAGE_CMD
typedef enum
{
  CAN_MESSAGE_CMD_NULL,
	CAN_MESSAGE_CMD_DATA1,
  CAN_MESSAGE_CMD_DATA2,//64bit
  CAN_MESSAGE_CMD_DATA3,//64bit
  CAN_MESSAGE_CMD_DATA4,//96bit
  CAN_MESSAGE_CMD_DATA4_1,
  CAN_MESSAGE_CMD_DATA4_2,
  CAN_MESSAGE_CMD_DATA5,//240bit
  CAN_MESSAGE_CMD_DATA5_1,
  CAN_MESSAGE_CMD_DATA5_2,
  CAN_MESSAGE_CMD_DATA5_3,
  CAN_MESSAGE_CMD_DATA5_4,
  CAN_MESSAGE_CMD_DATA5_5,//
  CAN_MESSAGE_CMD_DATA6,//64bit
  CAN_MESSAGE_CMD_DATA7,//160bit
  CAN_MESSAGE_CMD_DATA7_1,
  CAN_MESSAGE_CMD_DATA7_2,
  CAN_MESSAGE_CMD_DATA7_3,
  CAN_MESSAGE_CMD_DATA10,//  10 是F版新增加的
  CAN_MESSAGE_CMD_DATA10_1,
  CAN_MESSAGE_CMD_DATA10_2,
  CAN_MESSAGE_CMD_DATA10_3,
  CAN_MESSAGE_CMD_DATA10_4,
  CAN_MESSAGE_CMD_DATA10_5,
  CAN_MESSAGE_CMD_ACK,
  CAN_MESSAGE_CMD_NACK,
  CAN_MESSAGE_CMD_BUSY,
} can_message_command_type;

typedef enum
{
	CAN_CTS_UNCONNECT,  //无通讯	，无连接
	CAN_CTS_CONNECTING, //有通讯，连接建立，等后续CTS指令,如等不到2.5s后取消连接，用于data5和data7
	CAN_CTS_TRANSMIT,   //接收到CTS后开始发送后续字节，也可用于指示通讯状态忙，用于data5和data7
	CAN_CTS_SHUTOFF,    //用于对取消连接的回应
	CAN_TRANSMIT,
}cts_connect_status_type;

typedef enum
{
  CAN_ID_STANDARD = 0,  /*!< Standard Id */
  CAN_ID_EXTEND = 1  /*!< Extended Id */
}can_id_type;
typedef enum
{
	CAN_RTR_DATA_MODE = 0,
	CAN_RTR_REMOTE_MODE = 1
}can_remote_transmission_type;

/*stm32属于小端模式，
stm8属于大端模式  与STC宏晶一样
四字节联合*/
struct U32_H24_BITS {
	uint32_t   U8_Flags:8;			  //保留的 U8_Flags
	uint32_t   U24_Flags:24; 		  //主要给
};
struct U32_L24_BITS {
	uint32_t   U24_Flags:24;           //主要给PGN
	uint32_t   U8_Flags:8;             //保留的
};
struct U32_8_BITS {
	uint32_t   dxxx8_Flags:8;             //LL
	uint32_t   dxx8x_Flags:8;             //LH
	uint32_t   dx8xx_Flags:8;             //HL
	uint32_t   d8xxx_Flags:8;             //HH
};
struct U16_8_BITS {
	uint16_t   dx8_Flags:8;             //LL
	uint16_t   d8x_Flags:8;             //LH
};
typedef union
{
	struct U32_H24_BITS    U32_H24_BS;    //uint32_t_BitSection
	struct U32_L24_BITS    U32_L24_BS;    //uint32_t_BitSection
	struct U32_8_BITS     U32_8_BS;     //
	struct U16_8_BITS     U16_8_BS[2];  //
	uint32_t    U32;
	int32_t    I32;
	float  F32;
	uint16_t    U16[ 2 ];
	int16_t    I16[ 2 ];
	uint8_t     U8[ 4 ];
	int8_t     S8[ 4 ];
} four_byte_union_type;


/*双字节联合*/
typedef union
{
	struct U16_8_BITS	  U16_8_BS[2];//
	uint16_t  uint16_t;
	int16_t  I16;
	uint8_t   U8[ 2 ];
	int8_t   I8[ 2 ];
}two_byte_union_type;

/*8字节数据联合*/
typedef union
{
	struct U32_H24_BITS    U32_H24_BS[2];    //uint32_t_BitSection
	struct U32_L24_BITS    U32_L24_BS[2];    //uint32_t_BitSection
	uint8_t     Data[ 8 ];
} eight_byte_union_type;

typedef struct
{
//	uint32_t Id;
	four_byte_union_type id;   //可以拆分的
	uint8_t ide;//标准帧还是扩展帧
	uint8_t rtr;//数据帧还是远程帧
	uint8_t dlc;//数据长度0~0x08
	uint8_t data[8];//数据
	//EightByteUnioType  D8;  //8 data bytes
	uint8_t status_flag;//接收标志
	uint8_t fmi;
} can_receive_message_type;

typedef struct
{
	uint32_t id;
	uint32_t ide;//标准帧还是扩展帧
	uint8_t rtr;//数据帧还是远程帧
	uint8_t dlc;//数据长度0~0x08
	uint8_t data[8];//数据
	uint8_t status_flag;//发送标志
	uint8_t fmi;
} can_transmit_message_type;

typedef union
{
	uint32_t value;
	struct
	{
		uint32_t extend_id : 29;
		uint32_t res : 1;
		uint32_t ide : 1;
		uint32_t rtr : 1;
	} bits;
} filter_config_type;
typedef union
{
	__IO uint32_t value;
	struct
	{
		__IO uint32_t data1_cyclic_flag							: 1; /* [0] */
		__IO uint32_t data2_cyclic_flag							: 1; /* [1] */
		__IO uint32_t data2_on_request_flag					: 1; /* [2] */
		__IO uint32_t data3_on_request_flag					: 1; /* [3] */
		__IO uint32_t data4_on_request_flag					: 1; /* [4] */
		__IO uint32_t data5_on_request_flag					: 1; /* [5] */
		__IO uint32_t data5_cts_on_request_1_flag		: 1; /* [6] */
		__IO uint32_t data5_cts_on_request_2_flag		: 1; /* [7] */
		__IO uint32_t data5_cts_on_request_3_flag		: 1; /* [8] */
		__IO uint32_t data5_cts_on_request_4_flag		: 1; /* [9] */
		__IO uint32_t data5_cts_on_request_5_flag		: 1; /* [10] */
		__IO uint32_t data6_on_request_flag					: 1; /* [11] */
		__IO uint32_t data7_on_request_flag					: 1; /* [12] */
		__IO uint32_t data7_cts_on_request_1_flag		: 1; /* [13] */
		__IO uint32_t data7_cts_on_request_2_flag		: 1; /* [14] */
		__IO uint32_t data7_cts_on_request_3_flag		: 1; /* [15] */
		__IO uint32_t data10_on_request_flag				: 1; /* [16] */
		__IO uint32_t data10_cts_on_request_1_flag	: 1; /* [17] */
		__IO uint32_t data10_cts_on_request_2_flag	: 1; /* [18] */
		__IO uint32_t data10_cts_on_request_3_flag	: 1; /* [19] */
		__IO uint32_t data10_cts_on_request_4_flag	: 1; /* [19] */
		__IO uint32_t data10_cts_on_request_5_flag	: 1; /* [19] */
		__IO uint32_t data_ack											: 1; /* [20] */
		__IO uint32_t data_null											: 1; /* [21] */
		__IO uint32_t data_busy											: 1; /* [22] */
		__IO uint32_t other													: 9; /* [23-31] */
	} bit;
} sensor_transmit_data_flag_type;



#define RX_RECEIVE_NUM 5     //3组 深度的接收队列
typedef struct
{
	uint8_t busoff_flag;
	uint8_t receive_data_ready;
	uint8_t transmit_data_flag;
	
	can_receive_message_type receive_message_link_table[RX_RECEIVE_NUM];  // 接收数据链表
	uint8_t receive_message_index;          // 接收数据链表索引号
	uint8_t receive_message_counter;        // 接收标志，为0表示处理完成
	can_transmit_message_type transmit_message;           // 发送邮箱
	
	uint8_t adr; // 传感器数据
	uint8_t yy;
	uint8_t pgnlsb;
	uint8_t pgn;
	uint8_t pgnmsb;
	cts_connect_status_type cts_connect;  // CAN通信传送状态指标
	can_message_command_type message_command_num; // OBD命令的编号，参考BOSCH文档的DATA3~DATA7
	sensor_transmit_data_flag_type sensor_transmit_data_flag_struct; // data数据标志位
	data1_signal_values_type data1_signal_values_struct;
	data2_heater_ratio_deviation_type data2_heater_ratio_deviation_struct;
	data3_correction_factors_type data3_correction_factors_struct;
	uint16_t transmit_counter;
	uint8_t fast_recovery_counter;
	uint8_t can_debug_count;        // can调试计数器
	uint8_t data_message_timebase_compare;     // Can 在OBD时上传时基OBDtimer
	uint16_t can_timeout_count;          // Can超时计数器————DL3.5超时
	uint16_t recovery_time;            // CAN busoff事件后恢复时间
	
	// 特定mcu
	volatile uint32_t gInterruptLine1Status;
	volatile bool gServiceInt;
} can_handle_type;
void can_filter_init(uint8_t filter_flag, uint32_t filter_id_0, uint32_t filter_id_1, uint32_t filter_id_2);
void can_transmit_message(can_transmit_message_type *transmit_message);
void can_receive_message(void);
void can_error_handle(void);
#endif
