#include "can_driver.h"
#include "can_control.h"
#include "string.h"

DL_MCAN_RxBufElement rxMsg;
DL_MCAN_RxFIFOStatus rxFS;
DL_MCAN_TxBufElement txMsg;

void can_filter_init(uint8_t filter_flag, uint32_t filter_id_0, uint32_t filter_id_1, uint32_t filter_id_2)
{
	filter_config_type filter_config_struct;
	filter_config_struct.bits.rtr = 1;
	filter_config_struct.bits.ide = 1;
	filter_config_struct.bits.res = 1;
	if (filter_flag)
		filter_config_struct.bits.extend_id = 0x1FFFFFFF;
	else
		filter_config_struct.bits.extend_id = 0;
//	can_operation_mode_enter(CAN0, CAN_INACTIVE_MODE); // 需要在暂停模式才能修改相关寄存器
//	g_receive_message.code = CAN_MB_RX_STATUS_EMPTY;
//	g_receive_message.data = (uint32_t *)can_rx_data;
//	g_receive_message.ide = CAN_ID_EXTEND;
//	g_receive_message.rtr = CAN_RTR_DATA_MODE;

//	g_receive_message.id = filter_id_0;
//	CAN_RFIFOMPF0(CAN0)				= filter_config_struct.value;
//	can_mailbox_config(CAN0, 0, &g_receive_message);

//	g_receive_message.id = filter_id_1;
//	CAN_RFIFOMPF1(CAN0)				= filter_config_struct.value;
//	can_mailbox_config(CAN0, 1, &g_receive_message);

//	g_receive_message.id = filter_id_2;
//	CAN_RFIFOMPF2(CAN0)				= filter_config_struct.value;
//	can_mailbox_config(CAN0, 2, &g_receive_message);
//	can_operation_mode_enter(CAN0, CAN_NORMAL_MODE);
}

void can_busoff_reset_fast_counter(can_handle_type *can_handle)
{
	can_handle->fast_recovery_counter = 0;
	can_handle->busoff_flag = 0;
}

void can_busoff_confirm_recovery(can_handle_type *can_handle)
{
	if(can_handle->fast_recovery_counter < CFG_FAST_RECOVERY_CNT)
	{
		can_handle->recovery_time = CFG_FAST_RECOVERY_TIME;
		can_handle->fast_recovery_counter++;
	}
	else
	{
		can_handle->recovery_time = CFG_SLOW_RECOVERY;
	}
//	can_mailbox_transmit_abort(CAN0, 31);
//	can_software_reset(CAN0);
//	std_can_init();
//	can_filter_init(0, 0, 0, 0);
	can_handle->busoff_flag = 1;   //set
}
void can_error_handle(void)
{
	// To Do
}

void can_transmit_message(can_transmit_message_type *transmit_message)
{
	if (can_handle_struct.transmit_data_flag == 1) // 不发送
		return;
		/* Identifier Value. */
	txMsg.id = transmit_message->id;
	/* Transmit data frame. */
	txMsg.rtr = 0U;
	/* 11-bit extended identifier. */
	txMsg.xtd = 1U;
	/* ESI bit in CAN FD format depends only on error passive flag. */
	txMsg.esi = 0U;
	/* Transmitting 4 bytes. */
	txMsg.dlc = transmit_message->dlc;
	/* CAN FD frames transmitted with bit rate switching. */
	txMsg.brs = 0U;
	/* Frame transmitted in Classic CAN format. */
	txMsg.fdf = 0U;
	/* Store Tx events. */
	txMsg.efc = 0U;
	/* Message Marker. */
	txMsg.mm = 0xAAU;
	/* Data bytes. */
	for (uint32_t i = 0; i < txMsg.dlc; i++)
	{
		txMsg.data[i] = transmit_message->data[i];
	}
	DL_MCAN_writeMsgRam(MCAN0_INST, DL_MCAN_MEM_TYPE_BUF, 0U, &txMsg);
	DL_MCAN_TXBufAddReq(MCAN0_INST, 0U);
}

void can_receive_message(void)
{
	extern can_handle_type can_handle_struct;
	if (can_handle_struct.gServiceInt == true)
	{
			can_handle_struct.gServiceInt = false;
			rxFS.fillLvl = 0;
			if ((can_handle_struct.gInterruptLine1Status & MCAN_IR_RF0N_MASK) == MCAN_IR_RF0N_MASK) {
			rxFS.num = DL_MCAN_RX_FIFO_NUM_0;
			while ((rxFS.fillLvl) == 0) {
					DL_MCAN_getRxFIFOStatus(MCAN0_INST, &rxFS);
			}

			DL_MCAN_readMsgRam(
					MCAN0_INST, DL_MCAN_MEM_TYPE_FIFO, 0U, rxFS.num, &rxMsg);

			DL_MCAN_writeRxFIFOAck(MCAN0_INST, rxFS.num, rxFS.getIdx);
			// User do
			can_receive_message_type *can_receive_message = &can_handle_struct.receive_message_link_table[can_handle_struct.receive_message_index];
			can_receive_message->id.U32 = rxMsg.id;
			can_receive_message->ide = rxMsg.xtd;
			can_receive_message->rtr = rxMsg.rtr;
			can_receive_message->dlc = rxMsg.dlc;
			for (uint32_t i = 0; i < can_receive_message->dlc; i++)
			{
				can_receive_message->data[i] = rxMsg.data[i];
			}
			can_receive_message->status_flag = 0;
			can_receive_message->fmi = 0;
			if(++can_handle_struct.receive_message_index >= RX_RECEIVE_NUM)                  //save next position
			{
				can_handle_struct.receive_message_index = 0;
			}
			can_handle_struct.receive_data_ready = 1;
			can_busoff_reset_fast_counter(&can_handle_struct);
			can_handle_struct.gInterruptLine1Status &= ~(MCAN_IR_RF0N_MASK);
		}
	}
}
