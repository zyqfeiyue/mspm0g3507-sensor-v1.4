#include "can_control.h"
#include "sensor_obd.h"
//#include "calibration.h"
//#include "can.h"


//#include "total_control.h"
//#include "can_unique_id.h"

can_handle_type can_handle_struct;
void handle_data1(sensor_handle_type *sensor_handle, system_type *system, can_handle_type *can_handle, sensor_obd_type *sensor_obd)
{
	static uint8_t o2_status_count = 0, nox_status_count = 0, ip1_in_range_flag = 0;
	sensor_data_type *sensor_data = &sensor_handle->sensor_data_struct;
	sensor_hardware_type *sensor_hardware = &sensor_handle->sensor_hardware_struct;
	if (system->dewpoint_command == false)
	{
		can_handle->data1_signal_values_struct.nox_concentration_byte_01.value = 0xFFFF;
		can_handle->data1_signal_values_struct.o2_concentration_byte_23.value = 0xFFFF;
		can_handle->data1_signal_values_struct.status_byte_4.value = 0xFF;
		can_handle->data1_signal_values_struct.status_byte_5.value = 0xFF;
		can_handle->data1_signal_values_struct.status_byte_6.value = 0xFF;
		can_handle->data1_signal_values_struct.status_byte_7.value = 0xFF;
		return;
	}
	can_handle->data1_signal_values_struct.nox_concentration_byte_01.value = sensor_data->nox_concentration;
	can_handle->data1_signal_values_struct.o2_concentration_byte_23.value = sensor_data->o2_concentration;
	can_handle->data1_signal_values_struct.status_byte_4.bit.o2_reading_stable = sensor_obd->data1_signal_struct.status_byte_4.bit.o2_reading_stable;
	can_handle->data1_signal_values_struct.status_byte_4.bit.nox_reading_stable = sensor_obd->data1_signal_struct.status_byte_4.bit.nox_reading_stable;
	can_handle->data1_signal_values_struct.status_byte_4.bit.sensor_power = sensor_obd->data1_signal_struct.status_byte_4.bit.sensor_power;
	can_handle->data1_signal_values_struct.status_byte_4.bit.sensor_at_temperature = sensor_obd->data1_signal_struct.status_byte_4.bit.sensor_at_temperature;
	can_handle->data1_signal_values_struct.status_byte_5.bit.error_heater = sensor_obd->data1_signal_struct.status_byte_5.bit.error_heater;
	can_handle->data1_signal_values_struct.status_byte_5.bit.status_heater_mode = sensor_obd->data1_signal_struct.status_byte_5.bit.status_heater_mode;
	can_handle->data1_signal_values_struct.status_byte_5.bit.not_used = sensor_obd->data1_signal_struct.status_byte_5.bit.not_used;
	can_handle->data1_signal_values_struct.status_byte_6.bit.error_nox = sensor_obd->data1_signal_struct.status_byte_6.bit.error_nox;
	can_handle->data1_signal_values_struct.status_byte_6.bit.status_diagnosis_feedback = sensor_obd->data1_signal_struct.status_byte_6.bit.status_diagnosis_feedback;
	can_handle->data1_signal_values_struct.status_byte_7.bit.error_o2 = sensor_obd->data1_signal_struct.status_byte_7.bit.error_o2;
	can_handle->data1_signal_values_struct.status_byte_7.bit.not_used = sensor_obd->data1_signal_struct.status_byte_7.bit.not_used;
}

void handle_data2(sensor_hardware_type *sensor_hardware, can_handle_type *can_handle)
{
	can_handle->data2_heater_ratio_deviation_struct.heater_ratio_byte_01.value = 0xFFFF;
	can_handle->data2_heater_ratio_deviation_struct.nox_correction_gain_byte_23.value = 1000; // 不补偿
	can_handle->data2_heater_ratio_deviation_struct.nox_correction_offset_byte_4 = 125; // 不补偿
	sensor_hardware->life_hours = sensor_hardware->life_seconds / 3600;
	can_handle->data2_heater_ratio_deviation_struct.operation_hours_counter_byte_56.value = (uint16_t)sensor_hardware->life_hours;
	can_handle->data2_heater_ratio_deviation_struct.not_used_byte_7 = 0xFF;
}
void handle_data3(can_handle_type *can_handle)
{
	can_handle->data3_correction_factors_struct.correction_pressure_lambda_byte_0 = 0;
	can_handle->data3_correction_factors_struct.correction_pressure_nox_byte_1 = 0;
	can_handle->data3_correction_factors_struct.correction_no2_byte_2 = 0;
	can_handle->data3_correction_factors_struct.correction_nh3_byte_3 = 0;
	can_handle->data3_correction_factors_struct.self_diagnosis_result_value_byte_4 = 0xFF;
}

// 正常模式
void normal_can_transmit_message(sensor_handle_type *sensor_handle, can_handle_type *can_handle, system_type * system, 
	sensor_obd_type *sensor_obd)
{
	if (handle_normal_transmit_data(can_handle) == 0)
		return;
	sensor_data_type *sensor_data = &sensor_handle->sensor_data_struct;
	sensor_hardware_type *sensor_hardware = &sensor_handle->sensor_hardware_struct;
	if ((sensor_hardware->hardware_error & 0x04) != 0x00)
	{
		sensor_hardware->hardware_error &= (uint8_t)~(1 << 2); // 对应位清0
		sensor_hardware_error(can_handle, system->position);
		if (can_handle->cts_connect == CAN_TRANSMIT)
		{
			can_handle->data_message_timebase_compare = TIM_60_MS;
		}
		else
		{
			can_handle->data_message_timebase_compare = TIM_10_MS;
		}
	}
	switch (can_handle->message_command_num)
	{
		case CAN_MESSAGE_CMD_NULL:
			break;
		case CAN_MESSAGE_CMD_BUSY:
			sensor_obd_command_busy(can_handle, system->position);
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
			can_handle->cts_connect = CAN_CTS_UNCONNECT;
			can_handle->data_message_timebase_compare = TIM_10_MS;
			break;
		case CAN_MESSAGE_CMD_ACK:
			sensor_obd_command_ack(can_handle, system->position, 0x01);
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
			can_handle->cts_connect = CAN_CTS_UNCONNECT;
			can_handle->data_message_timebase_compare = TIM_10_MS;
			break;
		case CAN_MESSAGE_CMD_DATA1:
			handle_data1(sensor_handle, system, can_handle, sensor_obd);
			sensor_obd_command_data1(can_handle, system->position, sensor_hardware->hardware_error);
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
			break;
		case CAN_MESSAGE_CMD_DATA2:
			handle_data2(sensor_hardware, can_handle);
			sensor_obd_command_data2(can_handle, system->position, sensor_hardware->hardware_error);
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
			break;
		case CAN_MESSAGE_CMD_DATA3:
			handle_data3(can_handle);
			sensor_obd_command_data3(can_handle, system->position, sensor_hardware->hardware_error);
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
			can_handle->cts_connect = CAN_CTS_UNCONNECT;
			can_handle->data_message_timebase_compare = TIM_10_MS;
			break;
		case CAN_MESSAGE_CMD_DATA4:
			sensor_obd_command_data4(can_handle, system->position, (uint8_t *)"0000");
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
			can_handle->cts_connect = CAN_CTS_UNCONNECT;
			can_handle->data_message_timebase_compare = TIM_10_MS;
			break;
		case CAN_MESSAGE_CMD_DATA5:
			sensor_obd_command_data5(can_handle, system->position);
			if(can_handle->adr == 0xFF) // 广播
			{
				if( (sensor_hardware->hardware_error & 0x08) != 0x00)
				{
//					delay(500us);
				}
				can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_1_flag = 1;
				can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
				can_handle->cts_connect = CAN_TRANSMIT;
				can_handle->data_message_timebase_compare = TIM_60_MS;
			}
			else
			{
				can_handle->cts_connect = CAN_CTS_CONNECTING;
				can_handle->can_timeout_count = 0;
				can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
				can_handle->data_message_timebase_compare = TIM_10_MS;
			}
			break;
		case CAN_MESSAGE_CMD_DATA5_1:
			sensor_obd_command_data5_1(can_handle, system->position);
			can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_2_flag = 1;
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
			if(can_handle->cts_connect == CAN_TRANSMIT)
				can_handle->data_message_timebase_compare = TIM_60_MS;
			else can_handle->data_message_timebase_compare = TIM_10_MS;
			break;
		case CAN_MESSAGE_CMD_DATA5_2:
			sensor_obd_command_data5_2(can_handle, system->position);
			can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_3_flag = 1;
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
			if(can_handle->cts_connect == CAN_TRANSMIT)
				can_handle->data_message_timebase_compare = TIM_60_MS;
			else can_handle->data_message_timebase_compare = TIM_10_MS;
			break;
		case CAN_MESSAGE_CMD_DATA5_3:
			sensor_obd_command_data5_3(can_handle, system->position);
			can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_4_flag = 1;
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
			if(can_handle->cts_connect == CAN_TRANSMIT)
				can_handle->data_message_timebase_compare = TIM_60_MS;
			else can_handle->data_message_timebase_compare = TIM_10_MS;
			break;
		case CAN_MESSAGE_CMD_DATA5_4:
			sensor_obd_command_data5_4(can_handle, system->position);
			can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_5_flag = 1;
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
			if(can_handle->cts_connect == CAN_TRANSMIT)
				can_handle->data_message_timebase_compare = TIM_60_MS;
			else can_handle->data_message_timebase_compare = TIM_10_MS;
			break;
		case CAN_MESSAGE_CMD_DATA5_5:
			sensor_obd_command_data5_5(can_handle, system->position);
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
			if(can_handle->cts_connect == CAN_CTS_TRANSMIT)//建立连接方式下的处理方式，等待2.5s接收endack
			{
				can_handle->cts_connect = CAN_CTS_CONNECTING;
				can_handle->can_timeout_count = 0;
			}
			else can_handle->cts_connect = CAN_CTS_UNCONNECT;//global下的处理
			break;
		case CAN_MESSAGE_CMD_DATA6:
			sensor_obd_command_data6(can_handle, system->position, sensor_hardware->hardware_error);
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
			can_handle->data_message_timebase_compare = TIM_10_MS;
			break;
		case CAN_MESSAGE_CMD_DATA7:
			sensor_obd_command_data7(can_handle, system->position);
			if(can_handle->adr == 0xFF)
			{
				can_handle->sensor_transmit_data_flag_struct.bit.data7_cts_on_request_1_flag = 1;
				can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
				can_handle->cts_connect = CAN_TRANSMIT;
				can_handle->data_message_timebase_compare = TIM_60_MS;
			}
			else
			{
				can_handle->cts_connect = CAN_CTS_CONNECTING;
				can_handle->can_timeout_count = 0;
				can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
				can_handle->data_message_timebase_compare = TIM_10_MS;
			}
			break;
		case CAN_MESSAGE_CMD_DATA7_1:
			sensor_obd_command_data7_1(can_handle, system->position);
			can_handle->sensor_transmit_data_flag_struct.bit.data7_cts_on_request_2_flag = 1;
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
			if(can_handle->cts_connect == CAN_TRANSMIT)
				can_handle->data_message_timebase_compare = TIM_60_MS;
			else can_handle->data_message_timebase_compare = TIM_10_MS;
			break;
		case CAN_MESSAGE_CMD_DATA7_2:
			sensor_obd_command_data7_2(can_handle, system->position);
			can_handle->sensor_transmit_data_flag_struct.bit.data7_cts_on_request_3_flag = 1;
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
			if(can_handle->cts_connect  == CAN_TRANSMIT)
				can_handle->data_message_timebase_compare = TIM_60_MS;
			else can_handle->data_message_timebase_compare = TIM_10_MS;
			break;
		case CAN_MESSAGE_CMD_DATA7_3:
			sensor_obd_command_data7_3(can_handle, system->position);
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
			if(can_handle->cts_connect == CAN_CTS_TRANSMIT)//建立连接方式下的处理方式，等待2.5s接收endack
			{
				can_handle->cts_connect = CAN_CTS_CONNECTING;
				can_handle->can_timeout_count = 0;
			}
			else can_handle->cts_connect = CAN_CTS_UNCONNECT;
			break;
		case CAN_MESSAGE_CMD_DATA10:
			sensor_obd_command_data10(can_handle, system->position);
			if(can_handle->adr == 0xFF)
			{
				can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_1_flag = 1;
				can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
				can_handle->cts_connect = CAN_TRANSMIT;
				can_handle->data_message_timebase_compare = TIM_60_MS;
			}
			else
			{
				can_handle->cts_connect = CAN_CTS_CONNECTING;
				can_handle->can_timeout_count = 0;
				can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
				can_handle->data_message_timebase_compare = TIM_10_MS;
			}
			break;
		case CAN_MESSAGE_CMD_DATA10_1:
			sensor_obd_command_data10_1(can_handle, system->position, 0, 0, 0);
			can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_2_flag = 1;
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
			if(can_handle->cts_connect == CAN_TRANSMIT)
				can_handle->data_message_timebase_compare = TIM_60_MS;
			else can_handle->data_message_timebase_compare = TIM_10_MS;
			break;
		case CAN_MESSAGE_CMD_DATA10_2:
			sensor_obd_command_data10_2(can_handle, system->position, 0, 0, 0);
			can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_3_flag = 1;
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
			if(can_handle->cts_connect == CAN_TRANSMIT)
				can_handle->data_message_timebase_compare = TIM_60_MS;
			else can_handle->data_message_timebase_compare = TIM_10_MS;
			break;
		case CAN_MESSAGE_CMD_DATA10_3:
			sensor_obd_command_data10_3(can_handle, system->position, 0, 0, 0);
			can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_4_flag = 1;
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
			if(can_handle->cts_connect == CAN_TRANSMIT)
				can_handle->data_message_timebase_compare = TIM_60_MS;
			else can_handle->data_message_timebase_compare = TIM_10_MS;
			break;
		case CAN_MESSAGE_CMD_DATA10_4:
			sensor_obd_command_data10_4(can_handle, system->position, 0, 0, 0);
			can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_5_flag = 1;
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
			if(can_handle->cts_connect == CAN_TRANSMIT)
				can_handle->data_message_timebase_compare = TIM_60_MS;
			else can_handle->data_message_timebase_compare = TIM_10_MS;
			break;
		case CAN_MESSAGE_CMD_DATA10_5:
			sensor_obd_command_data10_5(can_handle, system->position);
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
			if(can_handle->cts_connect == CAN_CTS_TRANSMIT)//建立连接方式下的处理方式，等待2.5s接收endack
			{
				can_handle->cts_connect = CAN_CTS_CONNECTING;
				can_handle->can_timeout_count = 0;
			}
			else 
				can_handle->cts_connect = CAN_CTS_UNCONNECT;
			break;
		default:
			break;
	}
}


uint8_t handle_normal_transmit_data(can_handle_type *can_handle)
{
	static uint32_t now_time = 0;
	static uint32_t on_request_time = 0;
	static uint32_t cyclic_time = 0;
	static uint32_t cyclic_message_count = 0;
	now_time = soft_timer_tick_counter_get();
	if( now_time - cyclic_time >= 1 ) // 1ms
	{
		cyclic_time = soft_timer_tick_counter_get();
		cyclic_message_count++;
		cyclic_message_count %= 1000;
		//--CAN协议超时计数
		if((can_handle->can_timeout_count >= DL35TIMEOUT_BASE_1MS) && (can_handle->cts_connect == CAN_CTS_TRANSMIT))
		{
			can_handle->cts_connect = CAN_CTS_SHUTOFF;
			can_handle->can_timeout_count = DL35TIMEOUT_BASE_1MS;
		}
		else
		{
			can_handle->can_timeout_count++;
		}
	}
	else
		return 0;
	if ((cyclic_message_count % 50) == 0) // Data1
	{
		if (can_handle->sensor_transmit_data_flag_struct.bit.data1_cyclic_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data1_cyclic_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA1;
		}
	}
	else if (cyclic_message_count == 1) // Data2 before dew point or on request
	{
		if (can_handle->sensor_transmit_data_flag_struct.bit.data2_cyclic_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data2_cyclic_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA2;
		}
	}
	else if (now_time - on_request_time >= can_handle->data_message_timebase_compare)
	{
		on_request_time = soft_timer_tick_counter_get();
		if (can_handle->sensor_transmit_data_flag_struct.bit.data2_on_request_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data2_on_request_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA2;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data3_on_request_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data3_on_request_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA3;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data4_on_request_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data4_on_request_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA4;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data5_on_request_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data5_on_request_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA5;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_1_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_1_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA5_1;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_2_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_2_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA5_2;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_3_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_3_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA5_3;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_4_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_4_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA5_4;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_5_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_5_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA5_5;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data6_on_request_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data6_on_request_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA6;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data7_on_request_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data7_on_request_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA7;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data7_cts_on_request_1_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data7_cts_on_request_1_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA7_1;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data7_cts_on_request_2_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data7_cts_on_request_2_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA7_2;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data7_cts_on_request_3_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data7_cts_on_request_3_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA7_3;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data10_on_request_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data10_on_request_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA10;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_1_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_1_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA10_1;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_2_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_2_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA10_2;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_3_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_3_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA10_3;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_4_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_4_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA10_4;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_5_flag == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data10_cts_on_request_5_flag = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_DATA10_5;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data_ack == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data_ack = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_ACK;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data_null == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data_null = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_NULL;
		}
		else if (can_handle->sensor_transmit_data_flag_struct.bit.data_busy == 1)
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data_busy = 0;
			can_handle->message_command_num = CAN_MESSAGE_CMD_BUSY;
		}
	}
	return 1;
}
/*-----------------------------------------------------------------------
 函数: void CanActiveTx_Message( CanBusType *can_handle,system_type * system,sensor_type *sensor,time_type *time )
 描述: Debug或Calibration下主动上传  原函数名CanDebugOut
 参数:
 返回: none.
 备注:
------------------------------------------------------------------------*/
void debug_can_transmit_message(can_handle_type *can_handle, system_type * system, sensor_handle_type *sensor_handle, sensor_calibration_type *sensor)
{
	static uint32_t now_time = 0;
	static uint32_t debug_time = 0;
	static eeprom_data_union read_data;
	sensor_data_type *sensor_data = &sensor_handle->sensor_data_struct;
	sensor_hardware_type *sensor_hardware = &sensor_handle->sensor_hardware_struct;
	now_time = soft_timer_tick_counter_get();
	if( now_time - debug_time >= 50 ) // 50ms
	{
		debug_time = soft_timer_tick_counter_get();
	}
	else
		return;
	if( system->position == POSITION_EXHAUST )
		can_handle->transmit_message.id = 0x0CFD0F52;
	else
		can_handle->transmit_message.id = 0x0CFD1151;
	can_handle->transmit_message.ide = CAN_ID_EXTEND;
	can_handle->transmit_message.rtr = CAN_RTR_DATA_MODE;
	can_handle->transmit_message.dlc = 8;
	switch( can_handle->can_debug_count )
	{
		case 0:
			can_handle->transmit_message.data[0] = can_handle->can_debug_count;
			can_handle->transmit_message.data[1] = ((uint16_t)sensor_data->power_input_data) & 0xff;//======power=====
			can_handle->transmit_message.data[2] = ((int16_t)sensor_data->ip1_filt) & 0xFF;
			can_handle->transmit_message.data[3] = ((int16_t)sensor_data->ip1_filt) >> 8;
			can_handle->transmit_message.data[4] = ((int16_t)sensor_data->vp1_filt) & 0xFF;
			can_handle->transmit_message.data[5] = ((int16_t)sensor_data->vp1_filt) >> 8;
			can_handle->transmit_message.data[6] = ((int16_t)sensor_data->rpvs_origin_data) & 0xFF;
			can_handle->transmit_message.data[7] = ((int16_t)sensor_data->rpvs_origin_data) >> 8;;
			if(can_handle->recovery_time <= 1)
				can_transmit_message(&can_handle->transmit_message);
			can_handle->can_debug_count++;
			break;

		case 1:
			can_handle->transmit_message.data[0] = can_handle->can_debug_count;
			can_handle->transmit_message.data[1] = ((uint16_t)sensor_data->power_input_data) >> 8; //
			can_handle->transmit_message.data[2] = ((int16_t)sensor_data->rpvs_amp_data) & 0xFF;
			can_handle->transmit_message.data[3] = ((int16_t)sensor_data->rpvs_amp_data) >> 8;;
			can_handle->transmit_message.data[4] = ((int16_t)sensor_data->ip2_filt) & 0xFF;;
			can_handle->transmit_message.data[5] = ((int16_t)sensor_data->ip2_filt) >> 8;
			can_handle->transmit_message.data[6] = ((int16_t)sensor_data->vp2_filt) & 0xFF;
			can_handle->transmit_message.data[7] = ((int16_t)sensor_data->vp2_filt) >> 8;
			if(can_handle->recovery_time <= 1)
				can_transmit_message(&can_handle->transmit_message);
			can_handle->can_debug_count++;
			break;

		case 2:
			can_handle->transmit_message.data[0] = can_handle->can_debug_count;
			can_handle->transmit_message.data[1] = ((int16_t)sensor_data->vs_s_f_data) & 0xFF;
			can_handle->transmit_message.data[2] = ((int16_t)sensor_data->vs_s_f_data) >> 8;
			can_handle->transmit_message.data[3] = ((int16_t)sensor_data->vs_data) & 0xFF;
			can_handle->transmit_message.data[4] = ((int16_t)sensor_data->vs_data) >> 8;
			can_handle->transmit_message.data[5] = ((int16_t)sensor_data->com_filt) & 0xFF;
			can_handle->transmit_message.data[6] = ((int16_t)sensor_data->com_filt) >> 8;
			can_handle->transmit_message.data[7] = 0;
			if(can_handle->recovery_time <= 1)
				can_transmit_message(&can_handle->transmit_message);
			can_handle->can_debug_count++;
			break;//

		case 3:
			can_handle->transmit_message.data[0] = 3;
			can_handle->transmit_message.data[1] = 0xFF;
			can_handle->transmit_message.data[2] = 0xFF;
			can_handle->transmit_message.data[3] = 0xFF;
			can_handle->transmit_message.data[4] = 0xFF;
			can_handle->transmit_message.data[5] = 0xFF;
			can_handle->transmit_message.data[6] = 0xFF;
			can_handle->transmit_message.data[7] = 0xFF;
      can_transmit_message(&can_handle->transmit_message);
			can_handle->can_debug_count = 0;
			break;//
 #ifdef ENABLE_UNIQUE_ID_MODE
		case 4:
      can_handle->transmit_message.data[0] = 4;
			can_handle->transmit_message.data[1] = Uniqueid.SNInfo.ReceiveData[0];
			can_handle->transmit_message.data[2] = Uniqueid.SNInfo.ReceiveData[1];
			can_handle->transmit_message.data[3] = Uniqueid.SNInfo.ReceiveData[2];
			can_handle->transmit_message.data[4] = Uniqueid.SNInfo.ReceiveData[3];
			can_handle->transmit_message.data[5] = Uniqueid.SNInfo.ReceiveData[4];
			can_handle->transmit_message.data[6] = 0xFF;
			can_handle->transmit_message.data[7] = 0xFF;
      can_transmit_message(&can_handle->transmit_message);
			can_handle->can_debug_count = 0;
			break;
 #endif /* ENABLE_UNIQUE_ID_MODE */
		case 100:
			read_data = eeprom_read_data(EEPROM_ADDRESS_RPVS_BASE);
			sensor->calibration_rpvs = read_data.bit.original;
			read_data = eeprom_read_data(EEPROM_ADDRESS_VS_BASE);
			sensor->calibration_vs = read_data.bit.original;
			can_handle->transmit_message.data[0] = can_handle->can_debug_count;
			can_handle->transmit_message.data[1] = 0;
			can_handle->transmit_message.data[2] = sensor->calibration_rpvs & 0xFF;
			can_handle->transmit_message.data[3] = sensor->calibration_rpvs >> 8;
			can_handle->transmit_message.data[4] = sensor->calibration_vs & 0xFF;
			can_handle->transmit_message.data[5] = sensor->calibration_vs >> 8;
			can_handle->transmit_message.data[6] = 0;
			can_handle->transmit_message.data[7] = 0;
			can_transmit_message(&can_handle->transmit_message);
			can_handle->can_debug_count++;
			break;
		case 101:
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO100O2LOW_IP1);
			sensor->calibration_no100_o2low_ip1 = read_data.bit.original;
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO100O2LOW_IP2);
			sensor->calibration_no100_o2low_ip2 = read_data.bit.original;
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO100O2LOW_VP1);
			sensor->calibration_no100_o2low_vp1 = read_data.bit.original;
			can_handle->transmit_message.data[0] = can_handle->can_debug_count;
			can_handle->transmit_message.data[1] = 0;
			can_handle->transmit_message.data[2] = sensor->calibration_no100_o2low_ip1 & 0xFF;
			can_handle->transmit_message.data[3] = sensor->calibration_no100_o2low_ip1 >> 8;
			can_handle->transmit_message.data[4] = sensor->calibration_no100_o2low_ip2 & 0xFF;
			can_handle->transmit_message.data[5] = sensor->calibration_no100_o2low_ip2 >> 8;
			can_handle->transmit_message.data[6] = sensor->calibration_no100_o2low_vp1 & 0xFF;
			can_handle->transmit_message.data[7] = sensor->calibration_no100_o2low_vp1 >> 8;
			can_transmit_message(&can_handle->transmit_message);
			can_handle->can_debug_count++;
			break;
		case 102:
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO100O2HIGH_IP1);
			sensor->calibration_no100_o2high_ip1 = read_data.bit.original;
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO100O2HIGH_IP2);
			sensor->calibration_no100_o2high_ip2 = read_data.bit.original;
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO100O2HIGH_VP1);
			sensor->calibration_no100_o2high_vp1 = read_data.bit.original;
			can_handle->transmit_message.data[0] = can_handle->can_debug_count;
			can_handle->transmit_message.data[1] = 0;
			can_handle->transmit_message.data[2] = sensor->calibration_no100_o2high_ip1 & 0xFF;
			can_handle->transmit_message.data[3] = sensor->calibration_no100_o2high_ip1 >> 8;
			can_handle->transmit_message.data[4] = sensor->calibration_no100_o2high_ip2 & 0xFF;
			can_handle->transmit_message.data[5] = sensor->calibration_no100_o2high_ip2 >> 8;
			can_handle->transmit_message.data[6] = sensor->calibration_no100_o2high_vp1 & 0xFF;
			can_handle->transmit_message.data[7] = sensor->calibration_no100_o2high_vp1 >> 8;
			can_transmit_message(&can_handle->transmit_message);
			can_handle->can_debug_count++;
			break;
		case 103:
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO500O2LOW_IP1);
			sensor->calibration_no500_o2low_ip1 = read_data.bit.original;
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO500O2LOW_IP2);
			sensor->calibration_no500_o2low_ip2 = read_data.bit.original;
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO500O2LOW_VP1);
			sensor->calibration_no500_o2low_vp1 = read_data.bit.original;
			can_handle->transmit_message.data[0] = can_handle->can_debug_count;
			can_handle->transmit_message.data[1] = 0;
			can_handle->transmit_message.data[2] = sensor->calibration_no500_o2low_ip1 & 0xFF;
			can_handle->transmit_message.data[3] = sensor->calibration_no500_o2low_ip1 >> 8;
			can_handle->transmit_message.data[4] = sensor->calibration_no500_o2low_ip2 & 0xFF;
			can_handle->transmit_message.data[5] = sensor->calibration_no500_o2low_ip2 >> 8;
			can_handle->transmit_message.data[6] = sensor->calibration_no500_o2low_vp1 & 0xFF;
			can_handle->transmit_message.data[7] = sensor->calibration_no500_o2low_vp1 >> 8;
			can_transmit_message(&can_handle->transmit_message);
			can_handle->can_debug_count++;
			break;
		case 104:
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO500O2HIGH_IP1);
			sensor->calibration_no500_o2high_ip1 = read_data.bit.original;
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO500O2HIGH_IP2);
			sensor->calibration_no500_o2high_ip2 = read_data.bit.original;
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO500O2HIGH_VP1);
			sensor->calibration_no500_o2high_vp1 = read_data.bit.original;
			can_handle->transmit_message.data[0] = can_handle->can_debug_count;
			can_handle->transmit_message.data[1] = 0;
			can_handle->transmit_message.data[2] = sensor->calibration_no500_o2high_ip1 & 0xFF;
			can_handle->transmit_message.data[3] = sensor->calibration_no500_o2high_ip1 >> 8;
			can_handle->transmit_message.data[4] = sensor->calibration_no500_o2high_ip2 & 0xFF;
			can_handle->transmit_message.data[5] = sensor->calibration_no500_o2high_ip2 >> 8;
			can_handle->transmit_message.data[6] = sensor->calibration_no500_o2high_vp1 & 0xFF;
			can_handle->transmit_message.data[7] = sensor->calibration_no500_o2high_vp1 >> 8;
			can_transmit_message(&can_handle->transmit_message);
			can_handle->can_debug_count++;
			break;
		case 105:
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO1500O2LOW_IP1);
			sensor->calibration_no1500_o2low_ip1 = read_data.bit.original;
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO1500O2LOW_IP2);
			sensor->calibration_no1500_o2low_ip2 = read_data.bit.original;
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO1500O2LOW_VP1);
			sensor->calibration_no1500_o2low_vp1 = read_data.bit.original;
			can_handle->transmit_message.data[0] = can_handle->can_debug_count;
			can_handle->transmit_message.data[1] = 0;
			can_handle->transmit_message.data[2] = sensor->calibration_no1500_o2low_ip1 & 0xFF;
			can_handle->transmit_message.data[3] = sensor->calibration_no1500_o2low_ip1 >> 8;
			can_handle->transmit_message.data[4] = sensor->calibration_no1500_o2low_ip2 & 0xFF;
			can_handle->transmit_message.data[5] = sensor->calibration_no1500_o2low_ip2 >> 8;
			can_handle->transmit_message.data[6] = sensor->calibration_no1500_o2low_vp1 & 0xFF;
			can_handle->transmit_message.data[7] = sensor->calibration_no1500_o2low_vp1 >> 8;
			can_transmit_message(&can_handle->transmit_message);
			can_handle->can_debug_count++;
			break;

		case 106:
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO1500O2HIGH_IP1);
			sensor->calibration_no1500_o2high_ip1 = read_data.bit.original;
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO1500O2HIGH_IP2);
			sensor->calibration_no1500_o2high_ip2 = read_data.bit.original;
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO1500O2HIGH_VP1);
			sensor->calibration_no1500_o2high_vp1 = read_data.bit.original;
			can_handle->transmit_message.data[0] = can_handle->can_debug_count;
			can_handle->transmit_message.data[1] = 0;
			can_handle->transmit_message.data[2] = sensor->calibration_no1500_o2high_ip1 & 0xFF;
			can_handle->transmit_message.data[3] = sensor->calibration_no1500_o2high_ip1 >> 8;
			can_handle->transmit_message.data[4] = sensor->calibration_no1500_o2high_ip2 & 0xFF;
			can_handle->transmit_message.data[5] = sensor->calibration_no1500_o2high_ip2 >> 8;
			can_handle->transmit_message.data[6] = sensor->calibration_no1500_o2high_vp1 & 0xFF;
			can_handle->transmit_message.data[7] = sensor->calibration_no1500_o2high_vp1 >> 8;
			can_transmit_message(&can_handle->transmit_message);
			can_handle->can_debug_count++;
			break;
		case 107:
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO0O2HIGH_IP1);
			sensor->calibration_no0_o2high_ip1 = read_data.bit.original;
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO0O2HIGH_IP2);
			sensor->calibration_no0_o2high_ip2 = read_data.bit.original;
			read_data = eeprom_read_data(EEPROM_ADDRESS_NO0O2HIGH_VP1);
			sensor->calibration_no0_o2high_vp1 = read_data.bit.original;
			can_handle->transmit_message.data[0] = can_handle->can_debug_count;
			can_handle->transmit_message.data[1] = 0;
			can_handle->transmit_message.data[2] = sensor->calibration_no0_o2high_ip1 & 0xFF;
			can_handle->transmit_message.data[3] = sensor->calibration_no0_o2high_ip1 >> 8;
			can_handle->transmit_message.data[4] = sensor->calibration_no0_o2high_ip2 & 0xFF;
			can_handle->transmit_message.data[5] = sensor->calibration_no0_o2high_ip2 >> 8;
			can_handle->transmit_message.data[6] = sensor->calibration_no0_o2high_vp1 & 0xFF;
			can_handle->transmit_message.data[7] = sensor->calibration_no0_o2high_vp1 >> 8;
			can_transmit_message(&can_handle->transmit_message);
			can_handle->can_debug_count++;
			break;
		case 108:
			if( sensor->calibration_number == CALIBRATION_NUMBER_MAX )
			{
				system->mode = SYSTEM_MODE_DEBUG;//标定操作后退出到SYSTEM_MODE_DEBUG模式
				can_handle->can_debug_count = 0;
			}
			break;

//		case 120:
//			eeprom_read( EEPROM_ADDRESS_NOx100_ADD, system->eeprom_buffer, 2 );////
//			sensor->no100_add = system->eeprom_buffer[ 0 ];
//			eeprom_read( EEPROM_ADDRESS_NOx500_ADD, system->eeprom_buffer, 2 );////
//			sensor->no500_add = system->eeprom_buffer[ 0 ];
//			eeprom_read( EEPROM_ADDRESS_NOx500_FACTOR, system->eeprom_buffer, 2 );////
//			sensor->no500_factor = system->eeprom_buffer[ 0 ];
//			eeprom_read( EEPROM_ADDRESS_NOx1500_ADD, system->eeprom_buffer, 2 );////
//			sensor->no1500_add = system->eeprom_buffer[ 0 ];
//			eeprom_read( EEPROM_ADDRESS_NOx1500_FACTOR, system->eeprom_buffer, 2 );////
//			sensor->no1500_factor = system->eeprom_buffer[ 0 ];
//			eeprom_read( EEPROM_ADDRESS_NOx3000_ADD, system->eeprom_buffer, 2 );////
//			sensor->no3000_add = system->eeprom_buffer[ 0 ];
//			eeprom_read( EEPROM_ADDRESS_NOx3000_FACTOR, system->eeprom_buffer, 2 );////
//			sensor->no3000_factor = system->eeprom_buffer[ 0 ];
//			can_handle->transmit_message.data[0] = can_handle->can_debug_count;
//			can_handle->transmit_message.data[1] = sensor->no100_add;
//			can_handle->transmit_message.data[2] = sensor->no500_add;
//			can_handle->transmit_message.data[3] = sensor->no500_factor;
//			can_handle->transmit_message.data[4] = sensor->no1500_add;
//			can_handle->transmit_message.data[5] = sensor->no1500_factor;
//			can_handle->transmit_message.data[6] = sensor->no3000_add;
//			can_handle->transmit_message.data[7] = sensor->no3000_factor;
//			if(can_handle->recovery_time <= 1)
//				can_transmit_message(&can_handle->transmit_message);
//			can_handle->can_debug_count = 0;
//			break;
//		case 130:
////			EepromRead( EEPROM_ADDRESS_NOx4FAC, System.eeprom_buffer, 4 );//
//			//Sensor.nox4fac = (float)System.eeprom_buffer[ 0 ]/10;
////			eeprom_read( EEPROM_ADDRESS_NOx4FAC, system->eeprom_buffer, 2 );////
////			can_handle->transmit_message.data[0] = can_handle->can_debug_count;
////			can_handle->transmit_message.data[1] = system->eeprom_buffer[0];
////			can_handle->transmit_message.data[2] = 0x00;
////			can_handle->transmit_message.data[3] = 0x00;
////			can_handle->transmit_message.data[4] = 0x00;
////			can_handle->transmit_message.data[5] = 0x00;
////			can_handle->transmit_message.data[6] = 0x00;
////			can_handle->transmit_message.data[7] = 0x00;
////			if(can_handle->recovery_time <= 1)
////				can_transmit_message(&can_handle->transmit_message);
////			can_handle->can_debug_count = 0;
//			break;


		default:
			break;
	}

}
/*-----------------------------------------------------------------------
 函数: enum CAN_OBD_CMD  Get_FF_CMD_Number(CanRxMsg * pRxCan)
 描述:    获取FF的PGN--    data2=FD0F          data3=FD0E           data4=FEDA
 参数:                   data5=FDC5         data6=EE00            data7=D300
 返回: none.	获取FF的PGN，就应该能包括51，52的
 备注:
------------------------------------------------------------------------*/
void get_ff_command_number(can_receive_message_type *pRxCan, sensor_transmit_data_flag_type *sensor_transmit_data_flag_struct)
{
	if(pRxCan->data[ 2 ] == 0x00)
	{
		if(( ( pRxCan->data[ 0 ] == 0x11 ) && ( pRxCan->data[ 1 ] == 0xFD ))||
			(( pRxCan->data[ 0 ] == 0x0F ) && ( pRxCan->data[ 1 ] == 0xFD )))
			sensor_transmit_data_flag_struct->bit.data2_on_request_flag = 1;
		else if(( ( pRxCan->data[ 0 ] == 0x10 ) && ( pRxCan->data[ 1 ] == 0xFD ))||
			(( pRxCan->data[ 0 ] == 0x0E ) && ( pRxCan->data[ 1 ] == 0xFD )))
			sensor_transmit_data_flag_struct->bit.data3_on_request_flag = 1;
		else if( ( pRxCan->data[ 0 ] == 0xDA ) && ( pRxCan->data[ 1 ] == 0xFE ))
			sensor_transmit_data_flag_struct->bit.data4_on_request_flag = 1;
		else if( ( pRxCan->data[ 0 ] == 0xC5 ) && ( pRxCan->data[ 1 ] == 0xFD ))
			sensor_transmit_data_flag_struct->bit.data5_on_request_flag = 1;
		else if( ( pRxCan->data[ 0 ] == 0x00 ) && ( pRxCan->data[ 1 ] == 0xEE ))
			sensor_transmit_data_flag_struct->bit.data6_on_request_flag = 1;
		else if( ( pRxCan->data[ 0 ] == 0x00 ) && ( pRxCan->data[ 1 ] == 0xD3 ))
			sensor_transmit_data_flag_struct->bit.data7_on_request_flag = 1;
	}
	else
	{
		sensor_transmit_data_flag_struct->bit.data_ack = 1;
	}
	sensor_transmit_data_flag_struct->bit.data_null = 1;
}
/*-----------------------------------------------------------------------
 函数: enum CAN_OBD_CMD  Get_51_CMD_Number(CanRxMsg * pRxCan)
 描述:    获取51的PGN--  data2=FD11          data3=FD10           data4=FEDA
 参数:                   data5=FDC5         data6=EE00            data7=D300
 返回: none.	 51与52 就data2,data3的PGN不一样
 备注:
------------------------------------------------------------------------*/
void get_51_command_number(can_receive_message_type * pRxCan, sensor_transmit_data_flag_type *sensor_transmit_data_flag_struct)
{
	if(pRxCan->data[ 2 ] == 0x00)
	{
		if( ( pRxCan->data[ 0 ] == 0x11 ) && ( pRxCan->data[ 1 ] == 0xFD ))
			sensor_transmit_data_flag_struct->bit.data2_on_request_flag = 1;
		else if( ( pRxCan->data[ 0 ] == 0x10 ) && ( pRxCan->data[ 1 ] == 0xFD ))
			sensor_transmit_data_flag_struct->bit.data3_on_request_flag = 1;
		else if( ( pRxCan->data[ 0 ] == 0xDA ) && ( pRxCan->data[ 1 ] == 0xFE ))
			sensor_transmit_data_flag_struct->bit.data4_on_request_flag = 1;
		else if( ( pRxCan->data[ 0 ] == 0xC5 ) && ( pRxCan->data[ 1 ] == 0xFD ))
			sensor_transmit_data_flag_struct->bit.data5_on_request_flag = 1;
		else if( ( pRxCan->data[ 0 ] == 0x00 ) && ( pRxCan->data[ 1 ] == 0xEE ))
			sensor_transmit_data_flag_struct->bit.data6_on_request_flag = 1;
		else if( ( pRxCan->data[ 0 ] == 0x00 ) && ( pRxCan->data[ 1 ] == 0xD3 ))
			sensor_transmit_data_flag_struct->bit.data7_on_request_flag = 1;
	}
	else
	{
		sensor_transmit_data_flag_struct->bit.data_ack = 1;
	}
	sensor_transmit_data_flag_struct->bit.data_null = 1;  //所有命令字不匹配，返回为空
}
/*-----------------------------------------------------------------------
 函数: enum CAN_OBD_CMD  Get_52_CMD_Number(CanRxMsg * pRxCan)
 描述:    获取52的PGN--    data2=FD0F          data3=FD0E           data4=FEDA
 参数:                   data5=FDC5         data6=EE00            data7=D300
 返回: none.	51与52 就data2,data3的PGN不一样
 备注:
------------------------------------------------------------------------*/
void get_52_command_number(can_receive_message_type * pRxCan, sensor_transmit_data_flag_type *sensor_transmit_data_flag_struct)
{
	if(pRxCan->data[ 2 ] == 0x00)
	{
		if( ( pRxCan->data[ 0 ] == 0x0F ) && ( pRxCan->data[ 1 ] == 0xFD ))
			sensor_transmit_data_flag_struct->bit.data2_on_request_flag = 1;
		else if( ( pRxCan->data[ 0 ] == 0x0E ) && ( pRxCan->data[ 1 ] == 0xFD ))
			sensor_transmit_data_flag_struct->bit.data3_on_request_flag = 1;
		else if( ( pRxCan->data[ 0 ] == 0xDA ) && ( pRxCan->data[ 1 ] == 0xFE ))
			sensor_transmit_data_flag_struct->bit.data4_on_request_flag = 1;
		else if( ( pRxCan->data[ 0 ] == 0xC5 ) && ( pRxCan->data[ 1 ] == 0xFD ))
			sensor_transmit_data_flag_struct->bit.data5_on_request_flag = 1;
		else if( ( pRxCan->data[ 0 ] == 0x00 ) && ( pRxCan->data[ 1 ] == 0xEE ))
			sensor_transmit_data_flag_struct->bit.data6_on_request_flag = 1;
		else if( ( pRxCan->data[ 0 ] == 0x00 ) && ( pRxCan->data[ 1 ] == 0xD3 ))
			sensor_transmit_data_flag_struct->bit.data7_on_request_flag = 1;
	}
	else
	{
		sensor_transmit_data_flag_struct->bit.data_ack = 1;
	}
	sensor_transmit_data_flag_struct->bit.data_null = 1;  //所有命令字不匹配，返回为空
}
void can_receive_process(can_handle_type *can_handle, system_type *system)
{
	static uint8_t mask_data;
	static uint8_t shift_bit;
	static can_receive_message_type RxMsg;

	if(can_handle->receive_message_counter == can_handle->receive_message_index )//超过一圈重新造成相等，说明开辟存储深度不足
	{
		return;
	}
//--获取接收数据组--进来之后 RxMsgCounter 总是滞后 RxMsgIndex 1~RX_RECEIVE_NUM值
	RxMsg = can_handle->receive_message_link_table[can_handle->receive_message_counter];    //接收数据 RxMsgIndex

	if(++can_handle->receive_message_counter >= RX_RECEIVE_NUM)       //clr
	{
		can_handle->receive_message_counter = 0;
	}
	if (0x18EA5161 == RxMsg.id.U32) // 接收上位机的PID数据
	{
		ReceiveExcessOutputData(RxMsg.data);
	}
//--解析HOST的命令
	else if( ( 0x18FEDF00 == RxMsg.id.U32 ) || ( 0x18FEDF3D == RxMsg.id.U32 ) || ( 0x18FEDF55 == RxMsg.id.U32 ))//SYSTEM_MODE_DEFAULT下的ID
	{
		shift_bit = 0;                                           //假设为POSITION_INTAKE
		mask_data = 0x03;
		if( system->position == POSITION_EXHAUST)                //改成POSITION_EXHAUST
		{
			shift_bit = 2;
			mask_data = 0x0C;    //mask_data=0x03<<shift_bit;
		}
		if((RxMsg.data[ 7 ] & mask_data ) == (1 << shift_bit))//传感器工作控制字节 // 加热
		{
			system->preheater_command = false;   //这里不操作，该命令是什么值就是什么值
			system->dewpoint_command = true;
//			sensor_hardware->heater_timeout_count = TIM_300_S_BASE_1MS;
		}
		else  //进气的00，  02，  03 或   出气的00，08，0x0c全部停止加热 // 停止加热
		{
			system->preheater_command = true;   //这里不操作，该命令是什么值就是什么值
			system->dewpoint_command = false;
		}
	}
}
void can_receive_process1(can_handle_type *can_handle, system_type * system, sensor_hardware_type *sensor_hardware, sensor_calibration_type *sensor)
{
	static uint8_t mask_data;
	static uint8_t shift_bit;
	static can_receive_message_type RxMsg;

	if(can_handle->receive_message_counter == can_handle->receive_message_index )//超过一圈重新造成相等，说明开辟存储深度不足
	{
		return;
	}
//--获取接收数据组--进来之后 RxMsgCounter 总是滞后 RxMsgIndex 1~RX_RECEIVE_NUM值
	RxMsg = can_handle->receive_message_link_table[can_handle->receive_message_counter];    //接收数据 RxMsgIndex

	if(++can_handle->receive_message_counter >= RX_RECEIVE_NUM)       //clr
	{
		can_handle->receive_message_counter = 0;
	}


//--解析HOST的命令
	if( ( 0x18FEDF00 == RxMsg.id.U32 ) || ( 0x18FEDF3D == RxMsg.id.U32 ) || ( 0x18FEDF55 == RxMsg.id.U32 ))//SYSTEM_MODE_DEFAULT下的ID
	{
		shift_bit = 0;                                           //假设为POSITION_INTAKE
		mask_data = 0x03;
		if( system->position == POSITION_EXHAUST)                //改成POSITION_EXHAUST
		{
			shift_bit = 2;
			mask_data = 0x0C;    //mask_data=0x03<<shift_bit;
		}
		if((RxMsg.data[ 7 ] & mask_data ) == (1 << shift_bit))//传感器工作控制字节 // 加热
		{
			system->preheater_command = false;   //这里不操作，该命令是什么值就是什么值
			system->dewpoint_command = true;
			sensor_hardware->heater_timeout_count = TIM_300_S_BASE_1MS;
		}
		else  //进气的00，  02，  03 或   出气的00，08，0x0c全部停止加热 // 停止加热
		{
			system->preheater_command = true;   //这里不操作，该命令是什么值就是什么值
			system->dewpoint_command = false;
		}
	}

	else if( 0x18EEFF52 == RxMsg.id.U32)  //检测地址干涉故障 52
	{
		if( system->position == POSITION_EXHAUST )
		{
			if((sensor_hardware->hardware_error & 0x0F) == 0x00)
			{
				sensor_hardware->hardware_error |= 0x08;   //地址干涉故障
				system->run_state = SYSTEM_RUN_STATE_WAIT_JUDGESENSOR;  //SYSTEM_RUN_STATE_WAIT_JUDGESENSOR
			}
		}
	}
	else if(0x18EEFF51 == RxMsg.id.U32 )  //检测址干涉故障  51
	{
		if( system->position == POSITION_INTAKE )
		{
			if((sensor_hardware->hardware_error & 0x0F) == 0x00)
			{
				sensor_hardware->hardware_error |= 0x08;   //地址干涉故障
				system->run_state = SYSTEM_RUN_STATE_WAIT_JUDGESENSOR;  //SYSTEM_RUN_STATE_WAIT_JUDGESENSOR
			}
		}
	}
	else if(0x18EEFFFE == RxMsg.id.U32 )  //清地址干涉故障
	{
		if((sensor_hardware->hardware_error&0x08) == 0x08)
		{
			sensor_hardware->hardware_error &= 0xF7;
		}
	}
	else if (0x18EA5161 == RxMsg.id.U32) // 接收上位机的PID数据
	{
		ReceiveExcessOutputData(RxMsg.data);
	}
	else if (((0x18EA5162 == RxMsg.id.U32 ) && ( system->position == POSITION_INTAKE )) // 接收上位机的复位命令
		|| (( 0x18EA5262 == RxMsg.id.U32 ) && ( system->position == POSITION_EXHAUST )))
	{
//		__set_FAULTMASK(1);
		NVIC_SystemReset();
//		HAL_NVIC_SystemReset();
	}
	else if (((0x18EA5163 == RxMsg.id.U32 ) && ( system->position == POSITION_INTAKE )) // 接收上位机的关闭CAN发送命令
		|| (( 0x18EA5263 == RxMsg.id.U32 ) && ( system->position == POSITION_EXHAUST )))
	{
		can_handle->transmit_data_flag = 1;
	}
	else if((( 0x18EA5155 == RxMsg.id.U32 ) && ( system->position == POSITION_INTAKE )) //为了用51和52区分写参数
		|| (( 0x18EA5255 == RxMsg.id.U32 ) && ( system->position == POSITION_EXHAUST )))//自定义协议
	{
		if( system->mode == SYSTEM_MODE_NORMAL )
		{
			//---WL自定义协议可以加在此处 --Debug模式通信扩展特性--暂时不加

			//---判断接收PGN请求
			if((can_handle->cts_connect == CAN_CTS_TRANSMIT) || (can_handle->cts_connect  == CAN_TRANSMIT))//正在传输
			{
				can_handle->message_command_num = CAN_MESSAGE_CMD_BUSY;
				can_handle->sensor_transmit_data_flag_struct.bit.data_busy = 1;
			}
			else
			{
				can_handle->adr = RxMsg.id.U32_8_BS.dxx8x_Flags;
				can_handle->yy = RxMsg.id.U32_8_BS.dxxx8_Flags;
				can_handle->pgnlsb = RxMsg.data[ 0 ];
				can_handle->pgn = RxMsg.data[ 1 ];
				can_handle->pgnmsb = RxMsg.data[ 2 ];

				if(RxMsg.data[ 2 ] == 0x00)//这种方式改变了原始判断 ynw
				{
					get_ff_command_number(&RxMsg, &can_handle->sensor_transmit_data_flag_struct);            //FF具有51，52共同特性
				}
				//ID为0x18EA5155 数据如下则进入debug模式，每隔50ms循环输出三帧debug数据
				else if( ( RxMsg.data[ 0 ] == 0x21 ) && ( RxMsg.data[ 1 ] == 0x32 ) && ( RxMsg.data[ 2 ] == 0x43 )
					&& ( RxMsg.data[ 3 ] == 0x66 ) && ( RxMsg.data[ 4 ] == 0x55 )
					&& ( RxMsg.data[ 5 ] == 0x76 ) && ( RxMsg.data[ 6 ] == 0x87 ) && ( RxMsg.data[ 7 ] == 0x98 ) )
				{
					system->mode = SYSTEM_MODE_DEBUG;//输出调试信息
					can_handle->can_debug_count = 0;         //debug输出数据指针
				}
				#ifdef ENABLE_UNIQUE_ID_MODE
      //判断是否为进入UniqueId命令=========================================================================
				else if( ( RxMsg.data[ 0 ] == 0xF0 ) && ( RxMsg.data[ 1 ] == 0xF0 ) && ( RxMsg.data[ 2 ] == 0x43 )
            && ( RxMsg.data[ 3 ] == 0x66 ) && ( RxMsg.data[ 4 ] == 0x55 ) && ( RxMsg.data[ 5 ] == 0x76 )
            && ( RxMsg.data[ 6 ] == 0x45 ) && ( RxMsg.data[ 7 ] == 0x45 ) /*&& ( 0x18EA5255 == Can.rxId)*/ )
        {
          system->mode = SYSTEM_MODE_UNIQUE_ID;//进入该模式
          //Uniqueid.CanOutIndex = 0;//也可放在全局数据请求处，此项须屏蔽，应上位机要求，接收到命令后无需重置序号
          Uniqueid.CanOutRequestType = Global;//暂未增加对数据请求帧的判断，所以直接放结果
        }
				#endif /* ENABLE_UNIQUE_ID_MODE */
				else
				{
					can_handle->message_command_num = CAN_MESSAGE_CMD_ACK;
				}
			}
		}
		else if( system->mode == SYSTEM_MODE_DEBUG )
		{
			switch(RxMsg.data[ 0 ])
			{
				case(0x12)://自定义读或进入标定
					if( ( RxMsg.data[ 1 ] == 0x23 ) && ( RxMsg.data[ 2 ] == 0x34 )
					&& ( RxMsg.data[ 3 ] == 0x55 ) && ( RxMsg.data[ 4 ] == 0x66 ))
					{
						if(( RxMsg.data[ 5 ] == 0x67 ) && ( RxMsg.data[ 6 ] == 0x78 ) && ( RxMsg.data[ 7 ] == 0x89 ))//读取标定信息命令
						{
							system->mode = SYSTEM_MODE_CALIBRATION;
							sensor->calibration_number = CALIBRATION_NUMBER_MAX;
							sensor->calibration_count = 0;       //--3S内操作，超过3S后退出到SYSTEM_MODE_DEBUG模式
							can_handle->can_debug_count = 100;     //debug输出数据指针，输出读出的之前标定消息
						}
						else if(( RxMsg.data[ 5 ] == 0x67 ) && ( RxMsg.data[ 6 ] == 0x99 ) && ( RxMsg.data[ 7 ] == 0x99 ))//启动标定命令
						{
							system->mode = SYSTEM_MODE_CALIBRATION;
							sensor->calibration_number = CALIBRATION_NUMBER_NONE;//CALIBRATION_NUMBER_NONE;
							sensor->calibration_count = TIM_1_6_S_BASE_10MS;//--3S内操作，超过3S后退出到SYSTEM_MODE_DEFAULT模式
							can_handle->can_debug_count = 108;      // 不输出debug信息
						}
					}
					else if(( RxMsg.data[ 1 ] == 0x23 ) && ( RxMsg.data[ 2 ] == 0x34 ) 
						&& ( RxMsg.data[ 3 ] == 0x56 ) && ( RxMsg.data[ 4 ] == 0x65 )
						 && ( RxMsg.data[ 5 ] == 0x43 ) && ( RxMsg.data[ 6 ] == 0x32 ) && ( RxMsg.data[ 7 ] == 0x21 ) )
					{
						can_handle->can_debug_count = 120;//debug输出数据指针，输出读出的之前标定消息
					}
					else if(( RxMsg.data[ 1 ] == 0x23 ) && ( RxMsg.data[ 2 ] == 0x34 ) 
						&& ( RxMsg.data[ 3 ] == 0x56 ) && ( RxMsg.data[ 4 ] == 0x56 )
						 && ( RxMsg.data[ 5 ] == 0x34 ) && ( RxMsg.data[ 6 ] == 0x23 ) && ( RxMsg.data[ 7 ] == 0x12 ) )
					{
						can_handle->can_debug_count = 130;//debug输出数据指针，输出读出的之前标定消息
					}

					break;
				case(0x21):  //复位指令
					if(( RxMsg.data[ 1 ] == 0x32 ) && ( RxMsg.data[ 2 ] == 0x43 )
					&& ( RxMsg.data[ 3 ] == 0x66 ) && ( RxMsg.data[ 4 ] == 0x55 )
					&& ( RxMsg.data[ 5 ] == 0x76 ) && ( RxMsg.data[ 6 ] == 0x88 ) && ( RxMsg.data[ 7 ] == 0x88 ))
					{
						NVIC_SystemReset();
					}
				#ifdef ENABLE_UNIQUE_ID_MODE
				case 0xF1: //由上位机在生产标定加热丝电阻时，写入传感器ID
					if (RxMsg.data[ 1 ] == 0x1F)
					{
						UniqueID_CANRecInterrupt_WriteSN(&RxMsg, sensor);
					}
					break;
				case 0xF0: // 判断是否为进入UniqueId命令
					if(( RxMsg.data[ 1 ] == 0xF0 ) && ( RxMsg.data[ 2 ] == 0x43 )
					&& ( RxMsg.data[ 3 ] == 0x66 ) && ( RxMsg.data[ 4 ] == 0x55 )
					&& ( RxMsg.data[ 5 ] == 0x76 ) && ( RxMsg.data[ 6 ] == 0x45 ) && ( RxMsg.data[ 7 ] == 0x45 ))
					{
						system->mode = SYSTEM_MODE_UNIQUE_ID;//进入该模式
						//Uniqueid.CanOutIndex = 0;//也可放在全局数据请求处
						Uniqueid.CanOutRequestType = Global;//暂未增加对数据请求帧的判断，所以直接放结果
					}
					break;
				#endif
				default:
//					modify_calibration_parameter(RxMsg.data, sensor, system, heater_pid);
					break;
			}

		}
		else if( system->mode == SYSTEM_MODE_CALIBRATION )
		{
			//标定气体命令，主程序调用CalibrationAppParameter 函数完成>>>>>>>>>>>>>>>>>>>>>
			uint8_t temp = ~RxMsg.data[ 7 ];  //取非，用于简单校验
			if( ( RxMsg.data[ 0 ] == 0x98 ) && ( RxMsg.data[ 1 ] == 0x87 ) && ( RxMsg.data[ 2 ] == 0x76 )
				&& ( RxMsg.data[ 3 ] == 0x63 ) && ( RxMsg.data[ 4 ] == 0x32 )
				 && ( RxMsg.data[ 5 ] == 0x21 ) && ( RxMsg.data[ 6 ] == temp ) )
			{
				if( ( RxMsg.data[ 6 ] != 0 ) && ( RxMsg.data[ 6 ] < 0x0A ) )//NO号在1~9之间
				{
					sensor->calibration_number = RxMsg.data[ 6 ];  //标定序列号
					sensor->calibration_count = 0;                  //退出到SYSTEM_MODE_DEBUG模式
				}
				else
				{
					system->mode = SYSTEM_MODE_DEBUG;         //接收到错误，返回DEBUG模式
					can_handle->can_debug_count = 0;
				}
			}
			//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		}
		#ifdef ENABLE_UNIQUE_ID_MODE
		else if (system->mode == SYSTEM_MODE_UNIQUE_ID)
		{
          //上位机数据请求
      if( ( RxMsg.data[ 2 ] == 0x22 ) && ( RxMsg.data[ 3 ] == 0x33 ) && ( RxMsg.data[ 4 ] == 0x55 )
       && ( RxMsg.data[ 5 ] == 0x76 ) && ( RxMsg.data[ 6 ] == 0x45 ) && ( RxMsg.data[ 7 ] == 0x45 )
       /*&& ( 0x18EA5255 == Can.rxId)*/ )
			{
				/*
				//接收到数据全局请求
				Uniqueid.CanOutRequestType = Global;
				*/
			}
      //退出Uniqueid模式(软件复位)
      else if( ( RxMsg.data[ 0 ] == 0xF0 ) && ( RxMsg.data[ 1 ] == 0xF0 ) && ( RxMsg.data[ 2 ] == 0x43 )
            && ( RxMsg.data[ 3 ] == 0x66 ) && ( RxMsg.data[ 4 ] == 0x55 ) && ( RxMsg.data[ 5 ] == 0x76 )
            && ( RxMsg.data[ 6 ] == 0x54 ) && ( RxMsg.data[ 7 ] == 0x54 ) /*&& ( 0x18EA5255 == Can.rxId)*/ )
			{
				NVIC_SystemReset();
			}
			else if (RxMsg.data[0] == 0xA6) // 写参数
			{
			
			}
		}
		#endif
	}

	else if(( ( 0x18EA5100 == RxMsg.id.U32 ) || ( 0x18EA513D == RxMsg.id.U32 ) || ( 0x18EA5155 == RxMsg.id.U32 )) && ( system->position == POSITION_INTAKE ))
	{
		if((can_handle->cts_connect  == CAN_CTS_TRANSMIT)|| (can_handle->cts_connect  == CAN_TRANSMIT))//正在传输
		{
			if( ( RxMsg.data[ 0 ] == 0xC5 ) && ( RxMsg.data[ 1 ] == 0xFD ) && ( RxMsg.data[ 2 ] == 0x00 ) )
			{

			}
			else if( ( RxMsg.data[ 0 ] == 0x00 ) && ( RxMsg.data[ 1 ] == 0xD3 ) && ( RxMsg.data[ 2 ] == 0x00 ) )
			{

			}
			else
			{
				can_handle->sensor_transmit_data_flag_struct.bit.data_busy = 1;
			}
		}
		else
		{
			can_handle->adr = RxMsg.id.U32_8_BS.dxx8x_Flags;  //can_handle->Adr = (u8)((RxMsg.Id.U32 & 0x0000FF00) >> 8);
			can_handle->yy = RxMsg.id.U32_8_BS.dxxx8_Flags;   //can_handle->yy = (u8)(RxMsg.Id.U32 & 0x000000FF);
			can_handle->pgnlsb = RxMsg.data[ 0 ];
			can_handle->pgn = RxMsg.data[ 1 ];
			can_handle->pgnmsb = RxMsg.data[ 2 ];

			get_51_command_number(&RxMsg, &can_handle->sensor_transmit_data_flag_struct);//获取51地址的命令号
		}

	}
	else if((( 0x18EA5200 == RxMsg.id.U32 ) || ( 0x18EA523D == RxMsg.id.U32 ) || ( 0x18EA5255 == RxMsg.id.U32 ))  && ( system->position == POSITION_EXHAUST ))
	{
		if((can_handle->cts_connect  == CAN_CTS_TRANSMIT)|| (can_handle->cts_connect  == CAN_TRANSMIT))//正在传输
		{
			if( ( RxMsg.data[ 0 ] == 0xC5 ) && ( RxMsg.data[ 1 ] == 0xFD ) && ( RxMsg.data[ 2 ] == 0x00 ) ) //data5或DM19特定请求PGN判断
			{

			}
			else if( ( RxMsg.data[ 0 ] == 0x00 ) && ( RxMsg.data[ 1 ] == 0xD3 ) && ( RxMsg.data[ 2 ] == 0x00 ) )
			{

			}
			else
			{
				can_handle->sensor_transmit_data_flag_struct.bit.data_busy = 1;
			}
		}
		else
		{
			can_handle->adr = RxMsg.id.U32_8_BS.dxx8x_Flags;  //can_handle->Adr = (u8)((RxMsg.Id.U32 & 0x0000FF00) >> 8);
			can_handle->yy = RxMsg.id.U32_8_BS.dxxx8_Flags;   //can_handle->yy = (u8)(RxMsg.Id.U32 & 0x000000FF);

			can_handle->pgnlsb = RxMsg.data[ 0 ];
			can_handle->pgn = RxMsg.data[ 1 ];
			can_handle->pgnmsb = RxMsg.data[ 2 ];

			get_52_command_number(&RxMsg, &can_handle->sensor_transmit_data_flag_struct);//获取52地址的命令号
		}
	}
	else if(( 0x18EAFF00 == RxMsg.id.U32 ) || ( 0x18EAFF3D == RxMsg.id.U32 ) || ( 0x18EAFF55 == RxMsg.id.U32 ) )
	{
		if((can_handle->cts_connect == CAN_CTS_TRANSMIT) || (can_handle->cts_connect == CAN_TRANSMIT))//正在传输
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data_busy = 1;
		}
		else
		{
			can_handle->adr = RxMsg.id.U32_8_BS.dxx8x_Flags;  //can_handle->Adr = (u8)((RxMsg.Id.U32 & 0x0000FF00) >> 8);
			can_handle->yy = RxMsg.id.U32_8_BS.dxxx8_Flags;   //can_handle->yy = (u8)(RxMsg.Id.U32 & 0x000000FF);

			can_handle->pgnlsb = RxMsg.data[ 0 ];
			can_handle->pgn = RxMsg.data[ 1 ];
			can_handle->pgnmsb = RxMsg.data[ 2 ];

			get_ff_command_number(&RxMsg, &can_handle->sensor_transmit_data_flag_struct);//获取FF地址的命令号
		}
	}
	else if( ( 0x1CEC5100 == RxMsg.id.U32 ) ||( 0x1CEC513D == RxMsg.id.U32 ) ||( 0x1CEC5155 == RxMsg.id.U32 ) ||
			( 0x1CEC5200 == RxMsg.id.U32 )  ||( 0x1CEC523D == RxMsg.id.U32 ) || ( 0x1CEC5255 == RxMsg.id.U32 ))//CTS
	{
		if((can_handle->cts_connect  == CAN_CTS_TRANSMIT) || (can_handle->cts_connect  == CAN_TRANSMIT))//正在传输
		{
			if((RxMsg.data[ 0 ] == 0xFF) && (can_handle->cts_connect  == CAN_CTS_TRANSMIT))//TP.CM-ABORT
			{
				can_handle->cts_connect = CAN_CTS_UNCONNECT;
				if((RxMsg.data[ 2 ] == 0xFF)&&(RxMsg.data[ 3 ] == 0xFF)&&(RxMsg.data[ 4 ] == 0xFF)&&
					(RxMsg.data[ 5 ] == 0xC5)&&(RxMsg.data[ 6 ] == 0xFD)&&(RxMsg.data[ 7 ] == 0x00))//data5
          {
  					can_handle->sensor_transmit_data_flag_struct.bit.data_null = 1;
          }
				else if((RxMsg.data[ 2 ] == 0xFF)&&(RxMsg.data[ 3 ] == 0xFF)&&(RxMsg.data[ 4 ] == 0xFF)&&
					(RxMsg.data[ 5 ] == 0x00)&&(RxMsg.data[ 6 ] == 0xD3)&&(RxMsg.data[ 7 ] == 0x00))//data7=DM19
          {
            can_handle->sensor_transmit_data_flag_struct.bit.data_null = 1;
          }
				else
          {
            can_handle->sensor_transmit_data_flag_struct.bit.data_null = 1;
          }
			}
			else if(RxMsg.data[ 0 ] == 19)//TP.CM-END
			{
				if(( RxMsg.data[ 1 ] == 0x1E )&&(RxMsg.data[ 2 ] == 0x00)&&(RxMsg.data[ 3 ] == 0x05)&&
					(RxMsg.data[ 4 ] == 0xFF)&&(RxMsg.data[ 5 ] == 0xC5)&&(RxMsg.data[ 6 ] == 0xFD)&&(RxMsg.data[ 7 ] == 0x00))//data5
          {
            can_handle->sensor_transmit_data_flag_struct.bit.data_null = 1;
          }
				else if(( RxMsg.data[ 1 ] == 0x14)&&(RxMsg.data[ 2 ] == 0x00)&&(RxMsg.data[ 3 ] == 0x03)&&
					(RxMsg.data[ 4 ] == 0xFF)&&(RxMsg.data[ 5 ] == 0x00)&&(RxMsg.data[ 6 ] == 0xD3)&&(RxMsg.data[ 7 ] == 0x00))//data7
          {
            can_handle->sensor_transmit_data_flag_struct.bit.data_null = 1;
          }
				else
          {
            can_handle->sensor_transmit_data_flag_struct.bit.data_busy = 1;
          }
			}
			else
      {
        can_handle->sensor_transmit_data_flag_struct.bit.data_busy = 1;
      }
		}
		else if(can_handle->cts_connect  == CAN_CTS_CONNECTING)//连接中……
		{
			if(RxMsg.data[ 0 ] == 0x11)//TP.CM-CTS 17
			{
				can_handle->cts_connect = CAN_CTS_TRANSMIT;
				if(( RxMsg.data[ 1 ] == 0x05 )&&(RxMsg.data[ 3 ] == 0xFF)&&(RxMsg.data[ 4 ] == 0xFF)&&
					(RxMsg.data[ 5 ] == 0xC5)&&(RxMsg.data[ 6 ] == 0xFD)&&(RxMsg.data[ 7 ] == 0x00))//data5
          {
						can_handle->sensor_transmit_data_flag_struct.bit.data5_cts_on_request_1_flag = 1;
          }
				else if(( RxMsg.data[ 1 ] == 0x03 )&&(RxMsg.data[ 3 ] == 0xFF)&&(RxMsg.data[ 4 ] == 0xFF)&&
					(RxMsg.data[ 5 ] == 0x00)&&(RxMsg.data[ 6 ] == 0xD3)&&(RxMsg.data[ 7 ] == 0x00))//data7
					{
            can_handle->sensor_transmit_data_flag_struct.bit.data7_cts_on_request_1_flag = 1;
          }
				else
          {
            can_handle->sensor_transmit_data_flag_struct.bit.data_ack = 1;
          }
			}
			else if(RxMsg.data[ 0 ] == 0x13)//TP.CM-END
			{
				can_handle->cts_connect = CAN_CTS_UNCONNECT;
				if(( RxMsg.data[ 1 ] == 0x1E )&&(RxMsg.data[ 2 ] == 0x00)&&(RxMsg.data[ 3 ] == 0x05)&&
					(RxMsg.data[ 4 ] == 0xFF)&&(RxMsg.data[ 5 ] == 0xC5)&&(RxMsg.data[ 6 ] == 0xFD)&&(RxMsg.data[ 7 ] == 0x00))//data5
          {
						can_handle->sensor_transmit_data_flag_struct.bit.data_null = 1;
          }
				else if(( RxMsg.data[ 1 ] == 0x14)&&(RxMsg.data[ 2 ] == 0x00)&&(RxMsg.data[ 3 ] == 0x03)&&
					(RxMsg.data[ 4 ] == 0xFF)&&(RxMsg.data[ 5 ] == 0x00)&&(RxMsg.data[ 6 ] == 0xD3)&&(RxMsg.data[ 7 ] == 0x00))//data7
          {
  					can_handle->sensor_transmit_data_flag_struct.bit.data_null = 1;
          }
				else
          {
            can_handle->sensor_transmit_data_flag_struct.bit.data_ack = 1;
          }
			}
			else if(RxMsg.data[ 0 ] == 0xFF)//TP.CM-ABORT
			{
				can_handle->cts_connect = CAN_CTS_UNCONNECT;
				if((RxMsg.data[ 2 ] == 0xFF)&&(RxMsg.data[ 3 ] == 0xFF)&&(RxMsg.data[ 4 ] == 0xFF)&&
					(RxMsg.data[ 5 ] == 0xC5)&&(RxMsg.data[ 6 ] == 0xFD)&&(RxMsg.data[ 7 ] == 0x00))//data5
          {
            can_handle->sensor_transmit_data_flag_struct.bit.data_null = 1;
          }
				else if((RxMsg.data[ 2 ] == 0xFF)&&(RxMsg.data[ 3 ] == 0xFF)&&(RxMsg.data[ 4 ] == 0xFF)&&
					(RxMsg.data[ 5 ] == 0x00)&&(RxMsg.data[ 6 ] == 0xD3)&&(RxMsg.data[ 7 ] == 0x00))//data7
          {
            can_handle->sensor_transmit_data_flag_struct.bit.data_null = 1;
          }
				else
          {
            can_handle->sensor_transmit_data_flag_struct.bit.data_ack = 1;
          }
			}
			else
			{
				can_handle->cts_connect = CAN_CTS_UNCONNECT;
				can_handle->sensor_transmit_data_flag_struct.bit.data_ack = 1;
			}
		}
		else//未接收过请求指令
		{
			can_handle->sensor_transmit_data_flag_struct.bit.data_ack = 1;
		}
	}
	else if((uint8_t)RxMsg.id.U32_8_BS.dxxx8_Flags==0x52 && system->position == POSITION_EXHAUST )//地址冲突，自己收到自己ID号
	{
//		if((sensor->hardware_error & 0x0F) == 0x00)
//		{
//			sensor->hardware_error |= 0x04;
//			can_handle->data_message_timebase_compare = TIM_5_MS;
//		}
	}
	else if((uint8_t)RxMsg.id.U32_8_BS.dxxx8_Flags==0x51 && system->position == POSITION_INTAKE )//地址冲突，自己收到自己ID号
	{
//		if((sensor->hardware_error & 0x0F) == 0x00)
//		{
//			sensor->hardware_error |= 0x04;
//			can_handle->data_message_timebase_compare = TIM_5_MS;
//		}
	}
}
