/*
 * can_control.h
 *
 *  Created on: 2021-11-16
 *      Author: zyq
 */

#ifndef CAN_CONTROL_H_
#define CAN_CONTROL_H_
#include "main.h"
#include "can_driver.h"
#include "adc_app.h"
#include "heater_control.h"
#include "system_control.h"
#include "soft_timer.h"
#include "can_command.h"
#include "chip_excess_output.h"
#include "calibration.h"
#include "sensor_obd.h"

extern can_handle_type can_handle_struct;
void can_receive_process(can_handle_type *can_handle, system_type *system);
void can_receive_process1(can_handle_type *can_handle, system_type * system, sensor_hardware_type *sensor_hardware, sensor_calibration_type *sensor);
uint8_t handle_normal_transmit_data(can_handle_type *can_handle);
void normal_can_transmit_message(sensor_handle_type *sensor_handle, can_handle_type *can_handle, system_type * system, 
	sensor_obd_type *sensor_obd);
void debug_can_transmit_message(can_handle_type *can_handle, system_type * system, sensor_handle_type *sensor_handle, sensor_calibration_type *sensor);
//void normal_can_transmit_message(my9706_driver_type *my9706_driver, can_handle_type *can_handle,system_type * system,sensor_type *sensor, heater_pid_type *heater_pid);
//void debug_can_transmit_message( can_handle_type *can_handle,system_type * system,sensor_type *sensor, heater_pid_type *heater_pid);
//void can_receive_process(can_handle_type *can_handle, system_type * system, sensor_hardware_type *sensor_hardware);
#endif
