/*
 * can_control.h
 *
 *  Created on: 2021-11-16
 *      Author: zyq
 */

#ifndef LINEAR_FITTING_H_
#define LINEAR_FITTING_H_
#include "main.h"
uint8_t linear_fitting(float *arrayX, float *arrayY, uint8_t len, float *pRetFactor,float *pRetConstant);

#endif
