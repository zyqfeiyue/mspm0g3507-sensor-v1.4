#include "nox_calculate.h"

float calibration_no100_o2low_concentration = 3.f;

float calibration_no100_o2high_concentration = 17.f;

float calibration_no500_o2low_concentration = 5.f;

float calibration_no500_o2high_concentration = 15.f;

float calibration_no1500_o2low_concentration = 0.f;

float calibration_no1500_o2high_concentration = 13.f;

float calibration_no0_o2high_concentration = 20.5f;
typedef struct
{
	float no100_o2_k;
	float no100_o2_b;
	float no500_o2_k;
	float no500_o2_b;
	float no1500_o2_k;
	float no1500_o2_b;
	
	float calculate_no100_o3_ip2;
	float calculate_no100_o15_ip2;
	float calculate_no500_o3_ip2;
	float calculate_no500_o15_ip2;
	float calculate_no1500_o3_ip2;
	float calculate_no1500_o15_ip2;
	float calculate_no3000_o3_ip2;
	float calculate_no3000_o15_ip2;

	float no100_500_o2low_k;
	float no100_500_o2low_b;
	float no100_500_o2high_k;
	float no100_500_o2high_b;
	float no100_500_k_k; // 斜率曲线的斜率
	float no100_500_k_b;
	float no100_500_b_k; // 截距曲线的斜率
	float no100_500_b_b;

	float no500_1500_o2low_k;
	float no500_1500_o2low_b;
	float no500_1500_o2high_k;
	float no500_1500_o2high_b;
	float no500_1500_k_k; // 斜率曲线的斜率
	float no500_1500_k_b;
	float no500_1500_b_k; // 截距曲线的斜率
	float no500_1500_b_b;
	
	float no1500_3000_o2low_k;
	float no1500_3000_o2low_b;
	float no1500_3000_o2high_k;
	float no1500_3000_o2high_b;
	float no1500_3000_k_k; // 斜率曲线的斜率
	float no1500_3000_k_b;
	float no1500_3000_b_k; // 截距曲线的斜率
	float no1500_3000_b_b;

	float no0_100_o2low_k;
	float no0_100_o2low_b;
	float no0_100_o2high_k;
	float no0_100_o2high_b;
	float no0_100_k_k; // 斜率曲线的斜率
	float no0_100_k_b;
	float no0_100_b_k; // 截距曲线的斜率
	float no0_100_b_b;

	float low_o2_a;
	float low_o2_b;
	float low_o2_c;
	float high_o2_a;
	float high_o2_b;
	float high_o2_c;
	
	float lambda;
	float o2;
} nox_calculate_type;

static uint16_t get_average(uint16_t *data_table, uint8_t len)
{
	uint32_t temp;
	uint8_t i;
	temp = 0;
	for(i = 0; i < len; i++ )
	{
		temp += data_table[i];
	}
	temp /= len;
	return (uint16_t)temp;
}
nox_calculate_type nox_calculate_struct = {0};

void calculate_equation(sensor_calibration_type *sensor)
{
	sensor->calibration_no100_o2low_ip1 = 327;
	sensor->calibration_no100_o2low_ip2 = 363;
	sensor->calibration_no100_o2high_ip1 = 2405;
	sensor->calibration_no100_o2high_ip2 = 415;
	
	sensor->calibration_no500_o2low_ip1 = 595;
	sensor->calibration_no500_o2low_ip2 = 1440;
	sensor->calibration_no500_o2high_ip1 = 2103;
	sensor->calibration_no500_o2high_ip2 = 1585;
	
	sensor->calibration_no1500_o2low_ip1 = -112;
	sensor->calibration_no1500_o2low_ip2 = 3875;
	sensor->calibration_no1500_o2high_ip1 = 1795;
	sensor->calibration_no1500_o2high_ip2 = 4330;
	
	sensor->calibration_no0_o2high_ip1 = 2965;
	sensor->calibration_no0_o2high_ip2 = 90;

	float array_x[2];
	float array_y[2];
	array_x[0] = calibration_no100_o2low_concentration;
	array_x[1] = calibration_no100_o2high_concentration;
	array_y[0] = sensor->calibration_no100_o2low_ip2;
	array_y[1] = sensor->calibration_no100_o2high_ip2;
	linear_fitting(array_x, array_y, 2, &nox_calculate_struct.no100_o2_k, &nox_calculate_struct.no100_o2_b);
	nox_calculate_struct.calculate_no100_o3_ip2 = nox_calculate_struct.no100_o2_k * 3 + nox_calculate_struct.no100_o2_b;
	nox_calculate_struct.calculate_no100_o15_ip2 = nox_calculate_struct.no100_o2_k * 15 + nox_calculate_struct.no100_o2_b;

	array_x[0] = calibration_no500_o2low_concentration;
	array_x[1] = calibration_no500_o2high_concentration;
	array_y[0] = sensor->calibration_no500_o2low_ip2;
	array_y[1] = sensor->calibration_no500_o2high_ip2;
	linear_fitting(array_x, array_y, 2, &nox_calculate_struct.no500_o2_k, &nox_calculate_struct.no500_o2_b);
	nox_calculate_struct.calculate_no500_o3_ip2 = nox_calculate_struct.no500_o2_k * 3 + nox_calculate_struct.no500_o2_b;
	nox_calculate_struct.calculate_no500_o15_ip2 = nox_calculate_struct.no500_o2_k * 15 + nox_calculate_struct.no500_o2_b;

	array_x[0] = calibration_no1500_o2low_concentration;
	array_x[1] = calibration_no1500_o2high_concentration;
	array_y[0] = sensor->calibration_no1500_o2low_ip2;
	array_y[1] = sensor->calibration_no1500_o2high_ip2;
	linear_fitting(array_x, array_y, 2, &nox_calculate_struct.no1500_o2_k, &nox_calculate_struct.no1500_o2_b);
	nox_calculate_struct.calculate_no1500_o3_ip2 = nox_calculate_struct.no1500_o2_k * 3 + nox_calculate_struct.no1500_o2_b;
	nox_calculate_struct.calculate_no1500_o15_ip2 = nox_calculate_struct.no1500_o2_k * 15 + nox_calculate_struct.no1500_o2_b;
	
	float array_x_nox_o2_low[4] = {0, 100, 500, 1500};
	float array_y_ip2_o2_low[4] = {sensor->calibration_no0_o2high_ip2, nox_calculate_struct.calculate_no100_o3_ip2, nox_calculate_struct.calculate_no500_o3_ip2, nox_calculate_struct.calculate_no1500_o3_ip2};
	float coeff_ip2[3];
	get_coeff(array_x_nox_o2_low, array_y_ip2_o2_low, coeff_ip2, 4, 2);
	float a = coeff_ip2[2];
	float b = coeff_ip2[1];
	float c = coeff_ip2[0];
	float result = a * 2000.f * 2000.f + b * 2000.f + c;
	nox_calculate_struct.calculate_no3000_o3_ip2 = result;
	float array_x_nox_o2_high[4] = {0, 100, 500, 1500};
	float array_y_ip2_o2_high[4] = {sensor->calibration_no0_o2high_ip2, nox_calculate_struct.calculate_no100_o15_ip2, nox_calculate_struct.calculate_no500_o15_ip2, nox_calculate_struct.calculate_no1500_o15_ip2};
	get_coeff(array_x_nox_o2_high, array_y_ip2_o2_high, coeff_ip2, 4, 2);
	a = coeff_ip2[2];
	b = coeff_ip2[1];
	c = coeff_ip2[0];
	result = a * 2000.f * 2000.f + b * 2000.f + c;
	nox_calculate_struct.calculate_no3000_o15_ip2 = result;
	

	float o3 = 3.f;
	float o15 = 15.f;
	// 0-100
	array_x[0] = 0;
	array_x[1] = 100;
	array_y[0] = sensor->calibration_no0_o2high_ip2;
	array_y[1] = nox_calculate_struct.calculate_no100_o3_ip2;
	linear_fitting(array_x, array_y, 2, &nox_calculate_struct.no0_100_o2low_k, &nox_calculate_struct.no0_100_o2low_b);
	array_x[0] = 0;
	array_x[1] = 100;
	array_y[0] = sensor->calibration_no0_o2high_ip2;
	array_y[1] = nox_calculate_struct.calculate_no100_o15_ip2;
	linear_fitting(array_x, array_y, 2, &nox_calculate_struct.no0_100_o2high_k, &nox_calculate_struct.no0_100_o2high_b);
	array_x[0] = o3;
	array_x[1] = o15;
	array_y[0] = nox_calculate_struct.no0_100_o2low_k;
	array_y[1] = nox_calculate_struct.no0_100_o2high_k;
	linear_fitting(array_x, array_y, 2, &nox_calculate_struct.no0_100_k_k, &nox_calculate_struct.no0_100_k_b);
	array_y[0] = nox_calculate_struct.no0_100_o2low_b;
	array_y[1] = nox_calculate_struct.no0_100_o2high_b;
	linear_fitting(array_x, array_y, 2, &nox_calculate_struct.no0_100_b_k, &nox_calculate_struct.no0_100_b_b);
	// 100-500
	array_x[0] = 100;
	array_x[1] = 500;
	array_y[0] = nox_calculate_struct.calculate_no100_o3_ip2;
	array_y[1] = nox_calculate_struct.calculate_no500_o3_ip2;
	linear_fitting(array_x, array_y, 2, &nox_calculate_struct.no100_500_o2low_k, &nox_calculate_struct.no100_500_o2low_b);
	array_x[0] = 100;
	array_x[1] = 500;
	array_y[0] = nox_calculate_struct.calculate_no100_o15_ip2;
	array_y[1] = nox_calculate_struct.calculate_no500_o15_ip2;
	linear_fitting(array_x, array_y, 2, &nox_calculate_struct.no100_500_o2high_k, &nox_calculate_struct.no100_500_o2high_b);
	array_x[0] = o3;
	array_x[1] = o15;
	array_y[0] = nox_calculate_struct.no100_500_o2low_k;
	array_y[1] = nox_calculate_struct.no100_500_o2high_k;
	linear_fitting(array_x, array_y, 2, &nox_calculate_struct.no100_500_k_k, &nox_calculate_struct.no100_500_k_b);
	array_y[0] = nox_calculate_struct.no100_500_o2low_b;
	array_y[1] = nox_calculate_struct.no100_500_o2high_b;
	linear_fitting(array_x, array_y, 2, &nox_calculate_struct.no100_500_b_k, &nox_calculate_struct.no100_500_b_b);
	// 500-1500
	array_x[0] = 500;
	array_x[1] = 1500;
	array_y[0] = nox_calculate_struct.calculate_no500_o3_ip2;
	array_y[1] = nox_calculate_struct.calculate_no1500_o3_ip2;
	linear_fitting(array_x, array_y, 2, &nox_calculate_struct.no500_1500_o2low_k, &nox_calculate_struct.no500_1500_o2low_b);
	array_x[0] = 500;
	array_x[1] = 1500;
	array_y[0] = nox_calculate_struct.calculate_no500_o15_ip2;
	array_y[1] = nox_calculate_struct.calculate_no1500_o15_ip2;
	linear_fitting(array_x, array_y, 2, &nox_calculate_struct.no500_1500_o2high_k, &nox_calculate_struct.no500_1500_o2high_b);
	array_x[0] = o3;
	array_x[1] = o15;
	array_y[0] = nox_calculate_struct.no500_1500_o2low_k;
	array_y[1] = nox_calculate_struct.no500_1500_o2high_k;
	linear_fitting(array_x, array_y, 2, &nox_calculate_struct.no500_1500_k_k, &nox_calculate_struct.no500_1500_k_b);
	array_y[0] = nox_calculate_struct.no500_1500_o2low_b;
	array_y[1] = nox_calculate_struct.no500_1500_o2high_b;
	linear_fitting(array_x, array_y, 2, &nox_calculate_struct.no500_1500_b_k, &nox_calculate_struct.no500_1500_b_b);
	// 1500-3000
	array_x[0] = 1500;
	array_x[1] = 2000;
	array_y[0] = nox_calculate_struct.calculate_no1500_o3_ip2;
	array_y[1] = nox_calculate_struct.calculate_no3000_o3_ip2;
	linear_fitting(array_x, array_y, 2, &nox_calculate_struct.no1500_3000_o2low_k, &nox_calculate_struct.no1500_3000_o2low_b);
	array_x[0] = 1500;
	array_x[1] = 2000;
	array_y[0] = nox_calculate_struct.calculate_no1500_o15_ip2;
	array_y[1] = nox_calculate_struct.calculate_no3000_o15_ip2;
	linear_fitting(array_x, array_y, 2, &nox_calculate_struct.no1500_3000_o2high_k, &nox_calculate_struct.no1500_3000_o2high_b);
	array_x[0] = o3;
	array_x[1] = o15;
	array_y[0] = nox_calculate_struct.no1500_3000_o2low_k;
	array_y[1] = nox_calculate_struct.no1500_3000_o2high_k;
	linear_fitting(array_x, array_y, 2, &nox_calculate_struct.no1500_3000_k_k, &nox_calculate_struct.no1500_3000_k_b);
	array_y[0] = nox_calculate_struct.no1500_3000_o2low_b;
	array_y[1] = nox_calculate_struct.no1500_3000_o2high_b;
	linear_fitting(array_x, array_y, 2, &nox_calculate_struct.no1500_3000_b_k, &nox_calculate_struct.no1500_3000_b_b);
	
	float array_x_o2_low[4] = { sensor->calibration_no1500_o2low_ip1, sensor->calibration_no100_o2low_ip1, sensor->calibration_no500_o2low_ip1, sensor->calibration_no1500_o2high_ip1};
	float array_y_o2_low[4] = {calibration_no1500_o2low_concentration, calibration_no100_o2low_concentration, calibration_no500_o2low_concentration, calibration_no1500_o2high_concentration};
	float coeff_low[3];
	get_coeff(array_x_o2_low, array_y_o2_low, coeff_low, 4, 2);
	nox_calculate_struct.low_o2_a = coeff_low[2];
	nox_calculate_struct.low_o2_b = coeff_low[1];
	nox_calculate_struct.low_o2_c = coeff_low[0];
	
	float array_x_o2_high[4] = {sensor->calibration_no1500_o2high_ip1, sensor->calibration_no500_o2high_ip1, sensor->calibration_no100_o2high_ip1, sensor->calibration_no0_o2high_ip1};
	float array_y_o2_high[4] = {calibration_no1500_o2high_concentration, calibration_no500_o2high_concentration, calibration_no100_o2high_concentration, calibration_no0_o2high_concentration};
	float coeff_high[3];
	get_coeff(array_x_o2_high, array_y_o2_high, coeff_high, 4, 2);
	nox_calculate_struct.high_o2_a = coeff_high[2];
	nox_calculate_struct.high_o2_b = coeff_high[1];
	nox_calculate_struct.high_o2_c = coeff_high[0];
}

#define NOx_AVG		4
uint16_t nox_output_calculate(sensor_calibration_type *sensor, sensor_data_type *sensor_data)
{
	float ip2 = sensor_data->ip2_filt;
	float k, b;
	static uint16_t sensor_nox_buffer[NOx_AVG];
	static uint8_t sensor_nox_buffer_index;
	if (ip2 <= nox_calculate_struct.calculate_no100_o15_ip2)
	{
		k = nox_calculate_struct.o2 * nox_calculate_struct.no0_100_k_k + nox_calculate_struct.no0_100_k_b;
		b = nox_calculate_struct.o2 * nox_calculate_struct.no0_100_b_k + nox_calculate_struct.no0_100_b_b;
	}
	else if (ip2 <= nox_calculate_struct.calculate_no500_o15_ip2)
	{
		k = nox_calculate_struct.o2 * nox_calculate_struct.no100_500_k_k + nox_calculate_struct.no100_500_k_b;
		b = nox_calculate_struct.o2 * nox_calculate_struct.no100_500_b_k + nox_calculate_struct.no100_500_b_b;
	}
	else if (ip2 <= nox_calculate_struct.calculate_no1500_o15_ip2)
	{
		k = nox_calculate_struct.o2 * nox_calculate_struct.no500_1500_k_k + nox_calculate_struct.no500_1500_k_b;
		b = nox_calculate_struct.o2 * nox_calculate_struct.no500_1500_b_k + nox_calculate_struct.no500_1500_b_b;
	}
	else
	{
//		k = nox_calculate_struct.o2 * nox_calculate_struct.no500_1500_k_k + nox_calculate_struct.no500_1500_k_b;
//		b = nox_calculate_struct.o2 * nox_calculate_struct.no500_1500_b_k + nox_calculate_struct.no500_1500_b_b;
		k = nox_calculate_struct.o2 * nox_calculate_struct.no1500_3000_k_k + nox_calculate_struct.no1500_3000_k_b;
		b = nox_calculate_struct.o2 * nox_calculate_struct.no1500_3000_b_k + nox_calculate_struct.no1500_3000_b_b;
	}
	float nox = (ip2 - b) /k;
//	if (nox >= 2250)
//	{
//		nox *= 1.03f;
//	}
	if (nox >= 3012)
		nox = 3012;
	if (nox <= -200)
		nox = -200;
//	if (nox >= 1300)
//	{
//		gpio_bit_write(IP2_Normal_Select_GPIO_Port, IP2_Normal_Select_Pin, SET);
//	}
//	else
//	{
//		gpio_bit_write(IP2_Normal_Select_GPIO_Port, IP2_Normal_Select_Pin, RESET);
//	}
	nox += 200;
	nox *= 20;
	if(sensor_nox_buffer_index >= NOx_AVG)
		sensor_nox_buffer_index = 0;
	sensor_nox_buffer[ sensor_nox_buffer_index++ ] = (uint16_t)nox;
	return get_average(sensor_nox_buffer, NOx_AVG);
}
#define O2_AVG		4
uint16_t o2_output_calculate(sensor_calibration_type *sensor, sensor_data_type *sensor_data)
{
	float ip0 = sensor_data->ip1_filt;
	float o2;
	static uint16_t sensor_o2_buffer[O2_AVG];
	static uint8_t sensor_o2_buffer_index;
	if (ip0 <= sensor->calibration_no1500_o2high_ip1) // O2: 13%
	{
		o2 = (nox_calculate_struct.low_o2_a) * ip0 * ip0;
		o2 += nox_calculate_struct.low_o2_b * ip0;
		o2 += nox_calculate_struct.low_o2_c;
	}
	else
	{
		o2 = (nox_calculate_struct.high_o2_a) * ip0 * ip0;
		o2 += nox_calculate_struct.high_o2_b * ip0;
		o2 += nox_calculate_struct.high_o2_c;
	}
//	nox_output_struct.AF = ((0.316f * o2 / 100) + 1) / (1 - (4.785f * o2 / 100));
//	nox_output_struct.AF *= 14.7f;
	nox_calculate_struct.o2 = o2;
	if (o2 <= -12)
		o2 = -12;
	if (o2 >= 20.6f)
		o2 = 20.6f;
	//转换成J1939
	o2 += 12;
	o2 *= 1945;	// /= 0.000514;     1000000/514=1945
	if( sensor_o2_buffer_index >= O2_AVG)
		sensor_o2_buffer_index = 0;
	sensor_o2_buffer[ sensor_o2_buffer_index++ ] = (uint16_t)o2;
	uint16_t temp = get_average( sensor_o2_buffer, O2_AVG);

	return temp;
}

void sensor_nox_o2_calculate_task(sensor_calibration_type *sensor, sensor_data_type *sensor_data, 
	can_handle_type *can_handle, system_type *system, heater_control_type *heater_control)
{
	static uint32_t now_time = 0;
	static uint32_t o2_time = 0;
	static uint32_t nox_time = 0;
	now_time = soft_timer_tick_counter_get();
	if( now_time - o2_time >= 49 ) // 1ms
	{
		o2_time = soft_timer_tick_counter_get();
		if((heater_control->mode >= HEATER_MODE_3) && 
			(can_handle->data1_signal_values_struct.status_byte_6.bit.error_nox == NO_ERROR_DATA1) && 
			(can_handle->data1_signal_values_struct.status_byte_7.bit.error_o2 == NO_ERROR_DATA1) && 
			can_handle->data1_signal_values_struct.status_byte_5.bit.error_heater == NO_ERROR_DATA1)
			sensor_data->o2_concentration = o2_output_calculate(sensor, sensor_data);
		else 
			sensor_data->o2_concentration = 23346;
	}
	if (now_time - nox_time >= 50)
	{
		nox_time = soft_timer_tick_counter_get();
		if((heater_control->mode >= HEATER_MODE_3) && 
			(can_handle->data1_signal_values_struct.status_byte_6.bit.error_nox == NO_ERROR_DATA1) && 
			(can_handle->data1_signal_values_struct.status_byte_7.bit.error_o2 == NO_ERROR_DATA1) && 
			can_handle->data1_signal_values_struct.status_byte_5.bit.error_heater == NO_ERROR_DATA1)
			sensor_data->nox_concentration = nox_output_calculate(sensor, sensor_data);
		else
			sensor_data->nox_concentration = 4000;
//		can_handle->sensor_transmit_data_flag_struct.bit.data1_cyclic_flag = 1;
	}
}
