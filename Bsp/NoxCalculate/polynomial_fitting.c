#include <stdio.h>
#include "stdlib.h"
#include "math.h"
#include "polynomial_fitting.h"
//#include <vector>
//using namespace std;

//#define MAX_EXP 2 /* x最高次幂 */
//#define MATRIX_DIM (MAX_EXP + 1)   /* 矩阵阶数 */
//#define SMPNUM 3    /* 采样个数 */

/* 采样点想， y */
//float sampleX[SMPNUM] = {2975, 3390, 3924};
//float sampleY[SMPNUM] = {16, 18, 20.6};
//float coeff[MATRIX_DIM] = {0.0};
void get_coeff(float *sampleX, float *sampleY, float *coeff, int len, int exp);   //获取系数

//矩阵方程
void get_coeff(float *sampleX, float *sampleY, float *coeff, int len, int exp)
{
    double sum;
    int i, j, k;
		int matrix_dim = exp + 1;
    float matX[matrix_dim][matrix_dim];  //公式3左矩阵
    float matY[matrix_dim][matrix_dim];  //公式3右矩阵
    float temp2[matrix_dim];
    //公式3左矩阵
    for (i = 0; i < matrix_dim; i++)
    {
        for (j = 0; j < matrix_dim; j++)
        {
            sum = 0.0;
            for (k = 0; k < len; k++)
            {
                sum += pow(sampleX[k], j + i);
            }
            matX[i][j] = sum;
        }
    }

    //打印matFunX矩阵
//    printf("matFunX矩阵：\n");
//    for (i = 0; i < MATRIX_DIM; i++)
//    {
//        for (j = 0; j < MATRIX_DIM; j++)
//        {
//            printf("%f\t", matX[i][j]);
//        }
//        printf("\n");
//    }

    //公式3右矩阵
    for (i = 0; i < matrix_dim; i++)
    {
        //temp.clear();
        sum = 0;
        for (k = 0; k < len; k++)
        {
            sum += sampleY[k] * pow(sampleX[k], i);
        }
        matY[i][0] = sum;
    }
    //printf("matFunY.size=%d\n", matFunY.size());
    //打印matFunY的矩阵
//    printf("matFunY的矩阵：\n");
//    for (i = 0; i < MATRIX_DIM; i++)
//    {
//        printf("%f\n", matY[i][0]);
//    }
    //矩阵行列式变换，将matFunX矩阵变为下三角矩阵,将matFunY矩阵做怎样的变换呢？
    //AX=Y在将X变换为下三角矩阵X'时，是左右两边同乘ratio
    double num1, num2, ratio;
    for (i = 0; i < matrix_dim; i++)
    {
        num1 = matX[i][i];
        for (j = i + 1; j < matrix_dim; j++)
        {
            num2 = matX[j][i];
            ratio = num2 / num1;
            for (k = 0; k < matrix_dim; k++)
            {
                matX[j][k] = matX[j][k] - matX[i][k] * ratio;
            }
            matY[j][0] = matY[j][0] - matY[i][0] * ratio;
        }
    }
//    //打印matFunX行列变化之后的矩阵
//    printf("matFunX行列变换之后的矩阵：\n");
//    for (i = 0; i < MATRIX_DIM; i++)
//    {
//        for (j = 0; j < MATRIX_DIM; j++)
//        {
//            printf("%f\t", matX[i][j]);
//        }
//        printf("\n");
//    }
    //打印matFunY行列变换之后的矩阵
//    printf("matFunY行列变换之后的矩阵：\n");
//    for (i = 0; i < MATRIX_DIM; i++)
//    {
//        printf("%f\n", matY[i][0]);
//    }
    //计算拟合曲线的系数
    //doubleVector coeff(n + 1, 0);
    for (i = matrix_dim - 1; i >= 0; i--)
    {
        if (i == matrix_dim - 1)
        {
            coeff[i] = matY[i][0] / matX[i][i];
        }
        else
        {
            for (j = i + 1; j < matrix_dim; j++)
            {
                matY[i][0] = matY[i][0] - coeff[j] * matX[i][j];
            }
            coeff[i] = matY[i][0] / matX[i][i];
        }
    }
    return;
}
