#include "linear_fitting.h"


uint8_t linear_fitting(float *arrayX, float *arrayY, uint8_t len, float *pRetFactor, float *pRetConstant)
{
//	if(arrayX == NULL || arrayX == NULL || len < 2) 
//		return  0;

	float x, y;
	float sum_x = 0.0f , sum_y = 0.0f , xySum = 0.0f, x2sum = 0.0f;
 
	for(int i = 0 ; i < len ; i++)
	{ 
		x=arrayX[i];
		y=arrayY[i];
		sum_x += x;
		sum_y += y;
		xySum += x*y;
		x2sum += x*x;
	}

	*pRetFactor=(sum_x*sum_y/len-xySum)/(sum_x*sum_x/len-x2sum);

	*pRetConstant=(sum_y-(*pRetFactor)*sum_x)/len;
	return 1;
}
