#ifndef NOX_CALCULATE_H_
#define NOX_CALCULATE_H_
#include "main.h"
#include "can_control.h"
#include "system_state.h"
#include "system_control.h"
#include "linear_fitting.h"
#include "polynomial_fitting.h"
#include "soft_timer.h"
#include "can_command.h"
#include "calibration.h"
uint16_t nox_output_calculate(sensor_calibration_type *sensor, sensor_data_type *sensor_data);
uint16_t o2_output_calculate(sensor_calibration_type *sensor, sensor_data_type *sensor_data);
void calculate_equation(sensor_calibration_type *sensor);
void sensor_nox_o2_calculate_task(sensor_calibration_type *sensor, sensor_data_type *sensor_data, 
	can_handle_type *can_handle, system_type *system, heater_control_type *heater_control);
#endif
