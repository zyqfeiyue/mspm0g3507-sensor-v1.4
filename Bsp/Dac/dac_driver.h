/* USER CODE END Header */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DAC_DRIVER_H__
#define __DAC_DRIVER_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "ti/driverlib/dl_dac12.h"

void dac_driver_init(void);
void ip1p_dac_set_value(float voltage_mv);

#endif
