#include "dac_driver.h"

void dac_driver_init(void)
{
	DL_DAC12_startCalibration(DAC0);
}

void ip1p_dac_set_value(float voltage_mv)
{
	if (voltage_mv >= 3300)
	{
		voltage_mv = 3300;
	}
	else if (voltage_mv <= 0)
	{
		voltage_mv = 0;
	}
	float dac_value = voltage_mv / 3300 * 4095;
	DL_DAC12_output12(DAC0, dac_value);
//	DL_DAC12_enable(DAC0);
}
