/* USER CODE END Header */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __ADC_APP_H__
#define __ADC_APP_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc_driver.h"

typedef struct
{
	float ip1_data;
	float vip1_data;
	float ip1_s_data;
	float vp1_data;
	float ip2_data;
	float vip2_data;
	float vp2_data;
	float ip2n_data;
	float com_data;
	float vs_s_f_data;
	float vs_s_f_inject_data;
	float i_rpvs_data;
	float i_rpvs_neg_inject_data;
	float i_rpvs_pos_inject_data;	
	float vs_data;
	float vs_inject_data;
	float i_rpvs_neg_data;
	float i_rpvs_pos_data;
	float rpvs_origin_data;
	float rpvs_amp_data;
	float power_input_data;
	float power_5v_data;
	float heater_res_data;
	float heater_res_inject_data;
	float heater_res_normal_data;
	float heater_current_data;
	float heater_current_normal_data;
	float heater_current_inject_data;
	float heater_power_w;
	float heater_pwm_duty;
	
	float ip1_filt;
	float ip2_filt;
	float vp1_filt;
	float vp2_filt;
	float com_filt;
	float vs_filt;
	float rpvs_filt;
	float res_filt;
	float heater_power_filt;
	
	uint16_t nox_concentration;        //氮气浓度
	uint16_t o2_concentration;        //氧气浓度
} sensor_data_type;

typedef struct
{
	float mcu_ref_power; // mcu参考电源
	float mcu_adc_accuracy; // mcu adc精度
	float mcu_dac_accuracy; // mcu dac精度
	float adc_lsb;
	float dac_lsb;
	// IP1_S 测试电路
	float ip1_s_circuit_div_res; // 分压电阻值
	float ip1_s_circuit_smp_res; // 采样电阻值
	float ip1_s_partial_ratio; // 分压比例
	// VIP1 测试电路
	float vip1_offset_power; // 偏置电源
	float vip1_offset_div_res; // 分压电阻值
	float vip1_offset_smp_res; // 采样电阻值
	float vip1_feedback_res; // 反馈电阻
	float vip1_input_res; // 输入电阻
	float vip1_offset_voltage; // 实际偏置电压
	float vip1_amp_ratio; // 运放放大比例
	float ip1_smp_res;
	
	// IP1P 电流源电路
	float ip1p_dac_div_res;
	float ip1p_dac_smp_res;
	float ip1p_current_src_feedback_res;  // 反馈电阻
	float ip1p_current_src_input_res; // 反馈电阻
	float ip1p_current_src_smp_res; // 采样电阻值
	float ip1p_current_src_offset_power; // 偏置电源
	
	// i_rpvs 测试电路
	float i_rpvs_offset_power; // 偏置电源
	float i_rpvs_offset_div_res; // 分压电阻值
	float i_rpvs_offset_smp_res; // 采样电阻值
	float i_rpvs_feedback_res; // 反馈电阻
	float i_rpvs_input_res; // 输入电阻
	float i_rpvs_smp_res;
	float i_rpvs_offset_voltage; // 实际偏置电压
	float i_rpvs_amp_ratio; // 运放放大比例
	
	// vs 测试电路
	float vs_feedback_res; // 反馈电阻
	float vs_input_res; // 输入电阻
	float vs_amp_ratio; // 运放放大比例
	
	// vs_s_f 测试电路
	float vs_s_f_circuit_div_res;
	float vs_s_f_circuit_smp_res;
	float vs_s_f_partial_ratio; // 分压比例
	
	// VIP1 测试电路
	float vip2_offset_power; // 偏置电源
	float vip2_offset_div_res; // 分压电阻值
	float vip2_offset_smp_res; // 采样电阻值
	float vip2_feedback_res; // 反馈电阻
	float vip2_input_res; // 输入电阻
	float vip2_offset_voltage; // 实际偏置电压
	float vip2_amp_ratio; // 运放放大比例
	float ip2_smp_res;
	
	// ip2p dac
	float ip2p_dac_div_res;
	float ip2p_dac_smp_res;
	
	// 输入电源测试
	float power_input_div_res;
	float power_input_smp_res;
	float power_input_partial_ratio; // 分压比例
} sensor_character_para_type;

typedef struct
{
	//0xFx:不影响加热，0xxF:影响加热
	/*
	0x80 电源电压大于38v，通讯，不加热，报警
	0x40 电源电压小于7v，不通讯，不加热，报警
	0x20 电源电压大于7v，小于10v, 加热状态下可继续加热，否则不加热,通讯
	0x10 电源电压大于32v，小于38v, 加热状态下可继续加热，否则不加热,通讯
	0x08 地址帧地址冲突
	0x04 其它帧地址冲突
	0x02
	0x01
	*/
	uint8_t hardware_error;               //硬件错误标记  --  0x04：地址冲突--  0x08：地址冲突

	uint8_t power_error_l_counter;
	uint8_t power_error_ll_counter;
	uint8_t power_error_h_counter;
	uint8_t power_error_hh_counter;
	int16_t write_eeprom_flag;           //写标志
	
	uint16_t dc_bus_voltage;
	uint8_t dc_bus_less_voltage_flag;             //母线电压低于LessShutdown值
  uint8_t dc_bus_over_voltage_flag;             //母线电压高于OverShutdown值
	uint8_t heater_enable_flag; //相关步骤标志
	uint32_t heater_timeout_count;     // 接收露点命令后加热300S后停止，向下计数，减为零即时间到
	
	uint32_t life_hours;          // 运行时间，只用来计算最后结果
	uint32_t life_seconds;        // 运行时间/S
	
	uint8_t opera_compensated_factor;  // 老化补偿系数
} sensor_hardware_type;

typedef struct
{
	sensor_data_type sensor_data_struct;
	sensor_character_para_type sensor_character_para_struct;
	sensor_hardware_type sensor_hardware_struct;
} sensor_handle_type;


extern sensor_handle_type sensor_handle_struct;
void sensor_character_para_init(sensor_character_para_type *sensor_character_para);
void get_sensor_data(sensor_handle_type *sensor_handle, adc_handle_type *adc_handle);
#endif
