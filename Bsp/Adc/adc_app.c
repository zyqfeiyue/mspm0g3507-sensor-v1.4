#include "adc_app.h"
#include "math.h"
#include "tools.h"
exponential_average_type exponential_heater_power = {0};
exponential_average_type exponential_ip1 = {0};
exponential_average_type exponential_ip2 = {0};
exponential_average_type exponential_vp1 = {0};
exponential_average_type exponential_vp2 = {0};
exponential_average_type exponential_vs = {0};
exponential_average_type exponential_com = {0};
exponential_average_type exponential_rpvs = {0};
exponential_average_type exponential_res = {0};
sensor_handle_type sensor_handle_struct;

void sensor_character_para_init(sensor_character_para_type *sensor_character_para)
{
	sensor_character_para->mcu_ref_power = 3300;
	sensor_character_para->mcu_dac_accuracy = 4096;
	sensor_character_para->mcu_adc_accuracy = 4096;
	sensor_character_para->adc_lsb = sensor_character_para->mcu_ref_power / sensor_character_para->mcu_adc_accuracy;
	sensor_character_para->dac_lsb = sensor_character_para->mcu_ref_power / sensor_character_para->mcu_dac_accuracy;
	sensor_character_para->ip1_s_circuit_div_res = 68.f; // unit:K次
	sensor_character_para->ip1_s_circuit_smp_res = 68.f; // unit:K次
	sensor_character_para->ip1_s_partial_ratio = ((sensor_character_para->ip1_s_circuit_smp_res + sensor_character_para->ip1_s_circuit_div_res) / \
		sensor_character_para->ip1_s_circuit_smp_res);
	
	sensor_character_para->vip1_offset_power = 3300;
	sensor_character_para->vip1_offset_div_res = 374; // unit:K次
	sensor_character_para->vip1_offset_smp_res = 187; // unit:K次
	sensor_character_para->vip1_feedback_res = 124.7; // unit:K次
	sensor_character_para->vip1_input_res = 68; // unit:K次
	sensor_character_para->ip1_smp_res = 200;
	sensor_character_para->vip1_offset_voltage = sensor_character_para->vip1_offset_power * \
		 (sensor_character_para->vip1_offset_smp_res / \
		(sensor_character_para->vip1_offset_smp_res + sensor_character_para->vip1_offset_div_res));
	sensor_character_para->vip1_amp_ratio = (sensor_character_para->vip1_input_res / sensor_character_para->vip1_feedback_res);

	sensor_character_para->ip1p_dac_div_res = 20; // unit:K次
	sensor_character_para->ip1p_dac_smp_res = 30.1; // unit:K次
	sensor_character_para->ip1p_current_src_feedback_res = 5.9f + 0.1f; // unit:K次
	sensor_character_para->ip1p_current_src_input_res = 32.4; // unit:K次
	sensor_character_para->ip1p_current_src_smp_res = 100;
	sensor_character_para->ip1p_current_src_offset_power = 3.3;
	
	sensor_character_para->i_rpvs_offset_power = 3300;
	sensor_character_para->i_rpvs_offset_div_res = 210; // unit:K次
	sensor_character_para->i_rpvs_offset_smp_res = 210; // unit:K次
	sensor_character_para->i_rpvs_feedback_res = 105; // unit:K次
	sensor_character_para->i_rpvs_input_res = 32.4; // unit:K次
	sensor_character_para->i_rpvs_smp_res = 300;
	// calculate
	sensor_character_para->i_rpvs_offset_voltage = sensor_character_para->i_rpvs_offset_power * \
		 (sensor_character_para->i_rpvs_offset_smp_res / \
		(sensor_character_para->i_rpvs_offset_smp_res + sensor_character_para->i_rpvs_offset_div_res));
	sensor_character_para->i_rpvs_amp_ratio = (sensor_character_para->i_rpvs_input_res / sensor_character_para->i_rpvs_feedback_res);
	
	sensor_character_para->vs_feedback_res = 240; // unit:K次
	sensor_character_para->vs_input_res = 40.2f;
	sensor_character_para->vs_amp_ratio = (sensor_character_para->vs_input_res / sensor_character_para->vs_feedback_res);
	
	sensor_character_para->vs_s_f_circuit_div_res = 5.1; // unit:K次
	sensor_character_para->vs_s_f_circuit_smp_res = 10;
	sensor_character_para->vs_s_f_partial_ratio = ((sensor_character_para->vs_s_f_circuit_smp_res + sensor_character_para->vs_s_f_circuit_div_res) / \
		sensor_character_para->vs_s_f_circuit_smp_res);
	
	sensor_character_para->vip2_offset_power = 2500;
	sensor_character_para->vip2_offset_div_res = 6800; // unit:K次
	sensor_character_para->vip2_offset_smp_res = 750; // unit:K次
	sensor_character_para->vip2_feedback_res = 675.5; // unit:K次
	sensor_character_para->vip2_input_res = 422; // unit:K次
	sensor_character_para->vip2_offset_voltage = sensor_character_para->vip2_offset_power * \
		 (sensor_character_para->vip2_offset_smp_res / \
		(sensor_character_para->vip2_offset_smp_res + sensor_character_para->vip2_offset_div_res));
	sensor_character_para->vip2_amp_ratio = (sensor_character_para->vip2_input_res / sensor_character_para->vip2_feedback_res);
	sensor_character_para->ip2_smp_res = 169;
	
	sensor_character_para->ip2p_dac_div_res = 20; // unit:K次
	sensor_character_para->ip2p_dac_smp_res = 30.1;
	
	sensor_character_para->power_input_div_res = 22; // unit:K次
	sensor_character_para->power_input_smp_res = 2.7;
	sensor_character_para->power_input_partial_ratio = ((sensor_character_para->power_input_div_res + sensor_character_para->power_input_smp_res) / \
		sensor_character_para->power_input_smp_res);
	exponential_heater_power.decay = 0.98f;
	exponential_ip1.decay = 0.75f;
	exponential_ip2.decay = 0.95f;
	exponential_vp1.decay = 0.98f;
	exponential_vp2.decay = 0.98f;
	exponential_vs.decay = 0.98f;
	exponential_com.decay = 0.98f;
	exponential_rpvs.decay = 0.98f;
	exponential_res.decay = 0.98f;
}

float i_rpvs_convert_data_current(sensor_handle_type *sensor_handle, uint16_t adc_value)
{
	sensor_character_para_type *sensor_character_para = &sensor_handle->sensor_character_para_struct;
	float voltage = (float)sensor_character_para->adc_lsb * adc_value;
	float i_rpvs_voltage = (voltage - sensor_character_para->i_rpvs_offset_voltage) * sensor_character_para->i_rpvs_amp_ratio;
	float i_rpvs_ma = i_rpvs_voltage / sensor_character_para->i_rpvs_smp_res;
	return i_rpvs_ma;
}

float vs_convert_data_voltage(sensor_handle_type *sensor_handle, uint16_t adc_value)
{
	sensor_character_para_type *sensor_character_para = &sensor_handle->sensor_character_para_struct;
	float voltage = (float)sensor_character_para->adc_lsb * adc_value;
	float vs_mv = voltage * sensor_character_para->vs_amp_ratio;
	return vs_mv;
}

float com_convert_data_voltage(sensor_handle_type *sensor_handle, uint16_t adc_value)
{
	sensor_character_para_type *sensor_character_para = &sensor_handle->sensor_character_para_struct;
	float voltage = (float)sensor_character_para->adc_lsb * adc_value;
	return voltage;
}

float ip2n_convert_data_voltage(sensor_handle_type *sensor_handle, uint16_t adc_value)
{
	sensor_character_para_type *sensor_character_para = &sensor_handle->sensor_character_para_struct;
	float voltage = (float)sensor_character_para->adc_lsb * adc_value;
	return voltage;
}

float vs_s_f_convert_data_voltage(sensor_handle_type *sensor_handle, uint16_t adc_value)
{
	sensor_character_para_type *sensor_character_para = &sensor_handle->sensor_character_para_struct;
	sensor_data_type *sensor_data = &sensor_handle->sensor_data_struct;
	float voltage = (float)sensor_character_para->adc_lsb * adc_value;
	float vs_s_f_mv = voltage * sensor_character_para->vs_s_f_partial_ratio;
	return (vs_s_f_mv - sensor_data->com_data);
}

float ip1_s_convert_data_voltage(sensor_handle_type *sensor_handle, uint16_t adc_value)
{
	sensor_character_para_type *sensor_character_para = &sensor_handle->sensor_character_para_struct;
	float voltage = (float)sensor_character_para->adc_lsb * adc_value;
	float ip1_s_mv = voltage * sensor_character_para->ip1_s_partial_ratio;
	return ip1_s_mv;
}

float vip1_convert_data_voltage(sensor_handle_type *sensor_handle, uint16_t adc_value)
{
	sensor_character_para_type *sensor_character_para = &sensor_handle->sensor_character_para_struct;
	float voltage = (float)sensor_character_para->adc_lsb * adc_value;
	float vip1_mv = (voltage - sensor_character_para->vip1_offset_voltage) * sensor_character_para->vip1_amp_ratio;
	return vip1_mv;
}

float ip1_convert_data_current(sensor_handle_type *sensor_handle)
{
	sensor_character_para_type *sensor_character_para = &sensor_handle->sensor_character_para_struct;
	sensor_data_type *sensor_data = &sensor_handle->sensor_data_struct;
	float ip1_ua = sensor_data->vip1_data / sensor_character_para->ip1_smp_res * 1000.f;
	return ip1_ua;
}
float vip2_convert_data_voltage(sensor_handle_type *sensor_handle, uint16_t adc_value)
{
	sensor_character_para_type *sensor_character_para = &sensor_handle->sensor_character_para_struct;
	float voltage = (float)sensor_character_para->adc_lsb * adc_value;
	float vip2_mv = (voltage - sensor_character_para->vip2_offset_voltage) * sensor_character_para->vip2_amp_ratio;
	return vip2_mv;
}
float ip2_convert_data_current(sensor_handle_type *sensor_handle)
{
	sensor_character_para_type *sensor_character_para = &sensor_handle->sensor_character_para_struct;
	sensor_data_type *sensor_data = &sensor_handle->sensor_data_struct;
	float ip2_na = sensor_data->vip2_data / sensor_character_para->ip2_smp_res * 1000.f;
	return ip2_na;
}
float power_input_convert_data_voltage(sensor_handle_type *sensor_handle, uint16_t adc_value)
{
	sensor_character_para_type *sensor_character_para = &sensor_handle->sensor_character_para_struct;
	float voltage = (float)sensor_character_para->adc_lsb * adc_value;
	float power_input_mv = voltage * sensor_character_para->power_input_partial_ratio;
	return power_input_mv;
}

void get_sensor_data(sensor_handle_type *sensor_handle, adc_handle_type *adc_handle)
{
	sensor_data_type *sensor_data = &sensor_handle->sensor_data_struct;
	if (adc_handle->adc0_inject_vs_s_f_flag)
	{
		adc_handle->adc0_inject_vs_s_f_flag = 0;
		sensor_data->vs_s_f_inject_data = vs_s_f_convert_data_voltage(sensor_handle, adc_handle->adc0_inject_vs_s_f_data);
		sensor_data->rpvs_origin_data = (sensor_data->vs_s_f_data - sensor_data->vs_s_f_inject_data) / fabs(sensor_data->i_rpvs_neg_data);
		sensor_data->rpvs_filt = exponential_weighted_float(&exponential_rpvs, sensor_data->rpvs_origin_data);
	}
	if (adc_handle->adc1_inject_i_rpvs_neg_flag)
	{
		adc_handle->adc1_inject_i_rpvs_neg_flag = 0;
		sensor_data->i_rpvs_neg_inject_data = i_rpvs_convert_data_current(sensor_handle, adc_handle->adc1_inject_i_rpvs_neg_data);
		sensor_data->i_rpvs_neg_data = sensor_data->i_rpvs_neg_inject_data - sensor_data->i_rpvs_data;
	}
	if (adc_handle->adc1_inject_i_rpvs_pos_flag)
	{
		adc_handle->adc1_inject_i_rpvs_pos_flag = 0;
		sensor_data->i_rpvs_pos_inject_data = i_rpvs_convert_data_current(sensor_handle, adc_handle->adc1_inject_i_rpvs_pos_data);
		sensor_data->i_rpvs_pos_data = sensor_data->i_rpvs_pos_inject_data - sensor_data->i_rpvs_data;
	}
	if (adc_handle->adc1_inject_vs_flag)
	{
		adc_handle->adc1_inject_vs_flag = 0;
		sensor_data->vs_inject_data = vs_convert_data_voltage(sensor_handle, adc_handle->adc1_inject_vs_data);
		sensor_data->rpvs_amp_data = (sensor_data->vs_data - sensor_data->vs_inject_data) / fabs(sensor_data->i_rpvs_neg_data);
	}

	sensor_data->com_data = com_convert_data_voltage(sensor_handle, adc_handle->adc1_dma_data_struct.bits.adc_com);
	sensor_data->vip1_data = vip1_convert_data_voltage(sensor_handle, adc_handle->adc0_dma_data_struct.bits.adc_vip1);
	sensor_data->ip1_data = ip1_convert_data_current(sensor_handle);
	sensor_data->vs_s_f_data = vs_s_f_convert_data_voltage(sensor_handle, adc_handle->adc0_dma_data_struct.bits.adc_vs_s_f);
	sensor_data->vip2_data = vip2_convert_data_voltage(sensor_handle, adc_handle->adc0_dma_data_struct.bits.adc_vip2);
	sensor_data->ip2n_data = ip2n_convert_data_voltage(sensor_handle, adc_handle->adc0_dma_data_struct.bits.adc_ip2n);
	sensor_data->ip2_data = ip2_convert_data_current(sensor_handle);
	sensor_data->power_input_data = power_input_convert_data_voltage(sensor_handle, adc_handle->adc0_dma_data_struct.bits.adc_input_power);
	sensor_data->i_rpvs_data = i_rpvs_convert_data_current(sensor_handle, adc_handle->adc1_dma_data_struct.bits.adc_i_rpvs);
	sensor_data->vs_data = vs_convert_data_voltage(sensor_handle, adc_handle->adc1_dma_data_struct.bits.adc_vs);
	sensor_data->ip1_s_data = ip1_s_convert_data_voltage(sensor_handle, adc_handle->adc0_dma_data_struct.bits.adc_ip1n_s);
	
	sensor_data->vp1_data = sensor_data->ip1_s_data - sensor_data->com_data;
	sensor_data->vp2_data = sensor_data->ip2n_data - sensor_data->com_data;
	
	sensor_data->ip1_filt = exponential_weighted_float(&exponential_ip1, sensor_data->ip1_data);
	sensor_data->ip2_filt = exponential_weighted_float(&exponential_ip2, sensor_data->ip2_data);
	sensor_data->vp1_filt = exponential_weighted_float(&exponential_vp1, sensor_data->vp1_data);
	sensor_data->vp2_filt = exponential_weighted_float(&exponential_vp2, sensor_data->vp2_data);
	sensor_data->vs_filt = exponential_weighted_float(&exponential_vs, sensor_data->vs_data);
	sensor_data->com_filt = exponential_weighted_float(&exponential_com, sensor_data->com_data);
}
