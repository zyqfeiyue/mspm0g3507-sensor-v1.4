#include "adc_driver.h"
#include "ti/devices/msp/m0p/mspm0g350x.h"
#include "ti/driverlib/dl_adc12.h"

adc_handle_type adc_handle_struct;

void adc_driver_init(void)
{
	NVIC_EnableIRQ(ADC12_0_INST_INT_IRQN);
	NVIC_EnableIRQ(ADC12_1_INST_INT_IRQN);
}
void adc_task_handle(adc_handle_type *adc_handle)
{
	if (adc_handle->adc0_dma_flag)
	{
		adc_handle->adc0_dma_flag = 0;
    /* Configure DMA source, destination and size */
		DL_DMA_disableChannel(DMA, DMA_CH0_CHAN_ID);
		DL_DMA_setSrcAddr(DMA, DMA_CH0_CHAN_ID, (uint32_t) &(ADC0->ULLMEM.MEMRES[0]));
		DL_DMA_setDestAddr(DMA, DMA_CH0_CHAN_ID, (uint32_t)(&adc_handle->adc0_dma_data_struct.buffer));
		DL_DMA_setTransferSize(DMA, DMA_CH0_CHAN_ID, 6);
		DL_ADC12_setStartAddress(ADC0, DL_ADC12_SEQ_START_ADDR_00);
		DL_ADC12_setEndAddress(ADC0, DL_ADC12_SEQ_END_ADDR_05);
		DL_ADC12_setDMASamplesCnt(ADC12_0_INST, 6);
		DL_ADC12_disableDMATrigger(ADC12_0_INST,(DL_ADC12_DMA_MEM1_RESULT_LOADED));
		DL_ADC12_enableDMATrigger(ADC12_0_INST,(DL_ADC12_DMA_MEM5_RESULT_LOADED));
		DL_DMA_enableChannel(DMA, DMA_CH0_CHAN_ID);
		DL_ADC12_enableConversions(ADC0);
		DL_ADC12_startConversion(ADC0);
	}
	if (adc_handle->adc1_dma_flag)
	{
		adc_handle->adc1_dma_flag = 0;
		DL_DMA_disableChannel(DMA, DMA_CH1_CHAN_ID);
		DL_DMA_setSrcAddr(DMA, DMA_CH1_CHAN_ID, (uint32_t) &(ADC12_1_INST->ULLMEM.MEMRES[0]));
		DL_DMA_setDestAddr(DMA, DMA_CH1_CHAN_ID, (uint32_t)(&adc_handle->adc1_dma_data_struct.buffer));
		DL_DMA_setTransferSize(DMA, DMA_CH1_CHAN_ID, 3);
		DL_ADC12_setStartAddress(ADC12_1_INST, DL_ADC12_SEQ_START_ADDR_00);
		DL_ADC12_setEndAddress(ADC12_1_INST, DL_ADC12_SEQ_END_ADDR_02);
		DL_ADC12_setDMASamplesCnt(ADC12_1_INST, 3);
		DL_ADC12_disableDMATrigger(ADC12_1_INST,(DL_ADC12_DMA_MEM0_RESULT_LOADED));
		DL_ADC12_disableDMATrigger(ADC12_1_INST,(DL_ADC12_DMA_MEM1_RESULT_LOADED));
		DL_ADC12_enableDMATrigger(ADC12_1_INST,(DL_ADC12_DMA_MEM2_RESULT_LOADED));
		DL_DMA_enableChannel(DMA, DMA_CH1_CHAN_ID);
		DL_ADC12_enableConversions(ADC12_1_INST);
		DL_ADC12_startConversion(ADC12_1_INST);
	}
	if (adc_handle->vs_s_f_inject_flag)
	{
		adc_handle->vs_s_f_inject_flag = 0;
		DL_DMA_disableChannel(DMA, DMA_CH0_CHAN_ID);
		DL_DMA_setSrcAddr(DMA, DMA_CH0_CHAN_ID, (uint32_t) &(ADC0->ULLMEM.MEMRES[1]));
		DL_DMA_setDestAddr(DMA, DMA_CH0_CHAN_ID, (uint32_t)(&adc_handle->adc0_buffer));
		DL_DMA_setTransferSize(DMA, DMA_CH0_CHAN_ID, 1);
		DL_ADC12_setStartAddress(ADC0, DL_ADC12_SEQ_START_ADDR_01);
		DL_ADC12_setEndAddress(ADC0, DL_ADC12_SEQ_END_ADDR_01);
		DL_ADC12_setDMASamplesCnt(ADC12_0_INST,1);
		DL_ADC12_disableDMATrigger(ADC12_0_INST, (DL_ADC12_DMA_MEM5_RESULT_LOADED));
		DL_ADC12_enableDMATrigger(ADC12_0_INST, (DL_ADC12_DMA_MEM1_RESULT_LOADED));
		DL_DMA_enableChannel(DMA, DMA_CH0_CHAN_ID);
		DL_ADC12_clearInterruptStatus(ADC12_0_INST, (DL_ADC12_INTERRUPT_MEM1_RESULT_LOADED));
		DL_ADC12_enableInterrupt(ADC12_0_INST, (DL_ADC12_INTERRUPT_MEM1_RESULT_LOADED));
		DL_ADC12_enableConversions(ADC0);
		DL_ADC12_startConversion(ADC0);
	}
	if (adc_handle->vs_inject_flag)
	{
		adc_handle->vs_inject_flag = 0;
		adc_handle->vs_irpvs_convert_flag = 1;
		DL_DMA_disableChannel(DMA, DMA_CH1_CHAN_ID);
		DL_DMA_setSrcAddr(DMA, DMA_CH1_CHAN_ID, (uint32_t) &(ADC1->ULLMEM.MEMRES[0]));
		DL_DMA_setDestAddr(DMA, DMA_CH1_CHAN_ID, (uint32_t)(&adc_handle->adc1_buffer));
		DL_DMA_setTransferSize(DMA, DMA_CH1_CHAN_ID, 1);
		DL_ADC12_setStartAddress(ADC12_1_INST, DL_ADC12_SEQ_START_ADDR_00);
		DL_ADC12_setEndAddress(ADC12_1_INST, DL_ADC12_SEQ_END_ADDR_00);
		DL_ADC12_setDMASamplesCnt(ADC12_1_INST, 1);
		DL_ADC12_disableDMATrigger(ADC12_1_INST, (DL_ADC12_DMA_MEM1_RESULT_LOADED));
		DL_ADC12_disableDMATrigger(ADC12_1_INST, (DL_ADC12_DMA_MEM2_RESULT_LOADED));
		DL_ADC12_enableDMATrigger(ADC12_1_INST, (DL_ADC12_DMA_MEM0_RESULT_LOADED));
		DL_DMA_enableChannel(DMA, DMA_CH1_CHAN_ID);
		DL_ADC12_clearInterruptStatus(ADC12_1_INST, (DL_ADC12_INTERRUPT_MEM0_RESULT_LOADED));
		DL_ADC12_enableInterrupt(ADC12_1_INST, (DL_ADC12_INTERRUPT_MEM0_RESULT_LOADED));
		DL_ADC12_enableConversions(ADC12_1_INST);
		DL_ADC12_startConversion(ADC12_1_INST);
	}
	if (adc_handle->i_rpvs_inject_flag)
	{
		adc_handle->i_rpvs_inject_flag = 0;
		adc_handle->vs_irpvs_convert_flag = 0;
		DL_DMA_disableChannel(DMA, DMA_CH1_CHAN_ID);
		DL_DMA_setSrcAddr(DMA, DMA_CH1_CHAN_ID, (uint32_t) &(ADC1->ULLMEM.MEMRES[1]));
		DL_DMA_setDestAddr(DMA, DMA_CH1_CHAN_ID, (uint32_t)(&adc_handle->adc1_buffer));
		DL_DMA_setTransferSize(DMA, DMA_CH1_CHAN_ID, 1);
		DL_ADC12_setStartAddress(ADC12_1_INST, DL_ADC12_SEQ_START_ADDR_01);
		DL_ADC12_setEndAddress(ADC12_1_INST, DL_ADC12_SEQ_END_ADDR_01);
		DL_ADC12_setDMASamplesCnt(ADC12_1_INST, 1);
		DL_ADC12_disableDMATrigger(ADC12_1_INST, (DL_ADC12_DMA_MEM0_RESULT_LOADED));
		DL_ADC12_disableDMATrigger(ADC12_1_INST, (DL_ADC12_DMA_MEM2_RESULT_LOADED));
		DL_ADC12_enableDMATrigger(ADC12_1_INST, (DL_ADC12_DMA_MEM1_RESULT_LOADED));
		DL_DMA_enableChannel(DMA, DMA_CH1_CHAN_ID);
		DL_ADC12_clearInterruptStatus(ADC12_1_INST, (DL_ADC12_INTERRUPT_MEM1_RESULT_LOADED));
		DL_ADC12_enableInterrupt(ADC12_1_INST, (DL_ADC12_INTERRUPT_MEM1_RESULT_LOADED));
		DL_ADC12_enableConversions(ADC12_1_INST);
		DL_ADC12_startConversion(ADC12_1_INST);
	}
}

void get_adc_inject_vs_s_f_data(adc_handle_type *adc_handle, uint16_t data)
{
	adc_handle->adc0_inject_vs_s_f_data = data;
	adc_handle->adc0_inject_vs_s_f_flag = 1;
}

void get_adc_inject_vs_irpvs_data(adc_handle_type *adc_handle, uint16_t data)
{
	if (adc_handle->vs_irpvs_convert_flag)
	{
		adc_handle->adc1_inject_vs_data = data;
		adc_handle->adc1_inject_vs_flag = 1;
	}
	else
	{
		if (adc_handle->i_rpvs_neg_pos_flag)
		{
			adc_handle->adc1_inject_i_rpvs_neg_data = data;
			adc_handle->adc1_inject_i_rpvs_neg_flag = 1;
		}
		else
		{
			adc_handle->adc1_inject_i_rpvs_pos_data = data;
			adc_handle->adc1_inject_i_rpvs_pos_flag = 1;
		}
	}
}

void ADC12_0_INST_IRQHandler(void)
{
	switch (DL_ADC12_getPendingInterrupt(ADC12_0_INST)) {
			case DL_ADC12_IIDX_MEM1_RESULT_LOADED:
				get_adc_inject_vs_s_f_data(&adc_handle_struct, adc_handle_struct.adc0_buffer);
				DL_ADC12_clearInterruptStatus(ADC12_0_INST,(DL_ADC12_INTERRUPT_MEM1_RESULT_LOADED));
				DL_ADC12_disableInterrupt(ADC12_0_INST,(DL_ADC12_INTERRUPT_MEM1_RESULT_LOADED));
					break;
			case DL_ADC12_IIDX_DMA_DONE:
				break;
			default:
					break;
	}
}
void ADC12_1_INST_IRQHandler(void)
{
    switch (DL_ADC12_getPendingInterrupt(ADC12_1_INST)) {
        case DL_ADC12_IIDX_MEM0_RESULT_LOADED:
            get_adc_inject_vs_irpvs_data(&adc_handle_struct, adc_handle_struct.adc1_buffer);
            DL_ADC12_clearInterruptStatus(ADC12_1_INST,(DL_ADC12_INTERRUPT_MEM0_RESULT_LOADED));
            DL_ADC12_disableInterrupt(ADC12_1_INST,(DL_ADC12_INTERRUPT_MEM0_RESULT_LOADED));
            break;
        case DL_ADC12_IIDX_MEM1_RESULT_LOADED:
            get_adc_inject_vs_irpvs_data(&adc_handle_struct, adc_handle_struct.adc1_buffer);
            DL_ADC12_clearInterruptStatus(ADC12_1_INST,(DL_ADC12_INTERRUPT_MEM1_RESULT_LOADED));
            DL_ADC12_disableInterrupt(ADC12_1_INST,(DL_ADC12_INTERRUPT_MEM1_RESULT_LOADED));
            break;
        default:
            break;
    }
}