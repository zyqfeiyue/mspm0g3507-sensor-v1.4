#ifndef adc_driver_h
#define adc_driver_h
#include "main.h"

typedef union
{
	uint32_t buffer[6];
	struct
	{
		uint32_t adc_vip1;
		uint32_t adc_vs_s_f;
		uint32_t adc_vip2;
		uint32_t adc_ip2n;
		uint32_t adc_ip1n_s;
    uint32_t adc_input_power;
	} bits;
} adc0_dma_data_type;

typedef union
{
	uint32_t buffer[3];
	struct
	{
		uint32_t adc_vs;
		uint32_t adc_i_rpvs;
		uint32_t adc_com;
	} bits;
} adc1_dma_data_type;

typedef struct
{
	adc0_dma_data_type adc0_dma_data_struct;
	adc1_dma_data_type adc1_dma_data_struct;
    uint32_t adc0_buffer;
    uint32_t adc1_buffer;
	uint16_t adc0_inject_vs_s_f_data;
	uint16_t adc1_inject_i_rpvs_neg_data;
	uint16_t adc1_inject_i_rpvs_pos_data;
	uint16_t adc1_inject_vs_data;
	uint8_t adc0_dma_flag;
	uint8_t adc1_dma_flag;
	uint8_t vs_s_f_inject_flag;
	uint8_t vs_inject_flag;
	uint8_t i_rpvs_inject_flag;
	uint8_t i_rpvs_neg_pos_flag;
	uint8_t vs_irpvs_convert_flag;
	uint8_t adc0_inject_vs_s_f_flag;
	uint8_t adc1_inject_i_rpvs_neg_flag;
	uint8_t adc1_inject_i_rpvs_pos_flag;
	uint8_t adc1_inject_vs_flag;
} adc_handle_type;
void adc0_1_callback(void);
void adc_driver_init(void);
void adc_task_handle(adc_handle_type *adc_handle);
extern adc_handle_type adc_handle_struct;

#endif
