/*
 * state_machine_timer.h
 *
 *  Created on: 2021-10-12
 *      Author: zyq
 */
#ifndef STATE_MACHINE_TIMER_H_
#define STATE_MACHINE_TIMER_H_
#include "main.h"
unsigned long state_machine_tick_counter_get(void);
void state_machine_tick_counter_update(void);
#endif /* STATE_MACHINE_TIMER_H_ */
