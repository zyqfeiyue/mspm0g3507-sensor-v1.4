#include "soft_timer.h"

static volatile unsigned long tick_counter = 0;                  /*!< Tick counter of software timer */

unsigned long soft_timer_tick_counter_get(void)
{
	return tick_counter;
}

void soft_timer_tick_cnt_update(void)
{
	tick_counter++;
}

void std_systick_init(void)
{

}
