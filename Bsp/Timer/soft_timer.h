/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SOFT_TIMER_H
#define __SOFT_TIMER_H

#ifdef __cplusplus
extern "C" {
#endif
#include "main.h"
void std_systick_init(void);
void soft_timer_tick_cnt_update(void);
unsigned long soft_timer_tick_counter_get(void);
#endif
