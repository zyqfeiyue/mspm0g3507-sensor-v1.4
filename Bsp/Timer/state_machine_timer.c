#include "state_machine_timer.h"
#include "string.h"
static volatile unsigned long tick_counter = 0;                  /*!< Tick counter of software timer */

unsigned long state_machine_tick_counter_get(void)
{
	return tick_counter;
}

/**
 * @brief  Generally placed in the interrupt handler of the hardware timer
 *         and called periodically.
 * @note   The time base of the software timer depends on the interrupt of
 *         the hardware timer.
 * @param  None.
 * @retval None.
 */
void state_machine_tick_counter_update(void)
{
	tick_counter++;
}

