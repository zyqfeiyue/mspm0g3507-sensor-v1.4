#include "main.h"
#include "adc_driver.h"
#include "soft_timer.h"
#include "state_machine_timer.h"
#include "adc_app.h"
#include "system_state.h"
#include "run_control.h"
#include "heater_control.h"
#include "pump_control.h"
#include "can_driver.h"
#include "can_control.h"
#include "nox_calculate.h"
void system_nvic_init(void);
void sensor_parameter_init(void);
#define APP_START_ADDR 0x0004000 // 16k bootloader
int main(void)
{
	#ifdef BOOT_EN
	SCB->VTOR = APP_START_ADDR;// 重定向中断向量表
//	EnableInterrupts;
	__set_PRIMASK(0); // 开全局中断
	#endif
	sensor_parameter_init();
	SYSCFG_DL_init();
	system_nvic_init();
	uint32_t time = soft_timer_tick_counter_get();
	uint32_t activetime = soft_timer_tick_counter_get();
	while(1)
	{
		time = soft_timer_tick_counter_get();
		sensor_run_control_task();
		sensor_data_collect_task();
		can_receive_message();
		general_control_process(&can_handle_struct, &system_struct, &sensor_handle_struct, &sensor_obd_struct, &sensor_calibration_struct);
		system_run_state_process(&can_handle_struct, &system_struct, &sensor_handle_struct, &heater_control_struct, &sensor_calibration_struct);
		timeaging_compensation_process(&sensor_handle_struct.sensor_hardware_struct, &system_struct);
		heater_command_control(&system_struct, &sensor_handle_struct.sensor_hardware_struct, &can_handle_struct);
		if (time - activetime >= 1000)
		{
			activetime = soft_timer_tick_counter_get();
		}
	}
}
/**
 * @brief  系统中断初始化
 *         .
 * @note   
 *         
 * @param  None.
 * @retval None.
 */
void system_nvic_init(void)
{
	NVIC_EnableIRQ(MCAN0_INST_INT_IRQN);
	while (DL_MCAN_OPERATION_MODE_NORMAL != DL_MCAN_getOpMode(MCAN0_INST));
	adc_driver_init();
	NVIC_EnableIRQ(TIMER_0_INST_INT_IRQN);
	DL_TimerA_startCounter(TIMER_0_INST);
	NVIC_EnableIRQ(PWM_0_INST_INT_IRQN);
	DL_TimerA_startCounter(PWM_0_INST);
}
/**
 * @brief  传感器参数初始化
 *         .
 * @note   
 *         
 * @param  None.
 * @retval None.
 */
void sensor_parameter_init(void)
{
	calculate_equation(&sensor_calibration_struct);
	calibration_init();
	system_state_init();
	heater_control_init();
	pump_pid_init();
	sensor_character_para_init(&sensor_handle_struct.sensor_character_para_struct);
	system_struct.mode = SYSTEM_MODE_NORMAL;
	system_struct.run_state = SYSTEM_RUN_STATE_LOAD_PAREMETER;
	system_struct.dewpoint_command = false;
}
/**
 * @brief  系统滴答定时器
 *         .
 * @note   
 *         
 * @param  None.
 * @retval None.
 */
void SysTick_Handler(void)
{
	soft_timer_tick_cnt_update();
}
/**
 * @brief  加热pwm边沿中断
 *         .
 * @note   
 *         
 * @param  None.
 * @retval None.
 */
void PWM_0_INST_IRQHandler(void)
{
	switch (DL_TimerA_getPendingInterrupt(PWM_0_INST))
	{
		case DL_TIMER_IIDX_CC0_UP:
			rpvs_function_set_hpwm_flag(&rpvs_function_struct, RPVS_ENABLE);
			DL_TimerA_clearInterruptStatus(PWM_0_INST, DL_TIMERA_INTERRUPT_CC0_UP_EVENT);
			break;
		default:
			break;
	}
}
/**
 * @brief  传感器系统定时器中断
 *         .
 * @note   
 *         
 * @param  None.
 * @retval None.
 */
void TIMER_0_INST_IRQHandler(void)
{
	switch (DL_TimerA_getPendingInterrupt(TIMER_0_INST))
	{
		case DL_TIMER_IIDX_ZERO:
			state_machine_tick_counter_update();
			DL_TimerA_clearInterruptStatus(TIMER_0_INST, DL_TIMERA_INTERRUPT_ZERO_EVENT);
			break;
		default:
			break;
	}
}
/**
 * @brief  can通讯中断
 *         .
 * @note   
 *         
 * @param  None.
 * @retval None.
 */
void MCAN0_INST_IRQHandler(void)
{
	extern can_handle_type can_handle_struct;
	switch (DL_MCAN_getPendingInterrupt(MCAN0_INST)) {
		case DL_MCAN_IIDX_LINE1:
				/* Check MCAN interrupts fired during TX/RX of CAN package */
				can_handle_struct.gInterruptLine1Status |= DL_MCAN_getIntrStatus(MCAN0_INST);
				DL_MCAN_clearIntrStatus(MCAN0_INST, can_handle_struct.gInterruptLine1Status,
						DL_MCAN_INTR_SRC_MCAN_LINE_1);
				can_handle_struct.gServiceInt = true;
				break;
		default:
				break;
	}
}
