/*
 * Copyright (c) 2023, Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ============ ti_msp_dl_config.h =============
 *  Configured MSPM0 DriverLib module declarations
 *
 *  DO NOT EDIT - This file is generated for the MSPM0G350X
 *  by the SysConfig tool.
 */
#ifndef ti_msp_dl_config_h
#define ti_msp_dl_config_h

#define CONFIG_MSPM0G350X

#if defined(__ti_version__) || defined(__TI_COMPILER_VERSION__)
#define SYSCONFIG_WEAK __attribute__((weak))
#elif defined(__IAR_SYSTEMS_ICC__)
#define SYSCONFIG_WEAK __weak
#elif defined(__GNUC__)
#define SYSCONFIG_WEAK __attribute__((weak))
#endif

#include <ti/devices/msp/msp.h>
#include <ti/driverlib/driverlib.h>
#include <ti/driverlib/m0p/dl_core.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 *  ======== SYSCFG_DL_init ========
 *  Perform all required MSP DL initialization
 *
 *  This function should be called once at a point before any use of
 *  MSP DL.
 */


/* clang-format off */

#define POWER_STARTUP_DELAY                                                (16)


#define GPIO_HFXT_PORT                                                     GPIOA
#define GPIO_HFXIN_PIN                                             DL_GPIO_PIN_5
#define GPIO_HFXIN_IOMUX                                         (IOMUX_PINCM10)
#define GPIO_HFXOUT_PIN                                            DL_GPIO_PIN_6
#define GPIO_HFXOUT_IOMUX                                        (IOMUX_PINCM11)
#define CPUCLK_FREQ                                                     80000000



/* Defines for PWM_0 */
#define PWM_0_INST                                                         TIMA1
#define PWM_0_INST_IRQHandler                                   TIMA1_IRQHandler
#define PWM_0_INST_INT_IRQN                                     (TIMA1_INT_IRQn)
#define PWM_0_INST_CLK_FREQ                                              1000000
/* GPIO defines for channel 0 */
#define GPIO_PWM_0_C0_PORT                                                 GPIOA
#define GPIO_PWM_0_C0_PIN                                         DL_GPIO_PIN_10
#define GPIO_PWM_0_C0_IOMUX                                      (IOMUX_PINCM21)
#define GPIO_PWM_0_C0_IOMUX_FUNC                     IOMUX_PINCM21_PF_TIMA1_CCP0
#define GPIO_PWM_0_C0_IDX                                    DL_TIMER_CC_0_INDEX



/* Defines for TIMER_0 */
#define TIMER_0_INST                                                     (TIMA0)
#define TIMER_0_INST_IRQHandler                                 TIMA0_IRQHandler
#define TIMER_0_INST_INT_IRQN                                   (TIMA0_INT_IRQn)
#define TIMER_0_INST_LOAD_VALUE                                             (9U)




/* Defines for ADC12_0 */
#define ADC12_0_INST                                                        ADC0
#define ADC12_0_INST_IRQHandler                                  ADC0_IRQHandler
#define ADC12_0_INST_INT_IRQN                                    (ADC0_INT_IRQn)
#define ADC12_0_ADCMEM_IP1_ADC                                DL_ADC12_MEM_IDX_0
#define ADC12_0_ADCMEM_IP1_ADC_REF             DL_ADC12_REFERENCE_VOLTAGE_EXTREF
#define ADC12_0_ADCMEM_IP1_ADC_REF_VOLTAGE                                    3.30
#define ADC12_0_ADCMEM_VS_S_F_ADC                             DL_ADC12_MEM_IDX_1
#define ADC12_0_ADCMEM_VS_S_F_ADC_REF          DL_ADC12_REFERENCE_VOLTAGE_EXTREF
#define ADC12_0_ADCMEM_VS_S_F_ADC_REF_VOLTAGE                                    3.30
#define ADC12_0_ADCMEM_IP2_ADC                                DL_ADC12_MEM_IDX_2
#define ADC12_0_ADCMEM_IP2_ADC_REF             DL_ADC12_REFERENCE_VOLTAGE_EXTREF
#define ADC12_0_ADCMEM_IP2_ADC_REF_VOLTAGE                                    3.30
#define ADC12_0_ADCMEM_IP2N_ADC                               DL_ADC12_MEM_IDX_3
#define ADC12_0_ADCMEM_IP2N_ADC_REF            DL_ADC12_REFERENCE_VOLTAGE_EXTREF
#define ADC12_0_ADCMEM_IP2N_ADC_REF_VOLTAGE                                    3.30
#define ADC12_0_ADCMEM_IP1N_S_ADC                             DL_ADC12_MEM_IDX_4
#define ADC12_0_ADCMEM_IP1N_S_ADC_REF          DL_ADC12_REFERENCE_VOLTAGE_EXTREF
#define ADC12_0_ADCMEM_IP1N_S_ADC_REF_VOLTAGE                                    3.30
#define ADC12_0_ADCMEM_INPUT_POWER_ADC                        DL_ADC12_MEM_IDX_5
#define ADC12_0_ADCMEM_INPUT_POWER_ADC_REF       DL_ADC12_REFERENCE_VOLTAGE_EXTREF
#define ADC12_0_ADCMEM_INPUT_POWER_ADC_REF_VOLTAGE                                    3.30
#define GPIO_ADC12_0_C0_PORT                                               GPIOA
#define GPIO_ADC12_0_C0_PIN                                       DL_GPIO_PIN_27
#define GPIO_ADC12_0_C1_PORT                                               GPIOA
#define GPIO_ADC12_0_C1_PIN                                       DL_GPIO_PIN_26
#define GPIO_ADC12_0_C2_PORT                                               GPIOA
#define GPIO_ADC12_0_C2_PIN                                       DL_GPIO_PIN_25
#define GPIO_ADC12_0_C3_PORT                                               GPIOA
#define GPIO_ADC12_0_C3_PIN                                       DL_GPIO_PIN_24
#define GPIO_ADC12_0_C7_PORT                                               GPIOA
#define GPIO_ADC12_0_C7_PIN                                       DL_GPIO_PIN_22

/* Defines for ADC12_1 */
#define ADC12_1_INST                                                        ADC1
#define ADC12_1_INST_IRQHandler                                  ADC1_IRQHandler
#define ADC12_1_INST_INT_IRQN                                    (ADC1_INT_IRQn)
#define ADC12_1_ADCMEM_VS_ADC                                 DL_ADC12_MEM_IDX_0
#define ADC12_1_ADCMEM_VS_ADC_REF              DL_ADC12_REFERENCE_VOLTAGE_EXTREF
#define ADC12_1_ADCMEM_VS_ADC_REF_VOLTAGE                                    3.30
#define ADC12_1_ADCMEM_I_RPVS_ADC                             DL_ADC12_MEM_IDX_1
#define ADC12_1_ADCMEM_I_RPVS_ADC_REF          DL_ADC12_REFERENCE_VOLTAGE_EXTREF
#define ADC12_1_ADCMEM_I_RPVS_ADC_REF_VOLTAGE                                    3.30
#define ADC12_1_ADCMEM_COM_ADC                                DL_ADC12_MEM_IDX_2
#define ADC12_1_ADCMEM_COM_ADC_REF             DL_ADC12_REFERENCE_VOLTAGE_EXTREF
#define ADC12_1_ADCMEM_COM_ADC_REF_VOLTAGE                                    3.30
#define GPIO_ADC12_1_C1_PORT                                               GPIOA
#define GPIO_ADC12_1_C1_PIN                                       DL_GPIO_PIN_16
#define GPIO_ADC12_1_C2_PORT                                               GPIOA
#define GPIO_ADC12_1_C2_PIN                                       DL_GPIO_PIN_17
#define GPIO_ADC12_1_C3_PORT                                               GPIOA
#define GPIO_ADC12_1_C3_PIN                                       DL_GPIO_PIN_18


/* Defines for VREF */
#define VREF_VOLTAGE_MV                                                     3300
#define GPIO_VREF_VREFPOS_PORT                                             GPIOA
#define GPIO_VREF_VREFPOS_PIN                                     DL_GPIO_PIN_23
#define GPIO_VREF_IOMUX_VREFPOS                                  (IOMUX_PINCM53)
#define GPIO_VREF_IOMUX_VREFPOS_FUNC                IOMUX_PINCM53_PF_UNCONNECTED
#define VREF_READY_DELAY                                                   (800)




/* Defines for DMA_CH0 */
#define DMA_CH0_CHAN_ID                                                      (0)
#define ADC12_0_INST_DMA_TRIGGER                      (DMA_ADC0_EVT_GEN_BD_TRIG)

/* Defines for DMA_CH1 */
#define DMA_CH1_CHAN_ID                                                      (1)
#define ADC12_1_INST_DMA_TRIGGER                      (DMA_ADC1_EVT_GEN_BD_TRIG)



/* Port definition for Pin Group GPIO_GRP_0 */
#define GPIO_GRP_0_PORT                                                  (GPIOA)

/* Defines for ICP_SELECT: GPIOA.0 with pinCMx 1 on package pin 1 */
#define GPIO_GRP_0_ICP_SELECT_PIN                                (DL_GPIO_PIN_0)
#define GPIO_GRP_0_ICP_SELECT_IOMUX                               (IOMUX_PINCM1)
/* Defines for VS_I_SELECT: GPIOA.1 with pinCMx 2 on package pin 2 */
#define GPIO_GRP_0_VS_I_SELECT_PIN                               (DL_GPIO_PIN_1)
#define GPIO_GRP_0_VS_I_SELECT_IOMUX                              (IOMUX_PINCM2)
/* Defines for CAN_SET: GPIOA.3 with pinCMx 8 on package pin 7 */
#define GPIO_GRP_0_CAN_SET_PIN                                   (DL_GPIO_PIN_3)
#define GPIO_GRP_0_CAN_SET_IOMUX                                  (IOMUX_PINCM8)
/* Defines for RPVS_SELECT: GPIOA.4 with pinCMx 9 on package pin 8 */
#define GPIO_GRP_0_RPVS_SELECT_PIN                               (DL_GPIO_PIN_4)
#define GPIO_GRP_0_RPVS_SELECT_IOMUX                              (IOMUX_PINCM9)
/* Defines for COM_SELECT: GPIOA.7 with pinCMx 14 on package pin 11 */
#define GPIO_GRP_0_COM_SELECT_PIN                                (DL_GPIO_PIN_7)
#define GPIO_GRP_0_COM_SELECT_IOMUX                              (IOMUX_PINCM14)
/* Defines for VS_I_OUT_SELECT: GPIOA.8 with pinCMx 19 on package pin 12 */
#define GPIO_GRP_0_VS_I_OUT_SELECT_PIN                           (DL_GPIO_PIN_8)
#define GPIO_GRP_0_VS_I_OUT_SELECT_IOMUX                         (IOMUX_PINCM19)
/* Defines for IP2P_SELECT: GPIOA.9 with pinCMx 20 on package pin 13 */
#define GPIO_GRP_0_IP2P_SELECT_PIN                               (DL_GPIO_PIN_9)
#define GPIO_GRP_0_IP2P_SELECT_IOMUX                             (IOMUX_PINCM20)
/* Defines for IP2_BOOST_SELECT: GPIOA.11 with pinCMx 22 on package pin 15 */
#define GPIO_GRP_0_IP2_BOOST_SELECT_PIN                         (DL_GPIO_PIN_11)
#define GPIO_GRP_0_IP2_BOOST_SELECT_IOMUX                        (IOMUX_PINCM22)





/* Defines for DAC12 */
#define DAC12_IRQHandler                                         DAC0_IRQHandler
#define DAC12_INT_IRQN                                           (DAC0_INT_IRQn)
#define GPIO_DAC12_OUT_PORT                                                GPIOA
#define GPIO_DAC12_OUT_PIN                                        DL_GPIO_PIN_15
#define GPIO_DAC12_IOMUX_OUT                                     (IOMUX_PINCM37)
#define GPIO_DAC12_IOMUX_OUT_FUNC                   IOMUX_PINCM37_PF_UNCONNECTED




/* Defines for MCAN0 */
#define MCAN0_INST                                                        CANFD0
#define GPIO_MCAN0_CAN_TX_PORT                                             GPIOA
#define GPIO_MCAN0_CAN_TX_PIN                                     DL_GPIO_PIN_12
#define GPIO_MCAN0_IOMUX_CAN_TX                                  (IOMUX_PINCM34)
#define GPIO_MCAN0_IOMUX_CAN_TX_FUNC               IOMUX_PINCM34_PF_CANFD0_CANTX
#define GPIO_MCAN0_CAN_RX_PORT                                             GPIOA
#define GPIO_MCAN0_CAN_RX_PIN                                     DL_GPIO_PIN_13
#define GPIO_MCAN0_IOMUX_CAN_RX                                  (IOMUX_PINCM35)
#define GPIO_MCAN0_IOMUX_CAN_RX_FUNC               IOMUX_PINCM35_PF_CANFD0_CANRX
#define MCAN0_INST_IRQHandler                                 CANFD0_IRQHandler
#define MCAN0_INST_INT_IRQN                                     CANFD0_INT_IRQn


/* Defines for MCAN0 MCAN RAM configuration */
#define MCAN0_INST_MCAN_STD_ID_FILT_START_ADDR     (0)
#define MCAN0_INST_MCAN_STD_ID_FILTER_NUM          (0)
#define MCAN0_INST_MCAN_EXT_ID_FILT_START_ADDR     (500)
#define MCAN0_INST_MCAN_EXT_ID_FILTER_NUM          (1)
#define MCAN0_INST_MCAN_TX_BUFF_START_ADDR         (0)
#define MCAN0_INST_MCAN_TX_BUFF_SIZE               (1)
#define MCAN0_INST_MCAN_FIFO_1_START_ADDR          (190)
#define MCAN0_INST_MCAN_FIFO_1_NUM                 (2)
#define MCAN0_INST_MCAN_TX_EVENT_START_ADDR        (0)
#define MCAN0_INST_MCAN_TX_EVENT_SIZE              (2)
#define MCAN0_INST_MCAN_EXT_ID_AND_MASK            (0x1FFFFFFFU)
#define MCAN0_INST_MCAN_RX_BUFF_START_ADDR         (208)
#define MCAN0_INST_MCAN_FIFO_0_START_ADDR          (170)
#define MCAN0_INST_MCAN_FIFO_0_NUM                 (3)

#define MCAN0_INST_MCAN_INTERRUPTS (DL_MCAN_INTERRUPT_ARA | \
						DL_MCAN_INTERRUPT_BEU | \
						DL_MCAN_INTERRUPT_BO | \
						DL_MCAN_INTERRUPT_DRX | \
						DL_MCAN_INTERRUPT_ELO | \
						DL_MCAN_INTERRUPT_EP | \
						DL_MCAN_INTERRUPT_EW | \
						DL_MCAN_INTERRUPT_MRAF | \
						DL_MCAN_INTERRUPT_PEA | \
						DL_MCAN_INTERRUPT_PED | \
						DL_MCAN_INTERRUPT_RF0N | \
						DL_MCAN_INTERRUPT_TC | \
						DL_MCAN_INTERRUPT_TEFN | \
						DL_MCAN_INTERRUPT_TOO | \
						DL_MCAN_INTERRUPT_TSW | \
						DL_MCAN_INTERRUPT_WDI)



/* clang-format on */

void SYSCFG_DL_init(void);
void SYSCFG_DL_initPower(void);
void SYSCFG_DL_GPIO_init(void);
void SYSCFG_DL_SYSCTL_init(void);
void SYSCFG_DL_PWM_0_init(void);
void SYSCFG_DL_TIMER_0_init(void);
void SYSCFG_DL_ADC12_0_init(void);
void SYSCFG_DL_ADC12_1_init(void);
void SYSCFG_DL_VREF_init(void);
void SYSCFG_DL_DMA_init(void);

void SYSCFG_DL_SYSTICK_init(void);
void SYSCFG_DL_DAC12_init(void);

void SYSCFG_DL_MCAN0_init(void);

bool SYSCFG_DL_saveConfiguration(void);
bool SYSCFG_DL_restoreConfiguration(void);

#ifdef __cplusplus
}
#endif

#endif /* ti_msp_dl_config_h */
